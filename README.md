# Shogi de Terminal em C

## Apresentação

Esse é meu primeiro projeto pessoal e tem como objetivo fixar os conhecimentos da linguagem de programação C adquiridos no segundo semestre do curso superior de Análise e Desenvolvimento de Sistemas, bem como a criação de um portfólio que possibilite a aquisição do primeiro emprego na área de desenvolvimento de software.

Visto que gosto da linguagem C e de shogi pensei "por que não fazer um jogo de shogi para terminal ?".

## Descrição do projeto

Shogi (ou xadrez japonês) é um jogo de tabuleiro 9x9 parecido com xadrez mas com uma maior variedade de peças e com a possibilidade de retornar ao jogo as peças capturadas. Sendo um jogo originado no Japão, as peças são identificadas através de kanjis mas para facilitar o entendimento de todos que queiram testar o jogo, o projeto utiliza a nomenclatura em inglês das peças sendo exceção as peças promovidas, devido à escolha de design do tabuleiro no terminal, como pode ser visualizado nas tabelas e imagens abaixo:

| Peça em japonês | Peça em inglês | Peça no terminal |
|:---------------:|:--------------:|:----------------:|
| 歩 (Fu)         | Pawn           | P1               |
| 飛車 (Hisha)    | Rook           | H1               |
| 角 (Kaku)       | Bishop         | B1               |
| 香車 (Kyousha)  | Lance          | L1               |
| 桂馬 (Keima)    | Knight         | K1               |
| 銀 (Gin)        | Silver         | S1               |
| 金 (Kin)        | Gold           | G1               |
| 王 (Ou)         | King           | KK               |

| Peça em japonês | Peça em inglês  | Peça no terminal                 |
|:---------------:|:---------------:|:--------------------------------:|
| と金 (Tokin)    | Promoted Pawn   | T1                               |
| 杏 (Narikyou)   | Promoted Lance  | N1                               |
| 全 (Narigin)    | Promoted Silver | Z1 (全 pode ser lido como "Zen") |
| 今 (Narikei)    | Promoted Knight | I1 (今 pode ser lido como "Ima") |
| 竜 (Ryuu)       | Dragon          | D1                               |
| 馬 (Uma)        | Horse           | H1                               |

Tabuleiro do shogi com as peças

![Shogi Board](/imagens/ShogiBoard.PNG)

Imagem retirada do site [lishogi](https://lishogi.org/)

Tabuleiro no terminal

![Terminal Board](/imagens/TerminalShogi.PNG)

### Movimentação das peças && Regras do jogo

Recomendo a explicação da [Wikipedia](https://en.wikipedia.org/wiki/Shogi). É possível visualizar a movimentação e as regras do jogo digitando "menu" enquanto joga.

### Como jogar

* A primeira mensagem a aparecer é "Escolha a peça" e o usuário pode escolher qual de suas peças movimentar digitando o nome da peça ou, caso já tenha capturado alguma peça, basta digitar o nome da peça indicado no lado direito do tabuleiro.

* Independente da escolha, após a confirmação, a mensagem "Qual Casa ?" será exibida, basta digitar o numero da linha e da coluna da casa que a peça deve se mover/ser colocada.

* Seu turno termina e é a vez do oponente realizar a sua jogada (como ainda não aprendi inteligência artificial, pensei em usar números aleatório para simular uma I.A. mas não seria divertido jogar contra algo do tipo, portanto, essa versão do jogo é PvP).

O jogo termina quando houver um xeque-mate (o nome em inglês "checkmate" foi usado durante o projeto ao invés de xeque-mate). É possível acessar essas informações através do comando "menu".

### Promoção

Ao chegar nas linhas 3, 2 ou 1 (ou 7, 8 e 9 para o oponente) a mensagem "Deseja promover a peça ?" é exibida com as opções S (sim) e N (nao), é possível promover saindo ou entrando na zona de promoção.

### Captura && Colocando uma peça em jogo

Ao capturar uma peça promovida ela é automaticamente convertida para a sua versão não promovida, portanto <strong>não é possível colocar em jogo uma peça promovida, apenas sua versão não promovida</strong>.

## Conversão entre matrizes && Calculo dos movimentos

Essa foi a lógica utilizada para a programação do projeto:

Matriz Interface x Matriz Programação
![Shogi Board](/imagens/Matrix.png)

Cálculos para realizar a movimentação das peças
![Shogi Board](/imagens/MoveCalculation.png)