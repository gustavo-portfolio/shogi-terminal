#include <stdio.h>
#include <stdlib.h>
#include <Windows.h>
#include <stdbool.h>
#include <string.h>
#include <ctype.h>

typedef char pecasTabuleiro[3];

void menu();

void renderizarTabuleiro(pecasTabuleiro renderizar[9][9], int maoJogador[7], int maoOponente[7]);

bool acharPeca(char qualPeca[3], pecasTabuleiro tabuleiro[9][9], int *posicaoMatriz, int *posicaoInterface);

bool peaoColuna(char nomePeca, int *posicaoPeca, pecasTabuleiro tabuleiro[9][9]);

bool regrasPeca(char nomePeca, int *posicaoPeca, pecasTabuleiro tabuleiro[9][9]);

bool check(char rei, int *posicaoRei, pecasTabuleiro tabuleiro[9][9]);

bool colocarPeca(char peca[3], int *casa, pecasTabuleiro tabuleiro[9][9], int quantidadeMaoJogador[7], char numeroPecaJogador[13][9], int quantidadeMaoOponente[7], char numeroPecaOponente[13][9]);

void capturarPeca(char peca[3], int quantidadeMaoJogador[7], int quantidadeMaoOponente[7], char numeroPecaJogador[13][9], char numeroPecaOponente[13][9]);

void promoverPeca(char peca[3], int *casa, char numeroPecaJogador[13][9], char numeroPecaOponente[13][9], pecasTabuleiro tabuleiro[9][9]);

bool movimentarPeca(char peca[3], int *casa, int *posicaoMatriz, int *posicaoInterface, pecasTabuleiro tabuleiro[9][9], char capturado[3], bool *promover);

bool movimentosPossiveisRei(char rei, int *posicaoRei, pecasTabuleiro tabuleiro[9][9], int casasPossiveis[7]);

bool pinado(char peca, int *posicaoPeca, pecasTabuleiro tabuleiro[9][9]);

bool podeProtegerAtacando(char atacante, int *posicaoAtacante, pecasTabuleiro tabuleiro[9][9]);

bool podeProtegerDefendendo(char atacante, int *posicaoAtacante, int *posicaoRei, int maoJogador[7], int maoOponente[7], pecasTabuleiro tabuleiro[9][9]);

bool podeProtegerInterceptando(char atacante, int *posicaoAtacante, int *posicaoRei, pecasTabuleiro tabuleiro[9][9]);

int main()
{
    char peca[3];
    bool achou;
    char capturou[3];
    int casa;
    int posicaoMatriz;
    int posicaoInterface;
    bool promover = false;
    bool estaEmCheck = false;
    bool movimentou;
    bool podeProtegerAtk;
    bool podeProtegerDef;
    bool podeInterceptar;
    char atacante;
    int posicaoAtacante;
    int casasPossiveis[7];
    int i;
    int converterCasa;
    char reiJogador[3] = "KK";
    char reiOponente[3] = "kk";
    bool reiPodeMover = true;
    bool checkMate = false;
    bool colocou;
    bool jogadorJogou, oponenteJogou = false;
    bool jogadorGanhou = false;
    //Quantidade de cada peça que o usuário pode usar
    int quantidadeMaoJogador[7] = {0, 0, 0, 0, 0, 0, 0};
    int quantidadeMaoOponente[7] = {0, 0, 0, 0, 0, 0, 0};
    //Número de peça disponível para quando o usuário colocar a peça na mesa 
    char numeroPecaJogador[13][9] = 
    {
        //Iniciar os números de peça já disponíveis                Intervalo
        {'2', '0', '0', '0', '0', '0', '0', '0', '0'}, //Bishop     (1..2)
        {'3', '4', '0', '0', '0', '0', '0', '0', '0'}, //Gold       (1..4)
        {'3', '4', '0', '0', '0', '0', '0', '0', '0'}, //Knight     (1..4)
        {'3', '4', '0', '0', '0', '0', '0', '0', '0'}, //Lance      (1..4)
        {'0', '0', '0', '0', '0', '0', '0', '0', '0'}, //Pawn       (1..9)
        {'2', '0', '0', '0', '0', '0', '0', '0', '0'}, //Rook       (1..2)
        {'3', '4', '0', '0', '0', '0', '0', '0', '0'}, //Silver     (1..4)
        {'1', '2', '3', '4', '5', '6', '7', '8', '9'}, //Tokin      (1..9)
        {'1', '2', '3', '4', '0', '0', '0', '0', '0'}, //Zen        (1..4)
        {'1', '2', '3', '4', '0', '0', '0', '0', '0'}, //Narikyou   (1..4)
        {'1', '2', '3', '4', '0', '0', '0', '0', '0'}, //Ima        (1..4)
        {'1', '2', '0', '0', '0', '0', '0', '0', '0'}, //Dragon     (1..2)
        {'1', '2', '0', '0', '0', '0', '0', '0', '0'}, //Horse      (1..2)
    };
    char numeroPecaOponente[13][9] = 
    {
        //Iniciar os números de peça já disponíveis                Intervalo
        {'2', '0', '0', '0', '0', '0', '0', '0', '0'}, //Bishop     (1..2)
        {'3', '4', '0', '0', '0', '0', '0', '0', '0'}, //Gold       (1..4)
        {'3', '4', '0', '0', '0', '0', '0', '0', '0'}, //Knight     (1..4)
        {'3', '4', '0', '0', '0', '0', '0', '0', '0'}, //Lance      (1..4)
        {'0', '0', '0', '0', '0', '0', '0', '0', '0'}, //Pawn       (1..9)
        {'2', '0', '0', '0', '0', '0', '0', '0', '0'}, //Rook       (1..2)
        {'3', '4', '0', '0', '0', '0', '0', '0', '0'}, //Silver     (1..4)
        {'1', '2', '3', '4', '5', '6', '7', '8', '9'}, //Tokin      (1..9)
        {'1', '2', '3', '4', '0', '0', '0', '0', '0'}, //Zen        (1..4)
        {'1', '2', '3', '4', '0', '0', '0', '0', '0'}, //Narikyou   (1..4)
        {'1', '2', '3', '4', '0', '0', '0', '0', '0'}, //Ima        (1..4)
        {'1', '2', '0', '0', '0', '0', '0', '0', '0'}, //Dragon     (1..2)
        {'1', '2', '0', '0', '0', '0', '0', '0', '0'}, //Horse      (1..2)
    };
    //Relação dos índices com as peças
    /*
        VETOR
        0 1 2 3 4 5 6
        B G K L P R S
    */
    /*
        MATRIZ
        0 B
        1 G
        2 K
        3 L
        4 P
        5 R
        6 S
        //Promovidos
        7 T
        8 Z
        9 N
        10 I
        11 D
        12 H
    */

    pecasTabuleiro tabuleiro[9][9] =
    {
        {"l1", "k1", "s1", "g1", "kk", "g2", "s2", "k2", "l2"},
        {"  ", "r1", "  ", "  ", "  ", "  ", "  ", "b1", "  "},
        {"p1", "p2", "p3", "p4", "p5", "p6", "p7", "p8", "p9"},
        {"  ", "  ", "  ", "  ", "  ", "  ", "  ", "  ", "  "},
        {"  ", "  ", "  ", "  ", "  ", "  ", "  ", "  ", "  "},
        {"  ", "  ", "  ", "  ", "  ", "  ", "  ", "  ", "  "},
        {"P1", "P2", "P3", "P4", "P5", "P6", "P7", "P8", "P9"},
        {"  ", "R1", "  ", "  ", "  ", "  ", "  ", "B1", "  "},
        {"L1", "K1", "S1", "G1", "KK", "G2", "S2", "K2", "L2"},
    };

    system("cls");
    renderizarTabuleiro(tabuleiro, quantidadeMaoJogador, quantidadeMaoOponente);

    //Conferir checkmate
    while(checkMate == false)
    {
        //Turno do JOGADOR
        do
        {
            printf("\n              Jogador\n");
            printf("\nDigite \"menu\" para acessar o menu\n");
            //Se o rei está em check e não consegue usar uma peça para se proteger, o jogador deve mover o rei
            if((estaEmCheck == true) && (podeProtegerAtk == false) && (podeProtegerDef == false) && (podeInterceptar == false))
            {
                estaEmCheck = false;
                //O jogador escolhe o rei
                do
                {
                    printf("\nEscolha a peca\n");
                    scanf("%s", &peca);

                    if(toupper(peca[0]) == 'M') //Menu
                    {
                        menu();
                        renderizarTabuleiro(tabuleiro, quantidadeMaoJogador, quantidadeMaoOponente);
                        
                        printf("\n              Jogador\n");
                        printf("\nDigite \"menu\" para acessar o menu\n");
                    }
                    else if(peca[1] != 'K')
                    {
                        printf("\nO rei estah em xeque! Voce soh pode mexer o rei!\n");
                    }

                } while ((peca[1] != 'K') && (peca[0] != 'K'));

                acharPeca(peca, tabuleiro, &posicaoMatriz, &posicaoInterface);

                //"Limpar" o vetor "casasPossiveis" para a chamada da função "movimentosPossiveisRei"
                for(i = 0; i < 8; i++)
                {
                    casasPossiveis[i] = 0;
                }

                movimentosPossiveisRei(peca[0], &posicaoMatriz, tabuleiro, casasPossiveis);

                do
                {
                    do
                    {
                        //O jogador escolhe a casa onde o rei deve ser movido
                        printf("\nQual casa? (digite o valor da linha e da coluna, ex: 45)\n");
                        scanf("%d", &casa);

                        if((casa < 11) && (casa > 99))
                        {
                            printf("\nNumero de casa invalido!");
                        }

                    } while ((casa < 11) && (casa > 99));

                    //Converter a jogada para a matriz
                    converterCasa = ((casa / 10 ) - 1) * 10;
                    converterCasa += 9 - (casa % 10);

                    //Verificar se o movimento é possível
                    i = 0;
                    achou = false;

                    while((i < 8) && (achou == false))
                    {
                        if(converterCasa == casasPossiveis[i])
                        {
                            achou = true;
                        }
                        i++;
                    }

                    if(achou == false)
                    {
                        printf("\nMovimento invalido!\n");
                    }

                } while (achou == false);

                movimentou = movimentarPeca(peca, &casa, &posicaoMatriz, &posicaoInterface, tabuleiro, capturou, &promover);

                if(movimentou == true)
                {
                    //Verificar se houve captura
                    if((capturou[0] > 'A') && (movimentou == true))
                    {
                        capturarPeca(capturou, quantidadeMaoJogador, quantidadeMaoOponente, numeroPecaJogador, numeroPecaOponente);

                        //Evitar que "capturou" entre novamente no if no caso de não haver outra captura
                        capturou[0] = 'A';
                    }

                    //O jogador realizou a jogada com sucesso
                    jogadorJogou = true;
                }
            }
            //Se o jogador não precisa usar o rei, ele pode jogar normalmente
            else
            {
                //O jogador escolhe a peça
                do
                {
                    printf("\nEscolha a peca\n");
                    scanf("%s", &peca);

                    if(toupper(peca[0]) == 'M') //Menu
                    {
                        menu();
                        renderizarTabuleiro(tabuleiro, quantidadeMaoJogador, quantidadeMaoOponente);
                        
                        printf("\n              Jogador\n");
                        printf("\nDigite \"menu\" para acessar o menu\n");
                    }
                    else if((peca[0] > 'a') && (peca[0] <= 'z'))
                    {
                        printf("\nPeca invalida!\n");
                    }

                } while ((peca[0] > 'a') && (peca[0] <= 'z'));

                //Verificar se o usuário quer mover uma peça ou colocar uma peça em jogo
                //Colocar peça em jogo
                if(peca[1] == NULL)
                {
                    //O jogador escolhe a casa onde a peça deve ser movida
                    do
                    {
                        printf("\nQual casa? (digite o valor da linha e da coluna, ex: 45)\n");
                        scanf("%d", &casa);

                        if((casa < 11) && (casa > 99))
                        {
                            printf("\nNumero de casa invalido!");
                        }

                    } while ((casa < 11) && (casa > 99));

                    //Coloca peça
                    colocou = colocarPeca(peca, &casa, tabuleiro, quantidadeMaoJogador, numeroPecaJogador, quantidadeMaoOponente, numeroPecaOponente);

                    if(colocou == true)
                    {
                        //Verificar se houve um check
                        acharPeca(reiOponente, tabuleiro, &posicaoMatriz, &posicaoInterface);
                        estaEmCheck = check(reiOponente[0], &posicaoMatriz, tabuleiro);
                        
                        if(estaEmCheck == true)
                        {
                            atacante = peca[0];

                            podeProtegerAtk = podeProtegerAtacando(atacante, &casa, tabuleiro);
                            podeProtegerDef = podeProtegerDefendendo(atacante, &casa, &posicaoMatriz, quantidadeMaoJogador, quantidadeMaoOponente, tabuleiro);

                            reiPodeMover = movimentosPossiveisRei(reiOponente[0], &posicaoMatriz, tabuleiro, casasPossiveis);
                            podeInterceptar = podeProtegerInterceptando(atacante, &casa, &posicaoMatriz, tabuleiro);

                            //Conferir se houve um checkmate
                            if((podeProtegerAtk == false) && (podeProtegerDef == false) && (reiPodeMover == false) && (podeInterceptar == false) )
                            {
                                checkMate = true;
                                jogadorGanhou = true;
                            }
                        }

                        //O jogador realizou a jogada com sucesso
                        jogadorJogou = true;
                    }
                }
                else //Movimentar peça
                {
                    //Verifica se a peça existe no tabuleiro
                    if(acharPeca(peca, tabuleiro, &posicaoMatriz, &posicaoInterface) == false)
                    {
                        printf("\nA peca nao existe!\n");
                    }
                    else
                    {
                        //O jogador escolhe a casa onde a peça deve ser movida
                        do
                        {
                            printf("\nQual casa? (digite o valor da linha e da coluna, ex: 45)\n");
                            scanf("%d", &casa);

                            if((casa < 11) && (casa > 99))
                            {
                                printf("\nNumero de casa invalido!");
                            }

                        } while ((casa < 11) && (casa > 99));

                        movimentou = movimentarPeca(peca, &casa, &posicaoMatriz, &posicaoInterface, tabuleiro, capturou, &promover);

                        if(movimentou == true)
                        {
                            //Verificar se houve captura
                            if(capturou[0] > 'A')
                            {
                                capturarPeca(capturou, quantidadeMaoJogador, quantidadeMaoOponente, numeroPecaJogador, numeroPecaOponente);

                                //Evitar que "capturou" entre novamente no if no caso de não haver outra captura
                                capturou[0] = 'A';
                            }

                            //Verificar se deve promover ou promover automático
                            if(((peca[0] == 'P') || (peca[0] == 'L')) && (casa < 20))
                            {
                                promoverPeca(peca, &casa, numeroPecaJogador, numeroPecaOponente, tabuleiro);
                            }
                            else if(((peca[0] == 'K') && (peca[1] != 'K')) && (casa < 30))
                            {
                                promoverPeca(peca, &casa, numeroPecaJogador, numeroPecaOponente, tabuleiro);
                            }
                            else if(promover == true)
                            {
                                promoverPeca(peca, &casa, numeroPecaJogador, numeroPecaOponente, tabuleiro);
                                promover = false;
                            }

                            //Verificar se o rei OPONENTE está em check
                            acharPeca(reiOponente, tabuleiro, &posicaoMatriz, &posicaoInterface);
                            estaEmCheck = check(reiOponente[0], &posicaoMatriz, tabuleiro);
                            
                            if(estaEmCheck == true)
                            {
                                atacante = peca[0];

                                podeProtegerAtk = podeProtegerAtacando(atacante, &casa, tabuleiro);
                                podeProtegerDef = podeProtegerDefendendo(atacante, &casa, &posicaoMatriz, quantidadeMaoJogador, quantidadeMaoOponente, tabuleiro);

                                reiPodeMover = movimentosPossiveisRei(reiOponente[0], &posicaoMatriz, tabuleiro, casasPossiveis);
                                podeInterceptar = podeProtegerInterceptando(atacante, &casa, &posicaoMatriz, tabuleiro);

                                //Conferir se houve um checkmate
                                if((podeProtegerAtk == false) && (podeProtegerDef == false) && (reiPodeMover == false) && (podeInterceptar == false))
                                {
                                    checkMate = true;
                                    jogadorGanhou = true;
                                }
                            }

                            //O jogador realizou a jogada com sucesso
                            jogadorJogou = true;
                        }
                    }
                }
            }

            Sleep(500);
            system("cls");

            renderizarTabuleiro(tabuleiro, quantidadeMaoJogador, quantidadeMaoOponente);
            
        } while (jogadorJogou == false);
        
        jogadorJogou = false;
        
        //Turno do OPONENTE
        //Conferir se houve um checkmate antes de deixar o oponente jogar
        if(checkMate == false)
        {
            do
            {
                printf("\n              Oponente\n");
                printf("\nDigite \"menu\" para acessar o menu\n");
                //Se o rei está em check e não consegue usar uma peça para se proteger, o oponente deve mover o rei
                if((estaEmCheck == true) && (podeProtegerAtk == false) && (podeProtegerDef == false) && (podeInterceptar == false))
                {
                    estaEmCheck = false;
                    //O oponente escolhe o rei
                    do
                    {
                        printf("\nEscolha a peca\n");
                        scanf("%s", &peca);

                        if(toupper(peca[0]) == 'M') //Menu
                        {
                            menu();
                            renderizarTabuleiro(tabuleiro, quantidadeMaoJogador, quantidadeMaoOponente);
                            
                            printf("\n              Oponente\n");
                            printf("\nDigite \"menu\" para acessar o menu\n");
                        }
                        else if(peca[1] != 'k')
                        {
                            printf("\nO rei estah em xeque! Voce soh pode mexer o rei!\n");
                        }

                    } while ((peca[1] != 'k') && (peca[0] != 'k'));

                    acharPeca(peca, tabuleiro, &posicaoMatriz, &posicaoInterface);

                    //"Limpar" o vetor "casasPossiveis" para a chamada da função "movimentosPossiveisRei"
                    for(i = 0; i < 8; i++)
                    {
                        casasPossiveis[i] = 0;
                    }

                    movimentosPossiveisRei(peca[0], &posicaoMatriz, tabuleiro, casasPossiveis);

                    do
                    {
                        do
                        {
                            //O oponente escolhe a casa onde o rei deve ser movido
                            printf("\nQual casa? (digite o valor da linha e da coluna, ex: 45)\n");
                            scanf("%d", &casa);

                            if((casa < 11) && (casa > 99))
                            {
                                printf("\nNumero de casa invalido!");
                            }

                        } while ((casa < 11) && (casa > 99));

                        //Converter a jogada para a matriz
                        converterCasa = ((casa / 10 ) - 1) * 10;
                        converterCasa += 9 - (casa % 10);

                        //Verificar se o movimento é possível
                        i = 0;
                        achou = false;

                        while((i < 8) && (achou == false))
                        {
                            if(converterCasa == casasPossiveis[i])
                            {
                                achou = true;
                            }
                            i++;
                        }

                        if(achou == false)
                        {
                            printf("\nMovimento invalido!\n");
                        }

                    } while (achou == false);

                    movimentou = movimentarPeca(peca, &casa, &posicaoMatriz, &posicaoInterface, tabuleiro, capturou, &promover);

                    if(movimentou == true)
                    {
                        //Verificar se houve captura
                        if((capturou[0] > 'A') && (movimentou == true))
                        {
                            capturarPeca(capturou, quantidadeMaoJogador, quantidadeMaoOponente, numeroPecaJogador, numeroPecaOponente);

                            //Evitar que "capturou" entre novamente no if no caso de não haver outra captura
                            capturou[0] = 'A';
                        }

                        //O oponente realizou a jogada com sucesso
                        oponenteJogou = true;
                    }
                }
                //Se o oponente não precisa usar o rei, ele pode jogar normalmente
                else
                {
                    //O oponente escolhe a peça
                    do
                    {
                        printf("\nEscolha a peca\n");
                        scanf("%s", &peca);

                        if(tolower(peca[0]) == 'm') //Menu
                        {
                            menu();
                            renderizarTabuleiro(tabuleiro, quantidadeMaoJogador, quantidadeMaoOponente);
                            
                            printf("\n              Oponente\n");
                            printf("\nDigite \"menu\" para acessar o menu\n");
                        }
                        else if((peca[0] > 'A') && (peca[0] <= 'Z'))
                        {
                            printf("\nPeca invalida!\n");
                        }

                    } while ((peca[0] > 'A') && (peca[0] <= 'Z'));

                    //Verificar se o usuário quer mover uma peça ou colocar uma peça em jogo
                    //Colocar peça em jogo
                    if(peca[1] == NULL)
                    {
                        //O oponente escolhe a casa onde a peça deve ser movida
                        do
                        {
                            printf("\nQual casa? (digite o valor da linha e da coluna, ex: 45)\n");
                            scanf("%d", &casa);

                            if((casa < 11) && (casa > 99))
                            {
                                printf("\nNumero de casa invalido!");
                            }

                        } while ((casa < 11) && (casa > 99));

                        //Coloca peça
                        colocou = colocarPeca(peca, &casa, tabuleiro, quantidadeMaoJogador, numeroPecaJogador, quantidadeMaoOponente, numeroPecaOponente);

                        if(colocou == true)
                        {
                            //Verificar se houve um check
                            acharPeca(reiJogador, tabuleiro, &posicaoMatriz, &posicaoInterface);
                            estaEmCheck = check(reiJogador[0], &posicaoMatriz, tabuleiro);
                            
                            if(estaEmCheck == true)
                            {
                                atacante = peca[0];

                                podeProtegerAtk = podeProtegerAtacando(atacante, &casa, tabuleiro);
                                podeProtegerDef = podeProtegerDefendendo(atacante, &casa, &posicaoMatriz, quantidadeMaoJogador, quantidadeMaoOponente, tabuleiro);

                                reiPodeMover = movimentosPossiveisRei(reiJogador[0], &posicaoMatriz, tabuleiro, casasPossiveis);
                                podeInterceptar = podeProtegerInterceptando(atacante, &casa, &posicaoMatriz, tabuleiro);

                                //Conferir se houve um checkmate
                                if((podeProtegerAtk == false) && (podeProtegerDef == false) && (reiPodeMover == false) && (podeInterceptar == false))
                                {
                                    checkMate = true;
                                }
                            }

                            //O oponente realizou a jogada com sucesso
                            oponenteJogou = true;
                        }
                    }
                    else //Movimentar peça
                    {
                        //Verifica se a peça existe no tabuleiro
                        if(acharPeca(peca, tabuleiro, &posicaoMatriz, &posicaoInterface) == false)
                        {
                            printf("\nA peca nao existe!\n");
                        }
                        else
                        {
                            //O jogador escolhe a casa onde a peça deve ser movida
                            do
                            {
                                printf("\nQual casa? (digite o valor da linha e da coluna, ex: 45)\n");
                                scanf("%d", &casa);

                                if((casa < 11) && (casa > 99))
                                {
                                    printf("\nNumero de casa invalido!");
                                }

                            } while ((casa < 11) && (casa > 99));

                            movimentou = movimentarPeca(peca, &casa, &posicaoMatriz, &posicaoInterface, tabuleiro, capturou, &promover);

                            if(movimentou == true)
                            {
                                //Verificar se houve captura
                                if(capturou[0] > 'A')
                                {
                                    capturarPeca(capturou, quantidadeMaoJogador, quantidadeMaoOponente, numeroPecaJogador, numeroPecaOponente);

                                    //Evitar que "capturou" entre novamente no if no caso de não haver outra captura
                                    capturou[0] = 'A';
                                }

                                //Verificar se deve promover ou promover automático
                                if(((peca[0] == 'p') || (peca[0] == 'l')) && (casa > 90))
                                {
                                    promoverPeca(peca, &casa, numeroPecaJogador, numeroPecaOponente, tabuleiro);
                                }
                                else if(((peca[0] == 'k') && (peca[1] != 'k')) && (casa > 80))
                                {
                                    promoverPeca(peca, &casa, numeroPecaJogador, numeroPecaOponente, tabuleiro);
                                }
                                else if(promover == true)
                                {
                                    promoverPeca(peca, &casa, numeroPecaJogador, numeroPecaOponente, tabuleiro);
                                    promover = false;
                                }

                                //Verificar se o rei JOGADOR está em check
                                acharPeca(reiJogador, tabuleiro, &posicaoMatriz, &posicaoInterface);
                                estaEmCheck = check(reiJogador[0], &posicaoMatriz, tabuleiro);
                                
                                if(estaEmCheck == true)
                                {
                                    atacante = peca[0];

                                    podeProtegerAtk = podeProtegerAtacando(atacante, &casa, tabuleiro);
                                    podeProtegerDef = podeProtegerDefendendo(atacante, &casa, &posicaoMatriz, quantidadeMaoJogador, quantidadeMaoOponente, tabuleiro);

                                    reiPodeMover = movimentosPossiveisRei(reiJogador[0], &posicaoMatriz, tabuleiro, casasPossiveis);
                                    podeInterceptar = podeProtegerInterceptando(atacante, &casa, &posicaoMatriz, tabuleiro);

                                    //Conferir se houve um checkmate
                                    if((podeProtegerAtk == false) && (podeProtegerDef == false) && (reiPodeMover == false) && (podeInterceptar == false))
                                    {
                                        checkMate = true;
                                    }
                                }

                                //O oponente realizou a jogada com sucesso
                                oponenteJogou = true;
                            }
                        }
                    }
                }

                Sleep(500);
                system("cls");
                
                renderizarTabuleiro(tabuleiro, quantidadeMaoJogador, quantidadeMaoOponente);
                
            } while (oponenteJogou == false);
        }
        oponenteJogou = false;
    }

    //Tela de gameOver
    printf("\n\n             CHECKMATE   \n\n");

    //Quem ganhou ?
    if(jogadorGanhou == true)
    {
        printf("        O JOGADOR VENCEU!\n");
    }
    else
    {
        printf("        O OPONENTE VENCEU!\n");
    }

    return 0;
}

/*
    Menu do jogo. (aparentemente o C não aceita quebra de linha dentro do código na função printf, portanto, tive de usar vários printf)
*/
void menu()
{
    int opcao;

    system("cls");

    do
    {
        //Opções
        printf("1) Como jogar\n");
        printf("2) Movimento das pecas\n");
        printf("3) Regras do jogo\n");
        printf("4) Sair do menu\n\n");

        scanf("%d", &opcao);

        switch (opcao)
        {
            //Como jogar
            case 1:
                system("cls");
                printf("                                                 Movimentar uma peca\n\n");

                printf("Quando aparecer \"Escolha a peca\", voce deve escolher uma de suas pecas que estah no tabuleiro para realizar o movimento,\n");
                printf("apos isso, uma mensagem \"Qual casa?\" serah exibida, para efetivamente realizar o movimento, voce deve passar a linha e\n");
                printf("a coluna da casa em que a peca deve ser deslocada (favor consultar a opcao \"Movimento das pecas\" para verificar os\n");
                printf("possiveis deslocamentos), para cancelar a acao, basta digitar um valor maior que 99.\n");
                printf("Sua rodada serah encerrada e o oponente poderah jogar.\n\n");

                printf("                                               Colocar uma peca em jogo\n\n");

                printf("Quando aparecer \"Escolha a peca\", voce pode escolher uma peca da sua mao (indicada no canto direito do tabuleiro) para \n");
                printf("colocar em jogo, para isso, basta digitar o nome da peca desejada (favor consultar a opcao \"Regras do jogo\" para o uso\n"); 
                printf("das pecas \"Pawn\", \"Knight\" e \"Lance\"). Quando a mensagem \"Qual casa?\" for exibida, basta digitar o valor da linha \n");
                printf("e da coluna da casa desejada (obviamente, a casa deve estar vazia), para cancelar a acao, basta digitar um valor maior que 99.\n");
                printf("Sua rodada serah encerrada e o oponente poderah jogar.\n\n");

                printf("                                                         Check\n\n");

                printf("Check eh o nome da jogada em que o rei pode ser capturado por uma peca do oponente na proxima rodada. Eh possivel realizar um check \n");
                printf("ao movimentar uma peca ou colocar uma peca em jogo de tal forma que o rei esteja em um dos campos de deslocamento possiveis da dita peca.\n\n");

                system("pause");
                system("cls");

                printf("                                               Se protegendo de um check\n\n");

                printf("Para sair de um check, existem 4 opcoes:\n\n");
                printf("1) Movimentar o rei para uma casa onde nao haja uma segunda peca que pode captura-lo (incluindo capturar a peca que estah dando check).\n");
                printf("2) Capturar a peca que estah dando check com alguma peca do tabuleiro.\n");
                printf("3) Usar uma peca da mao para interceptar o check (casos como \"Lance\", \"Rook/Dragon\" e \"Bishop/Horse\").\n");
                printf("4) Usar uma peca do tabuleiro para interceptar o check (casos como \"Lance\", \"Rook/Dragon\" e \"Bishop/Horse\").\n\n");

                printf("                                                          Pin\n\n");

                printf("Pin eh uma condicao em que uma peca nao pode se mover pois isso resultaria na possivel captura do rei, esse caso ocorre quando \n");
                printf("as pecas \"Lance\", \"Rook/Dragon\" ou \"Bishop/Horse\" poderiam realizar um check mas existe uma peca \"protegendo o rei\"\n");
                printf("na vertical/horizontal/diagonal, essa peca, portanto, estah \"pinada\".\n\n");

                printf("                                                       Promocao\n\n");

                printf("Ao chegar em uma das 3 ultimas linhas do tabuleiro, voce tem a opcao de promover uma peca desde que ela nao seja gold, king ou \n");
                printf("alguma peca promovida. Quando a condicao for atendida a mensagem \"Deseja promover a peca ?\" aparecerah, basta digitar S(para sim) \n");
                printf("ou N(para nao). Eh possivel promover entrando nas casas de promocao ou saindo delas.\n\n");

                printf("                                                      Como ganhar\n\n");

                printf("Para ganhar, eh necessario realizar um checkmate, ou seja, voce deve realizar uma jogada em que, independente das acoes que o \n");
                printf("oponente tome, o rei dele serah capturado na proxima rodada.\n\n");

                system("pause");
                break;

            //Movimento das peças
            case 2:
                system("cls");
                printf("     King\n");
                printf("| || || || || |\n");
                printf("| ||*||*||*|| |\n");
                printf("| ||*||K||*|| |\n");
                printf("| ||*||*||*|| |\n");
                printf("| || || || || |\n\n");

                printf("     Gold\n");
                printf("| || || || || |\n");
                printf("| ||*||*||*|| |\n");
                printf("| ||*||G||*|| |\n");
                printf("| || ||*|| || |\n");
                printf("| || || || || |\n\n");

                printf("     Silver              Zen(promoted silver)\n");
                printf("| || || || || |            | || || || || |\n");
                printf("| ||*||*||*|| |            | ||*||*||*|| |\n");
                printf("| || ||S|| || |            | ||*||Z||*|| |\n");
                printf("| ||*|| ||*|| |            | || ||*|| || |\n");
                printf("| || || || || |            | || || || || |\n\n");

                printf("     Pawn                Tokin(promoted pawn)\n");
                printf("| || || || || |            | || || || || |\n");
                printf("| || ||*|| || |            | ||*||*||*|| |\n");
                printf("| || ||P|| || |            | ||*||T||*|| |\n");
                printf("| || || || || |            | || ||*|| || |\n");
                printf("| || || || || |            | || || || || |\n\n");

                system("pause");
                system("cls");

                printf("     Knight             Ima(promoted knight)\n");
                printf("| || || || || |            | || || || || |\n");
                printf("| ||*|| ||*|| |            | ||*||*||*|| |\n");
                printf("| || || || || |            | ||*||I||*|| |\n");
                printf("| || ||K|| || |            | || ||*|| || |\n");
                printf("| || || || || |            | || || || || |\n\n");

                printf("     Lance             Narikyou(promoted lance)\n");
                printf("| || ||*|| || |            | || || || || |\n");
                printf("| || ||*|| || |            | ||*||*||*|| |\n");
                printf("| || ||*|| || |            | ||*||N||*|| |\n");
                printf("| || ||*|| || |            | || ||*|| || |\n");
                printf("| || ||L|| || |            | || || || || |\n");
                printf("Obs: A lanca se movimenta n casas para cima\n\n");

                printf("     Rook               Dragon(promoted rook)\n");
                printf("| || ||*|| || |            | || ||*|| || |\n");
                printf("| || ||*|| || |            | ||*||*||*|| |\n");
                printf("|*||*||R||*||*|            |*||*||D||*||*|\n");
                printf("| || ||*|| || |            | ||*||*||*|| |\n");
                printf("| || ||*|| || |            | || ||*|| || |\n");
                printf("Obs: A torre/dragao se movimenta n casas na horizontal e vertical\n\n");

                printf("     Bishop             Horse(promoted bishop)\n");
                printf("|*|| || || ||*|            |*|| || || ||*|\n");
                printf("| ||*|| ||*|| |            | ||*||*||*|| |\n");
                printf("| || ||B|| || |            | ||*||H||*|| |\n");
                printf("| ||*|| ||*|| |            | ||*||*||*|| |\n");
                printf("|*|| || || ||*|            |*|| || || ||*|\n");
                printf("Obs: O bispo/cavalo se movimenta n casas nas verticais\n\n");

                system("pause");
                break;

            //Regras do jogo
            case 3:
                system("cls");
                printf("                                                       Pawn\n\n");

                printf("Caso a peca se movimente ateh a ultima casa da coluna, ela serah  automaticamente promovida.\n");
                printf("Nao eh possivel colocar um peao em uma coluna onde jah exista algum peao.\n\n");

                printf("                                                      Lance\n\n");

                printf("Caso a peca se movimente ateh a ultima casa da coluna, ela serah automaticamente promovida.\n\n");

                printf("                                                      Knight\n\n");

                printf("Caso a peca se movimente ateh a ultima ou penultima casa da coluna, ela serah automaticamente promovida.\n\n");

                printf("                                            Capturando pecas promovidas\n\n");

                printf("Ao capturar uma peca promovida ela retorna ao estado nao promovido, ou seja, o jogador soh pode colocar na mesa pecas nao promovidas.\n\n");

                printf("                                                      Check\n\n");

                printf("Caso o rei esteja em check, o jogador eh obrigado a realizar uma jogada que retire o rei do check.\n\n");

                system("pause");
                break;

            //Sair do menu
            case 4:
                break;
            
            default:
                printf("\nOpcao Invalida!");
                Sleep(700);
                break;
        }
        system("cls");

    } while (opcao != 4);
}

/*
    Imprime o tabuleiro e a quantidade de peças capturadas.
*/
void renderizarTabuleiro(pecasTabuleiro renderizar[9][9], int maoJogador[7], int maoOponente[7])
{
    int i, j, z;

    //Imprimir Tabuleiro
    printf(" 9   8   7   6   5   4   3   2   1\n");
    printf(".__________________________________.\n");
    for (i = 0; i < 9; i++)
    {
        for (j = 0; j < 9; j++)
        {
            printf("|%s|", renderizar[i][j]);
        }
        //Números da vertical do tabuleiro
        printf("  %d", (i + 1));

        //Imprimir a mão do Oponente e Jogador
        if(i == 0)
        {
            printf("        b g k l p r s");
        }
        else if(i == 1)
        {
            printf("        ");
            for(z = 0; z < 7; z++)
            {
                printf("%d ", maoOponente[z]);
            }
        }
        else if(i == 7)
        {
            printf("        B G K L P R S");
        }
        else if(i == 8)
        {
            printf("        ");
            for(z = 0; z < 7; z++)
            {
                printf("%d ", maoJogador[z]);
            }
        }
        printf("\n");
    }
    printf("'\"\"\"\"\"\"\"\"\"\"\"\"\"\"\"\"\"\"\"\"\"\"\"\"\"\"\"\"\"\"\"\"\"\"'");
}

/*
    Essa função procura uma peça no tabuleiro, se existir ela retorna, nos parâmetros "posicaoMatriz" && "posicaoInterface", a posição
    na matriz tabuleiro e na matriz interface.
*/
bool acharPeca(char qualPeca[3], pecasTabuleiro tabuleiro[9][9], int *posicaoMatriz, int *posicaoInterface)
{
    bool pecaExiste = false;
    int i, j;

    //Achar a peça
    for(i = 0; i < 9; i++)
    {
        //Verificar se a peça já foi encontrada
        if(pecaExiste == false)
        {
            for(j = 0; j < 9; j++)
            {
                if(strcmp(qualPeca, tabuleiro[i][j]) == 0)
                {
                    pecaExiste = true;

                    *posicaoMatriz = (i * 10) + j;
                    *posicaoInterface = ((i + 1) * 10) + (9 - j);
                }
            }
        }
    }
    
    return pecaExiste;
}

/*
    Essa função procura um peão em uma coluna. Uma coluna só pode ter 1 peão.
    "posicaoPeca" == valor matriz tabuleiro.
*/
bool peaoColuna(char nomePeca, int *posicaoPeca, pecasTabuleiro tabuleiro[9][9])
{
    bool achou = false;
    int linhaPos, colunaPos;
    int i = 0;

    //Pegar a posição atual da peça (linha x coluna)
    colunaPos = *posicaoPeca % 10;
    linhaPos = *posicaoPeca / 10;

    //Jogador
    if(nomePeca == 'P')
    {
        //Conferir se existe algum peão na coluna
        while((i < 9) && (achou == false))
        {
            if(tabuleiro[i][colunaPos][0] == 'P')
            {
                achou = true;
            }
            i++;
        }
    }
    else if(nomePeca == 'p') //Oponente
    {
        //Conferir se existe algum peão na coluna
        while((i < 9) && (achou == false))
        {
            if(tabuleiro[i][colunaPos][0] == 'p')
            {
                achou = true;
            }
            i++;
        }
    }

    return achou;
}

/*
    Essa função confere as regras aplicadas às peças Pawn, Lance && Knight.
    "posicaoPeca" é tanto o lugar onde se quer mover quanto o lugar onde se quer colocar a peça.
    "posicaoPeca" == valor matriz tabuleiro.
*/
bool regrasPeca(char nomePeca, int *posicaoPeca, pecasTabuleiro tabuleiro[9][9])
{
    bool valido = true;
    int linhaPos, colunaPos;
    int i = 0;

    //Pegar a posição atual da peça (linha x coluna)
    colunaPos = *posicaoPeca % 10;
    linhaPos = *posicaoPeca / 10;

    //Pawn
    //Jogador
    if(nomePeca == 'P')
    {
        //Conferir se o movimento é ilegal
        if(linhaPos == 0)
        {
            valido = false;
        }
    }
    else if(nomePeca == 'p') //Oponente
    {
        //Conferir se o movimento é ilegal
        if(linhaPos == 8)
        {
            valido = false;
        }
    }

    //Lance
    //Jogador
    else if(nomePeca == 'L')
    {
        //Conferir se o movimento é ilegal
        if(linhaPos == 0)
        {
            valido = false;
        }
    }
    else if(nomePeca == 'l') //Oponente
    {
        //Conferir se o movimento é ilegal
        if(linhaPos == 8)
        {
            valido = false;
        }
    }

    //Knight
    //Jogador
    else if(nomePeca == 'K')
    {
        //Conferir se o movimento é ilegal
        if(linhaPos < 2)
        {
            valido = false;
        }
    }
    else if(nomePeca == 'k') //Oponente
    {
        //Conferir se o movimento é ilegal
        if(linhaPos > 6)
        {
            valido = false;
        }
    }

    return valido;
}

bool check(char rei, int *posicaoRei, pecasTabuleiro tabuleiro[9][9])
{
    int i, j;
    bool emCheck = false;
    char pecaAtacando;
    bool temPeca = false; //Para verificar horizontais, verticais e diagonais
    int linhaPos, colunaPos;

    //Pegar a posição atual da peça (linha x coluna)
    colunaPos = *posicaoRei % 10;
    linhaPos = *posicaoRei  / 10;

    /*
        //Peças que podem atacar o rei
        L K S G R B P T Z N I D H
    */
    /*
        //Jogador
        * * * * *
          * * *
        * * K * *
          * * *
        *   *   *
    */
    /*
        //Oponente
        *   *   *
          * * *
        * * K * *
          * * *
        * * * * *
    */
	
    //Conferir se é o jogador ou oponente
	switch(rei)
	{
        //Jogador
	    case 'K':
            //Cima
	        //Garantir que não tenha números fora do índice da matriz
		    if((linhaPos - 1) >= 0)
		    {
		        pecaAtacando = tabuleiro[linhaPos - 1][colunaPos][0];
                rei = tabuleiro[linhaPos - 1][colunaPos][1];
		
		        //Verificar se tem alguma peça inimiga
		        if((pecaAtacando > 'a') && (pecaAtacando <= 'z'))
		        {
		            //Verificar se a peça pode capturar o rei
		            if((pecaAtacando == 'l') || (pecaAtacando == 's') || (pecaAtacando == 'g') || (pecaAtacando == 'r') || (pecaAtacando == 'p') || (rei == 'k') ||
		               (pecaAtacando == 't') || (pecaAtacando == 'z') || (pecaAtacando == 'n') || (pecaAtacando == 'i') || (pecaAtacando == 'd') || (pecaAtacando == 'h'))
		            {
		                emCheck = true;
                        //Utilizar break para sair do switch case evitando conferir outras direções
		                break;
		            }
		        }
		    }
		    //Cima esquerda
		    //Garantir que não tenha números fora do índice da matriz
		    if(((linhaPos - 1) >= 0) && ((colunaPos - 1) >= 0))
		    {
		        pecaAtacando = tabuleiro[linhaPos - 1][colunaPos - 1][0];
                rei = tabuleiro[linhaPos - 1][colunaPos - 1][1];
		
		        //Verificar se tem alguma peça inimiga
		        if((pecaAtacando > 'a') && (pecaAtacando <= 'z'))
		        {
		            //Verificar se a peça pode capturar o rei
		            if((pecaAtacando == 's') || (pecaAtacando == 'g') || (pecaAtacando == 'b') || (pecaAtacando == 't') || (rei == 'k') || 
		               (pecaAtacando == 'z') || (pecaAtacando == 'n') || (pecaAtacando == 'i') || (pecaAtacando == 'd') || (pecaAtacando == 'h'))
		            {
		                emCheck = true;
		                break;
		            }
		        }
		    }
		    //Cima direita
		    //Garantir que não tenha números fora do índice da matriz
		    if(((linhaPos - 1) >= 0) && ((colunaPos + 1) < 9))
		    {
		        pecaAtacando = tabuleiro[linhaPos - 1][colunaPos + 1][0];
                rei = tabuleiro[linhaPos - 1][colunaPos + 1][1];
		
		        //Verificar se tem alguma peça inimiga
		        if((pecaAtacando > 'a') && (pecaAtacando <= 'z'))
		        {
		            //Verificar se a peça pode capturar o rei
		            if((pecaAtacando == 's') || (pecaAtacando == 'g') || (pecaAtacando == 'b') || (pecaAtacando == 't') || (rei == 'k') || 
		               (pecaAtacando == 'z') || (pecaAtacando == 'n') || (pecaAtacando == 'i') || (pecaAtacando == 'd') || (pecaAtacando == 'h'))
		            {
		                emCheck = true;
		                break;
		            }
		        }
		    }
		    //Esquerda
		    //Garantir que não tenha números fora do índice da matriz
		    if((colunaPos - 1) >= 0)
		    {
		        pecaAtacando = tabuleiro[linhaPos][colunaPos - 1][0];
                rei = tabuleiro[linhaPos][colunaPos - 1][1];
		
		        //Verificar se tem alguma peça inimiga
		        if((pecaAtacando > 'a') && (pecaAtacando <= 'z'))
		        {
		            //Verificar se a peça pode capturar o rei
		            if((pecaAtacando == 'g') || (pecaAtacando == 'r') || (pecaAtacando == 't') || (pecaAtacando == 'z') || (rei == 'k') || 
		               (pecaAtacando == 'n') || (pecaAtacando == 'i') || (pecaAtacando == 'd') || (pecaAtacando == 'h'))
		            {
		                emCheck = true;
		                break;
		            }
		        }
		    }
		    //Direita
		    //Garantir que não tenha números fora do índice da matriz
		    if((colunaPos + 1) < 9)
		    {
		        pecaAtacando = tabuleiro[linhaPos][colunaPos + 1][0];
                rei = tabuleiro[linhaPos][colunaPos + 1][1];
		
		        //Verificar se tem alguma peça inimiga
		        if((pecaAtacando > 'a') && (pecaAtacando <= 'z'))
		        {
		            //Verificar se a peça pode capturar o rei
		            if((pecaAtacando == 'g') || (pecaAtacando == 'r') || (pecaAtacando == 't') || (pecaAtacando == 'z') || (rei == 'k') || 
		               (pecaAtacando == 'n') || (pecaAtacando == 'i') || (pecaAtacando == 'd') || (pecaAtacando == 'h'))
		            {
		                emCheck = true;
		                break;
		            }
		        }
		    }
		    //Baixo
		    //Garantir que não tenha números fora do índice da matriz
		    if((linhaPos + 1) < 9)
		    {
		        pecaAtacando = tabuleiro[linhaPos + 1][colunaPos][0];
                rei = tabuleiro[linhaPos + 1][colunaPos][1];
		
		        //Verificar se tem alguma peça inimiga
		        if((pecaAtacando > 'a') && (pecaAtacando <= 'z'))
		        {
		            //Verificar se a peça pode capturar o rei
		            if((pecaAtacando == 'g') || (pecaAtacando == 'r') || (pecaAtacando == 't') || (pecaAtacando == 'z') || (rei == 'k') || 
		               (pecaAtacando == 'n') || (pecaAtacando == 'i') || (pecaAtacando == 'd') || (pecaAtacando == 'h'))
		            {
		                emCheck = true;
		                break;
		            }
		        }
		    }
		    //Baixo esquerda
		    //Garantir que não tenha números fora do índice da matriz
		    if(((linhaPos + 1) < 9) && ((colunaPos - 1) >= 0))
		    {
		        pecaAtacando = tabuleiro[linhaPos + 1][colunaPos - 1][0];
                rei = tabuleiro[linhaPos + 1][colunaPos - 1][1];
		
		        //Verificar se tem alguma peça inimiga
		        if((pecaAtacando > 'a') && (pecaAtacando <= 'z'))
		        {
		            //Verificar se a peça pode capturar o rei
		            if((pecaAtacando == 's') || (pecaAtacando == 'b') || (pecaAtacando == 'd') || (pecaAtacando == 'h') || (rei == 'k'))
		            {
		                emCheck = true;
		                break;
		            }
		        }
		    }
		    //Baixo direita
		    //Garantir que não tenha números fora do índice da matriz
		    if(((linhaPos + 1) < 9) && ((colunaPos + 1) < 9))
		    {
		        pecaAtacando = tabuleiro[linhaPos + 1][colunaPos + 1][0];
                rei = tabuleiro[linhaPos + 1][colunaPos + 1][1];
		
		        //Verificar se tem alguma peça inimiga
		        if((pecaAtacando > 'a') && (pecaAtacando <= 'z'))
		        {
		            //Verificar se a peça pode capturar o rei
		            if((pecaAtacando == 's') || (pecaAtacando == 'b') || (pecaAtacando == 'd') || (pecaAtacando == 'h') || (rei == 'k'))
		            {
		                emCheck = true;
		                break;
		            }
		        }
		    }
		    //Vertical superior
		    i = 1;
		        
		    //Tenta localizar rook, dragon ou lance
		    while(((linhaPos - i) >= 0) && (temPeca == false) && (emCheck == false))
		    {
		        pecaAtacando = tabuleiro[linhaPos - i][colunaPos][0];
                rei = tabuleiro[linhaPos - i][colunaPos][1];
		
		        //Verificar se tem rook, dragon ou lance
		        if((pecaAtacando == 'r') || (pecaAtacando == 'd') || (pecaAtacando == 'l'))
		        {
		            emCheck = true;
		            break;
		        }
                //Verificar se tem uma peça no caminho
		        else if(pecaAtacando != ' ' && rei != 'K')
		        {
		            temPeca = true;
		        }
		        i++;
		    }
		    
		    //Vertical inferior
		    i = 1;
            temPeca = false;
		
		    //Tenta localizar rook ou dragon
		    while(((linhaPos + i) < 9) && (temPeca == false) && (emCheck == false))
		    {
		        pecaAtacando = tabuleiro[linhaPos + i][colunaPos][0];
                rei = tabuleiro[linhaPos + i][colunaPos][1];
		
		        if((pecaAtacando == 'r') || (pecaAtacando == 'd'))
		        {
		            emCheck = true;
		            break;
		        }
                //Verificar se tem uma peça no caminho
		        else if(pecaAtacando != ' ' && rei != 'K')
		        {
		            temPeca = true;
		        }
		        i++;
		    }
		    
		    //Horizontal esquerda
		    j = 1;
            temPeca = false;
		
		    //Tenta localizar rook ou dragon
		    while(((colunaPos - j) >= 0) && (temPeca == false) && (emCheck == false))
		    {
		        pecaAtacando = tabuleiro[linhaPos][colunaPos - j][0];
                rei = tabuleiro[linhaPos][colunaPos - j][1];
		
		        if((pecaAtacando == 'r') || (pecaAtacando == 'd'))
		        {
		            emCheck = true;
		            break;
		        }
                //Verificar se tem uma peça no caminho
		        else if(pecaAtacando != ' ' && rei != 'K')
		        {
		            temPeca = true;
		        }
		        j++;
		    }
		    
		    //Horizontal direita
		    j = 1;
            temPeca = false;
		
		    //Tenta localizar rook ou dragon
		    while(((colunaPos + j) < 9) && (temPeca == false) && (emCheck == false))
		    {
		        pecaAtacando = tabuleiro[linhaPos][colunaPos + j][0];
                rei = tabuleiro[linhaPos][colunaPos + j][1];

		        if((pecaAtacando == 'r') || (pecaAtacando == 'd'))
		        {
		            emCheck = true;
		            break;
		        }
                //Verificar se tem uma peça no caminho
		        else if(pecaAtacando != ' ' && rei != 'K')
		        {
		            temPeca = true;
		        }
		        j++;
		    }
		    
		    //Diagonal esquerda superior
		    i = 1;
		    j = 1;
            temPeca = false;
		
		    //Tenta localizar bishop ou horse
		    while(((linhaPos - i) >= 0) && (temPeca == false) && (emCheck == false))
		    {
		        pecaAtacando = tabuleiro[linhaPos - i][colunaPos - j][0];
                rei = tabuleiro[linhaPos - i][colunaPos - j][1];
		
		        //Verificar se tem bishop ou horse
		        if((pecaAtacando == 'b') || (pecaAtacando == 'h'))
		        {
		            emCheck = true;
		            break;
		        }
                //Verificar se tem uma peça no caminho
		        else if(pecaAtacando != ' ' && rei != 'K')
		        {
		            temPeca = true;
		        }
		        i++;
		        j++;
		    }
		    
		    //Diagonal direita inferior
		    i = 1;
		    j = 1;
            temPeca = false;
		
		    //Tenta localizar bishop ou horse
		    while(((linhaPos + i) < 9) && (temPeca == false) && (emCheck == false))
		    {
		        pecaAtacando = tabuleiro[linhaPos + i][colunaPos + j][0];
                rei = tabuleiro[linhaPos + i][colunaPos + j][1];

		        //Verificar se tem bishop ou horse
		        if((pecaAtacando == 'b') || (pecaAtacando == 'h'))
		        {
		            emCheck = true;
		            break;
		        }
                //Verificar se tem uma peça no caminho
		        else if(pecaAtacando != ' ' && rei != 'K')
		        {
		            temPeca = true;
		        }
		        i++;
		        j++;
		    }
		    
		    //Diagonal direita superior
		    i = 1;
		    j = 1;
            temPeca = false;
		
		    //Tenta localizar bishop ou horse
		    while(((linhaPos - i) >= 0) && (temPeca == false) && (emCheck == false))
		    {
		        pecaAtacando = tabuleiro[linhaPos - i][colunaPos + j][0];
                rei = tabuleiro[linhaPos - i][colunaPos + j][1];

		        //Verificar se tem bishop ou horse
		        if((pecaAtacando == 'b') || (pecaAtacando == 'h'))
		        {
		            emCheck = true;
		            break;
		        }
                //Verificar se tem uma peça no caminho
		        else if(pecaAtacando != ' ' && rei != 'K')
		        {
		            temPeca = true;
		        }
		        i++;
		        j++;
		    }
		    
		    //Diagonal esquerda inferior
		    i = 1;
		    j = 1;
            temPeca = false;
		
		    //Tenta localizar bishop ou horse
		    while(((linhaPos + i) < 9) && (temPeca == false) && (emCheck == false))
		    {
		        pecaAtacando = tabuleiro[linhaPos + i][colunaPos - j][0];
                rei = tabuleiro[linhaPos + i][colunaPos - j][1];

		        //Verificar se tem bishop ou horse
		        if((pecaAtacando == 'b') || (pecaAtacando == 'h'))
		        {
		            emCheck = true;
		            break;
		        }
                //Verificar se tem uma peça no caminho
		        else if(pecaAtacando != ' ' && rei != 'K')
		        {
		            temPeca = true;
		        }
		        i++;
		        j++;
		    }
		    //Knight na esquerda
		    if(((linhaPos - 2) >= 0) && ((colunaPos - 1) >= 0))
		    {
		        //Verificar se não é rei inimigo (começa com a mesma palavra)
                rei = tabuleiro[linhaPos - 2][colunaPos - 1][1];
		
		        if(rei != 'k')
		        {
		            pecaAtacando = tabuleiro[linhaPos - 2][colunaPos - 1][0];
		
		            //Verificar se tem knight
		            if(pecaAtacando == 'k')
		            {
		                emCheck = true;
		                break;
		            }
		        }
		    }
		    //Knight na direita
		    if(((linhaPos - 2) >= 0) && ((colunaPos + 1) < 9))
		    {
		        //Verificar se não é rei inimigo (começa com a mesma palavra)
		        rei = tabuleiro[linhaPos - 2][colunaPos + 1][1];
		
		        if(rei != 'k')
		        {
		            pecaAtacando = tabuleiro[linhaPos - 2][colunaPos + 1][0];
		
		            //Verificar se tem knight
		            if(pecaAtacando == 'k')
		            {
		                emCheck = true;
		                break;
		            }
		        }
		    }
            break;

        //Oponente
	    case 'k':
            //Cima
	        //Garantir que não tenha números fora do índice da matriz
		    if((linhaPos - 1) >= 0)
		    {
		        pecaAtacando = tabuleiro[linhaPos - 1][colunaPos][0];
                rei = tabuleiro[linhaPos - 1][colunaPos][1];
		
		        //Verificar se tem alguma peça inimiga
		        if((pecaAtacando > 'A') && (pecaAtacando <= 'Z'))
		        {
		            //Verificar se a peça pode capturar o rei
		            if((pecaAtacando == 'L') || (pecaAtacando == 'S') || (pecaAtacando == 'G') || (pecaAtacando == 'R') || (pecaAtacando == 'P') || (rei == 'K') ||
		               (pecaAtacando == 'T') || (pecaAtacando == 'Z') || (pecaAtacando == 'N') || (pecaAtacando == 'I') || (pecaAtacando == 'D') || (pecaAtacando == 'H'))
		            {
		                emCheck = true;
                        //Utilizar break para sair do switch case evitando conferir outras direções
		                break;
		            }
		        }
		    }
		    //Cima esquerda
		    //Garantir que não tenha números fora do índice da matriz
		    if(((linhaPos - 1) >= 0) && ((colunaPos - 1) >= 0))
		    {
		        pecaAtacando = tabuleiro[linhaPos - 1][colunaPos - 1][0];
                rei = tabuleiro[linhaPos - 1][colunaPos - 1][1];
		
		        //Verificar se tem alguma peça inimiga
		        if((pecaAtacando > 'A') && (pecaAtacando <= 'Z'))
		        {
		            //Verificar se a peça pode capturar o rei
		            if((pecaAtacando == 'S') || (pecaAtacando == 'G') || (pecaAtacando == 'B') || (pecaAtacando == 'T') || (rei == 'K') || 
		               (pecaAtacando == 'Z') || (pecaAtacando == 'N') || (pecaAtacando == 'I') || (pecaAtacando == 'D') || (pecaAtacando == 'H'))
		            {
		                emCheck = true;
		                break;
		            }
		        }
		    }
		    //Cima direita
		    //Garantir que não tenha números fora do índice da matriz
		    if(((linhaPos - 1) >= 0) && ((colunaPos + 1) < 9))
		    {
		        pecaAtacando = tabuleiro[linhaPos - 1][colunaPos + 1][0];
                rei = tabuleiro[linhaPos - 1][colunaPos + 1][1];
		
		        //Verificar se tem alguma peça inimiga
		        if((pecaAtacando > 'A') && (pecaAtacando <= 'Z'))
		        {
		            //Verificar se a peça pode capturar o rei
		            if((pecaAtacando == 'S') || (pecaAtacando == 'G') || (pecaAtacando == 'B') || (pecaAtacando == 'T') || (rei == 'K') || 
		               (pecaAtacando == 'Z') || (pecaAtacando == 'N') || (pecaAtacando == 'I') || (pecaAtacando == 'D') || (pecaAtacando == 'H'))
		            {
		                emCheck = true;
		                break;
		            }
		        }
		    }
		    //Esquerda
		    //Garantir que não tenha números fora do índice da matriz
		    if((colunaPos - 1) >= 0)
		    {
		        pecaAtacando = tabuleiro[linhaPos][colunaPos - 1][0];
                rei = tabuleiro[linhaPos][colunaPos - 1][1];
		
		        //Verificar se tem alguma peça inimiga
		        if((pecaAtacando > 'A') && (pecaAtacando <= 'Z'))
		        {
		            //Verificar se a peça pode capturar o rei
		            if((pecaAtacando == 'G') || (pecaAtacando == 'R') || (pecaAtacando == 'T') || (pecaAtacando == 'Z') || (rei == 'K') || 
		               (pecaAtacando == 'N') || (pecaAtacando == 'I') || (pecaAtacando == 'D') || (pecaAtacando == 'H'))
		            {
		                emCheck = true;
		                break;
		            }
		        }
		    }
		    //Direita
		    //Garantir que não tenha números fora do índice da matriz
		    if((colunaPos + 1) < 9)
		    {
		        pecaAtacando = tabuleiro[linhaPos][colunaPos + 1][0];
                rei = tabuleiro[linhaPos][colunaPos + 1][1];
		
		        //Verificar se tem alguma peça inimiga
		        if((pecaAtacando > 'A') && (pecaAtacando <= 'Z'))
		        {
		            //Verificar se a peça pode capturar o rei
		            if((pecaAtacando == 'G') || (pecaAtacando == 'R') || (pecaAtacando == 'T') || (pecaAtacando == 'Z') || (rei == 'K') || 
		               (pecaAtacando == 'N') || (pecaAtacando == 'I') || (pecaAtacando == 'D') || (pecaAtacando == 'H'))
		            {
		                emCheck = true;
		                break;
		            }
		        }
		    }
		    //Baixo
		    //Garantir que não tenha números fora do índice da matriz
		    if((linhaPos + 1) < 9)
		    {
		        pecaAtacando = tabuleiro[linhaPos + 1][colunaPos][0];
                rei = tabuleiro[linhaPos + 1][colunaPos][1];
		
		        //Verificar se tem alguma peça inimiga
		        if((pecaAtacando > 'A') && (pecaAtacando <= 'Z'))
		        {
		            //Verificar se a peça pode capturar o rei
		            if((pecaAtacando == 'G') || (pecaAtacando == 'R') || (pecaAtacando == 'T') || (pecaAtacando == 'Z') || (rei == 'K') || 
		               (pecaAtacando == 'N') || (pecaAtacando == 'I') || (pecaAtacando == 'D') || (pecaAtacando == 'H'))
		            {
		                emCheck = true;
		                break;
		            }
		        }
		    }
		    //Baixo esquerda
		    //Garantir que não tenha números fora do índice da matriz
		    if(((linhaPos + 1) < 9) && ((colunaPos - 1) >= 0))
		    {
		        pecaAtacando = tabuleiro[linhaPos + 1][colunaPos - 1][0];
                rei = tabuleiro[linhaPos + 1][colunaPos - 1][1];
		
		        //Verificar se tem alguma peça inimiga
		        if((pecaAtacando > 'A') && (pecaAtacando <= 'Z'))
		        {
		            //Verificar se a peça pode capturar o rei
		            if((pecaAtacando == 'S') || (pecaAtacando == 'B') || (pecaAtacando == 'D') || (pecaAtacando == 'H') || (rei == 'K'))
		            {
		                emCheck = true;
		                break;
		            }
		        }
		    }
		    //Baixo direita
		    //Garantir que não tenha números fora do índice da matriz
		    if(((linhaPos + 1) < 9) && ((colunaPos + 1) < 9))
		    {
		        pecaAtacando = tabuleiro[linhaPos + 1][colunaPos + 1][0];
                rei = tabuleiro[linhaPos + 1][colunaPos + 1][1];
		
		        //Verificar se tem alguma peça inimiga
		        if((pecaAtacando > 'A') && (pecaAtacando <= 'Z'))
		        {
		            //Verificar se a peça pode capturar o rei
		            if((pecaAtacando == 'S') || (pecaAtacando == 'B') || (pecaAtacando == 'D') || (pecaAtacando == 'H') || (rei == 'K'))
		            {
		                emCheck = true;
		                break;
		            }
		        }
		    }
		    //Vertical superior
		    i = 1;
		        
		    //Tenta localizar rook, dragon ou lance
		    while(((linhaPos - i) >= 0) && (temPeca == false) && (emCheck == false))
		    {
		        pecaAtacando = tabuleiro[linhaPos - i][colunaPos][0];
                rei = tabuleiro[linhaPos - i][colunaPos][1];
		
		        //Verificar se tem rook, dragon ou lance
		        if((pecaAtacando == 'R') || (pecaAtacando == 'D') || (pecaAtacando == 'L'))
		        {
		            emCheck = true;
		            break;
		        }
                //Verificar se tem uma peça no caminho
		        else if(pecaAtacando != ' ' && rei != 'k')
		        {
		            temPeca = true;
		        }
		        i++;
		    }
		    
		    //Vertical inferior
		    i = 1;
            temPeca = false;
		
		    //Tenta localizar rook ou dragon
		    while(((linhaPos + i) < 9) && (temPeca == false) && (emCheck == false))
		    {
		        pecaAtacando = tabuleiro[linhaPos + i][colunaPos][0];
                rei = tabuleiro[linhaPos + i][colunaPos][1];
		
		        if((pecaAtacando == 'R') || (pecaAtacando == 'D'))
		        {
		            emCheck = true;
		            break;
		        }
                //Verificar se tem uma peça no caminho
		        else if(pecaAtacando != ' ' && rei != 'k')
		        {
		            temPeca = true;
		        }
		        i++;
		    }
		    
		    //Horizontal esquerda
		    j = 1;
            temPeca = false;
		
		    //Tenta localizar rook ou dragon
		    while(((colunaPos - j) >= 0) && (temPeca == false) && (emCheck == false))
		    {
		        pecaAtacando = tabuleiro[linhaPos][colunaPos - j][0];
                rei = tabuleiro[linhaPos][colunaPos - j][1];
		
		        if((pecaAtacando == 'R') || (pecaAtacando == 'D'))
		        {
		            emCheck = true;
		            break;
		        }
                //Verificar se tem uma peça no caminho
		        else if(pecaAtacando != ' ' && rei != 'k')
		        {
		            temPeca = true;
		        }
		        j++;
		    }
		    
		    //Horizontal direita
		    j = 1;
            temPeca = false;
		
		    //Tenta localizar rook ou dragon
		    while(((colunaPos + j) < 9) && (temPeca == false) && (emCheck == false))
		    {
		        pecaAtacando = tabuleiro[linhaPos][colunaPos + j][0];
                rei = tabuleiro[linhaPos][colunaPos + j][1];

		        if((pecaAtacando == 'R') || (pecaAtacando == 'D'))
		        {
		            emCheck = true;
		            break;
		        }
                //Verificar se tem uma peça no caminho
		        else if(pecaAtacando != ' ' && rei != 'k')
		        {
		            temPeca = true;
		        }
		        j++;
		    }
		    
		    //Diagonal esquerda superior
		    i = 1;
		    j = 1;
            temPeca = false;
		
		    //Tenta localizar bishop ou horse
		    while(((linhaPos - i) >= 0) && (temPeca == false) && (emCheck == false))
		    {
		        pecaAtacando = tabuleiro[linhaPos - i][colunaPos - j][0];
                rei = tabuleiro[linhaPos - i][colunaPos - j][1];
		
		        //Verificar se tem bishop ou horse
		        if((pecaAtacando == 'B') || (pecaAtacando == 'H'))
		        {
		            emCheck = true;
		            break;
		        }
                //Verificar se tem uma peça no caminho
		        else if(pecaAtacando != ' ' && rei != 'k')
		        {
		            temPeca = true;
		        }
		        i++;
		        j++;
		    }
		    
		    //Diagonal direita inferior
		    i = 1;
		    j = 1;
            temPeca = false;
		
		    //Tenta localizar bishop ou horse
		    while(((linhaPos + i) < 9) && (temPeca == false) && (emCheck == false))
		    {
		        pecaAtacando = tabuleiro[linhaPos + i][colunaPos + j][0];
                rei = tabuleiro[linhaPos + i][colunaPos + j][1];

		        //Verificar se tem bishop ou horse
		        if((pecaAtacando == 'B') || (pecaAtacando == 'H'))
		        {
		            emCheck = true;
		            break;
		        }
                //Verificar se tem uma peça no caminho
		        else if(pecaAtacando != ' ' && rei != 'k')
		        {
		            temPeca = true;
		        }
		        i++;
		        j++;
		    }
		    
		    //Diagonal direita superior
		    i = 1;
		    j = 1;
            temPeca = false;
		
		    //Tenta localizar bishop ou horse
		    while(((linhaPos - i) >= 0) && (temPeca == false) && (emCheck == false))
		    {
		        pecaAtacando = tabuleiro[linhaPos - i][colunaPos + j][0];
                rei = tabuleiro[linhaPos - i][colunaPos + j][1];

		        //Verificar se tem bishop ou horse
		        if((pecaAtacando == 'B') || (pecaAtacando == 'H'))
		        {
		            emCheck = true;
		            break;
		        }
                //Verificar se tem uma peça no caminho
		        else if(pecaAtacando != ' ' && rei != 'k')
		        {
		            temPeca = true;
		        }
		        i++;
		        j++;
		    }
		    
		    //Diagonal esquerda inferior
		    i = 1;
		    j = 1;
            temPeca = false;
		
		    //Tenta localizar bishop ou horse
		    while(((linhaPos + i) < 9) && (temPeca == false) && (emCheck == false))
		    {
		        pecaAtacando = tabuleiro[linhaPos + i][colunaPos - j][0];
                rei = tabuleiro[linhaPos + i][colunaPos - j][1];

		        //Verificar se tem bishop ou horse
		        if((pecaAtacando == 'B') || (pecaAtacando == 'H'))
		        {
		            emCheck = true;
		            break;
		        }
                //Verificar se tem uma peça no caminho
		        else if(pecaAtacando != ' ' && rei != 'k')
		        {
		            temPeca = true;
		        }
		        i++;
		        j++;
		    }
		    //Knight na esquerda
		    if(((linhaPos + 2) < 9) && ((colunaPos - 1) >= 0))
		    {
		        //Verificar se não é rei inimigo (começa com a mesma palavra)
                rei = tabuleiro[linhaPos + 2][colunaPos - 1][1];
		
		        if(rei != 'K')
		        {
		            pecaAtacando = tabuleiro[linhaPos + 2][colunaPos - 1][0];
		
		            //Verificar se tem knight
		            if(pecaAtacando == 'K')
		            {
		                emCheck = true;
		                break;
		            }
		        }
		    }
		    //Knight na direita
		    if(((linhaPos + 2) < 9) && ((colunaPos + 1) < 9))
		    {
		        //Verificar se não é rei inimigo (começa com a mesma palavra)
		        rei = tabuleiro[linhaPos + 2][colunaPos + 1][1];
		
		        if(rei != 'K')
		        {
		            pecaAtacando = tabuleiro[linhaPos + 2][colunaPos + 1][0];
		
		            //Verificar se tem knight
		            if(pecaAtacando == 'K')
		            {
		                emCheck = true;
		                break;
		            }
		        }
		    }
            break;

		default:
			break;
	}

    return emCheck;
}

/*
    Essa função coloca uma peça em jogo caso o usuário possua tal peça, bem como atribuir um número de peça disponível à peça.
*/
bool colocarPeca(char peca[3], int *casa, pecasTabuleiro tabuleiro[9][9], int quantidadeMaoJogador[7], char numeroPecaJogador[13][9], int quantidadeMaoOponente[7], char numeroPecaOponente[13][9])
{
    /*
        //Vetor
        0 1 2 3 4 5 6 
        B G K L P R S
    */
    /*
        //Matriz
        0 B
        1 G
        2 K
        3 L
        4 P
        5 R
        6 S
    */
    int j = 0;
    char numeroPeca;
    bool possui = false;
    bool achou = false;
    bool colocou = false;
    int linhaCasa, colunaCasa;
    int auxPeaoColuna; //Variável que terá o valor da casa convertido para a chamada da função peaoColuna

    //Converter "casa" para matriz tabuleiro
    colunaCasa = 9 - (*casa % 10);
    linhaCasa = (*casa / 10) - 1;

    auxPeaoColuna = linhaCasa * 10;
    auxPeaoColuna += colunaCasa;

    //Verificar se existe alguma peça no local escolhido
    if(tabuleiro[linhaCasa][colunaCasa][0] == ' ')
    {
        switch (toupper(peca[0]))
        {
            case 'B':
                //Jogador
                if(peca[0] == 'B')
                {
                    //Verificar se o jogador possui a peça
                    if(quantidadeMaoJogador[0] > 0)
                    {
                        possui = true;
                        quantidadeMaoJogador[0]--;

                        //Procurar um número de peça disponível   !!!!DANDO PROBLEMA!!!!
                        while((j < 2) && (achou == false))
                        {
                            if (numeroPecaJogador[0][j] > '0')
                            {
                                numeroPeca = numeroPecaJogador[0][j];
                                numeroPecaJogador[0][j] = '0';

                                achou = true;
                            }
                            j++;
                        }
                    }
                }
                else //Oponente
                {
                    //Verificar se o oponente possui a peça
                    if(quantidadeMaoOponente[0] > 0)
                    {
                        possui = true;
                        quantidadeMaoOponente[0]--;

                        //Procurar um número de peça disponível   !!!!DANDO PROBLEMA!!!!
                        while((j < 9) && (achou == false))
                        {
                            if (numeroPecaOponente[0][j] > '0')
                            {
                                numeroPeca = numeroPecaOponente[0][j];
                                numeroPecaOponente[0][j] = '0';

                                achou = true;
                            }
                            j++;
                        }
                    }
                }
                break;

            case 'G':
                //Jogador
                if(peca[0] == 'G')
                {
                    //Verificar se o jogador possui a peça
                    if(quantidadeMaoJogador[1] > 0)
                    {
                        possui = true;
                        quantidadeMaoJogador[1]--;

                        //Procurar um número de peça disponível
                        while((j < 9) && (achou == false))
                        {
                            if (numeroPecaJogador[1][j] > '0')
                            {
                                numeroPeca = numeroPecaJogador[1][j];
                                numeroPecaJogador[1][j] = '0';

                                achou = true;
                            }
                            j++;
                        }
                    }
                }
                else //Oponente
                {
                    //Verificar se o oponente possui a peça
                    if(quantidadeMaoOponente[1] > 0)
                    {
                        possui = true;
                        quantidadeMaoOponente[1]--;

                        //Procurar um número de peça disponível
                        while((j < 9) && (achou == false))
                        {
                            if (numeroPecaOponente[1][j] > '0')
                            {
                                numeroPeca = numeroPecaOponente[1][j];
                                numeroPecaOponente[1][j] = '0';

                                achou = true;
                            }
                            j++;
                        }
                    }
                }
                break;
            
            case 'K':
                //Jogador
                if(peca[0] == 'K')
                {
                    //Verificar se o jogador possui a peça
                    if(quantidadeMaoJogador[2] > 0)
                    {
                        //Conferir regra (pode chamar função "regrasPeca" mas não precisa)
                        if(linhaCasa > 1)
                        {
                            possui = true;
                            quantidadeMaoJogador[2]--;

                            //Procurar um número de peça disponível
                            while((j < 9) && (achou == false))
                            {
                                if (numeroPecaJogador[2][j] > '0')
                                {
                                    numeroPeca = numeroPecaJogador[2][j];
                                    numeroPecaJogador[2][j] = '0';

                                    achou = true;
                                }
                                j++;
                            }
                        }
                    }
                }
                else //Oponente
                {
                    //Verificar se o oponente possui a peça
                    if(quantidadeMaoOponente[2] > 0)
                    {
                        //Conferir regra (pode chamar função "regrasPeca" mas não precisa)
                        if(linhaCasa < 7)
                        {
                            possui = true;
                            quantidadeMaoOponente[2]--;

                            //Procurar um número de peça disponível
                            while((j < 9) && (achou == false))
                            {
                                if (numeroPecaOponente[2][j] > '0')
                                {
                                    numeroPeca = numeroPecaOponente[2][j];
                                    numeroPecaOponente[2][j] = '0';

                                    achou = true;
                                }
                                j++;
                            }
                        }
                    }
                }
                break;

            case 'L':
                //Jogador
                if(peca[0] == 'L')
                {
                    //Verificar se o jogador possui a peça
                    if(quantidadeMaoJogador[3] > 0)
                    {
                        //Conferir regra (pode chamar função "regrasPeca" mas não precisa)
                        if(linhaCasa != 0)
                        {
                            possui = true;
                            quantidadeMaoJogador[3]--;

                            //Procurar um número de peça disponível
                            while((j < 9) && (achou == false))
                            {
                                if (numeroPecaJogador[3][j] > '0')
                                {
                                    numeroPeca = numeroPecaJogador[3][j];
                                    numeroPecaJogador[3][j] = '0';

                                    achou = true;
                                }
                                j++;
                            }
                        }
                    }
                }
                else //Oponente
                {
                    //Verificar se o jogador possui a peça
                    if(quantidadeMaoOponente[3] > 0)
                    {
                        //Conferir regra (pode chamar função "regrasPeca" mas não precisa)
                        if(linhaCasa != 8)
                        {
                            possui = true;
                            quantidadeMaoOponente[3]--;

                            //Procurar um número de peça disponível
                            while((j < 9) && (achou == false))
                            {
                                if (numeroPecaOponente[3][j] > '0')
                                {
                                    numeroPeca = numeroPecaOponente[3][j];
                                    numeroPecaOponente[3][j] = '0';

                                    achou = true;
                                }
                                j++;
                            }
                        }
                    }
                }
                break;

            case 'P':
                //Jogador
                if(peca[0] == 'P')
                {
                    //Verificar se o jogador possui a peça
                    if(quantidadeMaoJogador[4] > 0)
                    {
                        //Conferir regra (pode chamar função "regrasPeca" mas não precisa)
                        if((linhaCasa != 0) && (peaoColuna(peca[0], &auxPeaoColuna, tabuleiro) == false))
                        {
                            possui = true;
                            quantidadeMaoJogador[4]--;

                            //Procurar um número de peça disponível
                            while((j < 9) && (achou == false))
                            {
                                if (numeroPecaJogador[4][j] > '0')
                                {
                                    numeroPeca = numeroPecaJogador[4][j];
                                    numeroPecaJogador[4][j] = '0';

                                    achou = true;
                                }
                                j++;
                            }
                        }
                    }
                }
                else //Oponente
                {
                    //Verificar se o oponente possui a peça
                    if(quantidadeMaoOponente[4] > 0)
                    {
                        //Conferir regra (pode chamar função "regrasPeca" mas não precisa)
                        if((linhaCasa != 8) && (peaoColuna(peca[0], &auxPeaoColuna, tabuleiro) == false))
                        {
                            possui = true;
                            quantidadeMaoOponente[4]--;

                            //Procurar um número de peça disponível
                            while((j < 9) && (achou == false))
                            {
                                if (numeroPecaOponente[4][j] > '0')
                                {
                                    numeroPeca = numeroPecaOponente[4][j];
                                    numeroPecaOponente[4][j] = '0';

                                    achou = true;
                                }
                                j++;
                            }
                        }
                    }
                }
                break;

            case 'R':
                //Jogador
                if(peca[0] == 'R')
                {
                    //Verificar se o jogador possui a peça
                    if(quantidadeMaoJogador[5] > 0)
                    {
                        possui = true;
                        quantidadeMaoJogador[5]--;

                        //Procurar um número de peça disponível
                        while((j < 9) && (achou == false))
                        {
                            if (numeroPecaJogador[5][j] > '0')
                            {
                                numeroPeca = numeroPecaJogador[5][j];
                                numeroPecaJogador[5][j] = '0';

                                achou = true;
                            }
                            j++;
                        }
                    }
                }
                else //Oponente
                {
                    //Verificar se o oponente possui a peça
                    if(quantidadeMaoOponente[5] > 0)
                    {
                        possui = true;
                        quantidadeMaoOponente[5]--;

                        //Procurar um número de peça disponível
                        while((j < 9) && (achou == false))
                        {
                            if (numeroPecaOponente[5][j] > '0')
                            {
                                numeroPeca = numeroPecaOponente[5][j];
                                numeroPecaOponente[5][j] = '0';

                                achou = true;
                            }
                            j++;
                        }
                    }
                }
                break;

            case 'S':
                //Jogador
                if(peca[0] == 'S')
                {
                    //Verificar se o jogador possui a peça
                    if(quantidadeMaoJogador[6] > 0)
                    {
                        possui = true;
                        quantidadeMaoJogador[6]--;

                        //Procurar um número de peça disponível
                        while((j < 9) && (achou == false))
                        {
                            if (numeroPecaJogador[6][j] > '0')
                            {
                                numeroPeca = numeroPecaJogador[6][j];
                                numeroPecaJogador[6][j] = '0';

                                achou = true;
                            }
                            j++;
                        }
                    }
                }
                else //Oponente
                {
                    //Verificar se o oponente possui a peça
                    if(quantidadeMaoOponente[6] > 0)
                    {
                        possui = true;
                        quantidadeMaoOponente[6]--;

                        //Procurar um número de peça disponível
                        while((j < 9) && (achou == false))
                        {
                            if (numeroPecaOponente[6][j] > '0')
                            {
                                numeroPeca = numeroPecaOponente[6][j];
                                numeroPecaOponente[6][j] = '0';

                                achou = true;
                            }
                            j++;
                        }
                    }
                }
                break;
            
            default:
                printf("\nA peca não existe!\n");
                break;
        }

        if(possui == false)
        {
            printf("\nVoce nao possui essa peca OU a casa infringe as regras do jogo!\n");
        }
        else
        {
            //Colocar a peça
            tabuleiro[linhaCasa][colunaCasa][0] = peca[0];
            tabuleiro[linhaCasa][colunaCasa][1] = numeroPeca;

            colocou = true;
        }
    }
    else
    {
        printf("\nExiste uma peca na posicao!\n");
    }

    return colocou;
}

/*
    Essa função adiciona a peça capturada à mão do usuário e libera o uso do numero da dita peça para quando houver a chamada da função
    colocarPeca.
*/
void capturarPeca(char peca[3], int quantidadeMaoJogador[7], int quantidadeMaoOponente[7], char numeroPecaJogador[13][9], char numeroPecaOponente[13][9])
{
    /*
        //Vetor
        0 1 2 3 4 5 6 
        B G K L P R S
    */
    /*
        //Matriz
        0 B
        1 G
        2 K
        3 L
        4 P
        5 R
        6 S
        //Promovidos
        7 T
        8 Z
        9 N
        10 I
        11 D
        12 H
    */
    int i = 0;
    bool achou = false;

    switch (toupper(peca[0]))
    {
        //Bishop
        case 'B':
            //O jogador capturou
            if(peca[0] == 'b')
            {
                //Adiciona 1 à quantidade de peças
                quantidadeMaoJogador[0]++;

                //Adiciona o número da peça capturada ao numero de peça disponíveis
                while((i < 2) && (achou == false))
                {
                    if(numeroPecaOponente[0][i] == '0')
                    {
                        numeroPecaOponente[0][i] = peca[1];
                        achou = true;
                    }
                    i++;
                }
            }
            else //Oponente capturou
            {
                //Adiciona 1 à quantidade de peças
                quantidadeMaoOponente[0]++;

                //Adiciona o número da peça capturada ao numero de peça disponíveis
                while((i < 2) && (achou == false))
                {
                    if(numeroPecaJogador[0][i] == '0')
                    {
                        numeroPecaJogador[0][i] = peca[1];
                        achou = true;
                    }
                    i++;
                }
            }
            break;

        //Rook
        case 'R':
            //O jogador capturou
            if(peca[0] == 'r')
            {
                //Adiciona 1 à quantidade de peças
                quantidadeMaoJogador[5]++;

                //Adiciona o número da peça capturada ao numero de peça disponíveis
                while((i < 2) && (achou == false))
                {
                    if(numeroPecaOponente[5][i] == '0')
                    {
                        numeroPecaOponente[5][i] = peca[1];
                        achou = true;
                    }
                    i++;
                }
            }
            else //Oponente capturou
            {
                //Adiciona 1 à quantidade de peças
                quantidadeMaoOponente[5]++;

                //Adiciona o número da peça capturada ao numero de peça disponíveis
                while((i < 2) && (achou == false))
                {
                    if(numeroPecaJogador[5][i] == '0')
                    {
                        numeroPecaJogador[5][i] = peca[1];
                        achou = true;
                    }
                    i++;
                }
            }
            break;

        //Gold
        case 'G':
            //O jogador capturou
            if(peca[0] == 'g')
            {
                //Adiciona 1 à quantidade de peças
                quantidadeMaoJogador[1]++;

                //Adiciona o número da peça capturada ao numero de peça disponíveis
                while((i < 4) && (achou == false))
                {
                    if(numeroPecaOponente[1][i] == '0')
                    {
                        numeroPecaOponente[1][i] = peca[1];
                        achou = true;
                    }
                    i++;
                }
            }
            else //Oponente capturou
            {
                //Adiciona 1 à quantidade de peças
                quantidadeMaoOponente[1]++;

                //Adiciona o número da peça capturada ao numero de peça disponíveis
                while((i < 4) && (achou == false))
                {
                    if(numeroPecaJogador[1][i] == '0')
                    {
                        numeroPecaJogador[1][i] = peca[1];
                        achou = true;
                    }
                    i++;
                }
            }
            break;

        //Knight
        case 'K':
            //O jogador capturou
            if(peca[0] == 'k')
            {
                //Adiciona 1 à quantidade de peças
                quantidadeMaoJogador[2]++;

                //Adiciona o número da peça capturada ao numero de peça disponíveis
                while((i < 4) && (achou == false))
                {
                    if(numeroPecaOponente[2][i] == '0')
                    {
                        numeroPecaOponente[2][i] = peca[1];
                        achou = true;
                    }
                    i++;
                }
            }
            else //Oponente capturou
            {
                //Adiciona 1 à quantidade de peças
                quantidadeMaoOponente[2]++;

                //Adiciona o número da peça capturada ao numero de peça disponíveis
                while((i < 4) && (achou == false))
                {
                    if(numeroPecaJogador[2][i] == '0')
                    {
                        numeroPecaJogador[2][i] = peca[1];
                        achou = true;
                    }
                    i++;
                }
            }
            break;

        //Lance
        case 'L':
            //O jogador capturou
            if(peca[0] == 'l')
            {
                //Adiciona 1 à quantidade de peças
                quantidadeMaoJogador[3]++;

                //Adiciona o número da peça capturada ao numero de peça disponíveis
                while((i < 4) && (achou == false))
                {
                    if(numeroPecaOponente[3][i] == '0')
                    {
                        numeroPecaOponente[3][i] = peca[1];
                        achou = true;
                    }
                    i++;
                }
            }
            else //Oponente capturou
            {
                //Adiciona 1 à quantidade de peças
                quantidadeMaoOponente[3]++;

                //Adiciona o número da peça capturada ao numero de peça disponíveis
                while((i < 4) && (achou == false))
                {
                    if(numeroPecaJogador[3][i] == '0')
                    {
                        numeroPecaJogador[3][i] = peca[1];
                        achou = true;
                    }
                    i++;
                }
            }
            break;

        //Silver
        case 'S':
            //O jogador capturou
            if(peca[0] == 's')
            {
                //Adiciona 1 à quantidade de peças
                quantidadeMaoJogador[6]++;

                //Adiciona o número da peça capturada ao numero de peça disponíveis
                while((i < 4) && (achou == false))
                {
                    if(numeroPecaOponente[6][i] == '0')
                    {
                        numeroPecaOponente[6][i] = peca[1];
                        achou = true;
                    }
                    i++;
                }
            }
            else //Oponente capturou
            {
                //Adiciona 1 à quantidade de peças
                quantidadeMaoOponente[6]++;

                //Adiciona o número da peça capturada ao numero de peça disponíveis
                while((i < 4) && (achou == false))
                {
                    if(numeroPecaJogador[6][i] == '0')
                    {
                        numeroPecaJogador[6][i] = peca[1];
                        achou = true;
                    }
                    i++;
                }
            }
            break;

        //Pawn
        case 'P':
            //O jogador capturou
            if(peca[0] == 'p')
            {
                //Adiciona 1 à quantidade de peças
                quantidadeMaoJogador[4]++;

                //Adiciona o número da peça capturada ao numero de peça disponíveis
                while((i < 9) && (achou == false))
                {
                    if(numeroPecaOponente[4][i] == '0')
                    {
                        numeroPecaOponente[4][i] = peca[1];
                        achou = true;
                    }
                    i++;
                }
            }
            else //Oponente capturou
            {
                //Adiciona 1 à quantidade de peças
                quantidadeMaoOponente[4]++;

                //Adiciona o número da peça capturada ao numero de peça disponíveis
                while((i < 9) && (achou == false))
                {
                    if(numeroPecaJogador[4][i] == '0')
                    {
                        numeroPecaJogador[4][i] = peca[1];
                        achou = true;
                    }
                    i++;
                }
            }
            break;

        //Tokin (Promoted Pawn)
        case 'T':
            //O jogador capturou
            if(peca[0] == 't')
            {
                //Adiciona 1 à quantidade de peças
                quantidadeMaoJogador[4]++;

                //Adiciona o número da peça capturada ao numero de peça disponíveis
                while((i < 9) && (achou == false))
                {
                    if(numeroPecaOponente[7][i] == '0')
                    {
                        numeroPecaOponente[7][i] = peca[1];
                        achou = true;
                    }
                    i++;
                }
            }
            else //Oponente capturou
            {
                //Adiciona 1 à quantidade de peças
                quantidadeMaoOponente[4]++;

                //Adiciona o número da peça capturada ao numero de peça disponíveis
                while((i < 9) && (achou == false))
                {
                    if(numeroPecaJogador[7][i] == '0')
                    {
                        numeroPecaJogador[7][i] = peca[1];
                        achou = true;
                    }
                    i++;
                }
            }
            break;

        //Zen (Promoted Silver)
        case 'Z':
            //O jogador capturou
            if(peca[0] == 'z')
            {
                //Adiciona 1 à quantidade de peças
                quantidadeMaoJogador[6]++;

                //Adiciona o número da peça capturada ao numero de peça disponíveis
                while((i < 4) && (achou == false))
                {
                    if(numeroPecaOponente[8][i] == '0')
                    {
                        numeroPecaOponente[8][i] = peca[1];
                        achou = true;
                    }
                    i++;
                }
            }
            else //Oponente capturou
            {
                //Adiciona 1 à quantidade de peças
                quantidadeMaoOponente[6]++;

                //Adiciona o número da peça capturada ao numero de peça disponíveis
                while((i < 4) && (achou == false))
                {
                    if(numeroPecaJogador[8][i] == '0')
                    {
                        numeroPecaJogador[8][i] = peca[1];
                        achou = true;
                    }
                    i++;
                }
            }
            break;

        //Narikyou (Promoted Lance)
        case 'N':
            //O jogador capturou
            if(peca[0] == 'n')
            {
                //Adiciona 1 à quantidade de peças
                quantidadeMaoJogador[3]++;

                //Adiciona o número da peça capturada ao numero de peça disponíveis
                while((i < 4) && (achou == false))
                {
                    if(numeroPecaOponente[9][i] == '0')
                    {
                        numeroPecaOponente[9][i] = peca[1];
                        achou = true;
                    }
                    i++;
                }
            }
            else //Oponente capturou
            {
                //Adiciona 1 à quantidade de peças
                quantidadeMaoOponente[3]++;

                //Adiciona o número da peça capturada ao numero de peça disponíveis
                while((i < 4) && (achou == false))
                {
                    if(numeroPecaJogador[9][i] == '0')
                    {
                        numeroPecaJogador[9][i] = peca[1];
                        achou = true;
                    }
                    i++;
                }
            }
            break;

        //Ima (Promoted Knight)
        case 'I':
            //O jogador capturou
            if(peca[0] == 'i')
            {
                //Adiciona 1 à quantidade de peças
                quantidadeMaoJogador[2]++;

                //Adiciona o número da peça capturada ao numero de peça disponíveis
                while((i < 4) && (achou == false))
                {
                    if(numeroPecaOponente[10][i] == '0')
                    {
                        numeroPecaOponente[10][i] = peca[1];
                        achou = true;
                    }
                    i++;
                }
            }
            else //Oponente capturou
            {
                //Adiciona 1 à quantidade de peças
                quantidadeMaoOponente[2]++;

                //Adiciona o número da peça capturada ao numero de peça disponíveis
                while((i < 4) && (achou == false))
                {
                    if(numeroPecaJogador[10][i] == '0')
                    {
                        numeroPecaJogador[10][i] = peca[1];
                        achou = true;
                    }
                    i++;
                }
            }
            break;

        //Dragon (Promoted Rook)
        case 'D':
            //O jogador capturou
            if(peca[0] == 'd')
            {
                //Adiciona 1 à quantidade de peças
                quantidadeMaoJogador[5]++;

                //Adiciona o número da peça capturada ao numero de peça disponíveis
                while((i < 2) && (achou == false))
                {
                    if(numeroPecaOponente[11][i] == '0')
                    {
                        numeroPecaOponente[11][i] = peca[1];
                        achou = true;
                    }
                    i++;
                }
            }
            else //Oponente capturou
            {
                //Adiciona 1 à quantidade de peças
                quantidadeMaoOponente[5]++;

                //Adiciona o número da peça capturada ao numero de peça disponíveis
                while((i < 2) && (achou == false))
                {
                    if(numeroPecaJogador[11][i] == '0')
                    {
                        numeroPecaJogador[11][i] = peca[1];
                        achou = true;
                    }
                    i++;
                }
            }
            break;

        //Horse (Promoted Bishop)
        case 'H':
            //O jogador capturou
            if(peca[0] == 'h')
            {
                //Adiciona 1 à quantidade de peças
                quantidadeMaoJogador[0]++;

                //Adiciona o número da peça capturada ao numero de peça disponíveis
                while((i < 2) && (achou == false))
                {
                    if(numeroPecaOponente[12][i] == '0')
                    {
                        numeroPecaOponente[12][i] = peca[1];
                        achou = true;
                    }
                    i++;
                }
            }
            else //Oponente capturou
            {
                //Adiciona 1 à quantidade de peças
                quantidadeMaoOponente[0]++;

                //Adiciona o número da peça capturada ao numero de peça disponíveis
                while((i < 2) && (achou == false))
                {
                    if(numeroPecaJogador[12][i] == '0')
                    {
                        numeroPecaJogador[12][i] = peca[1];
                        achou = true;
                    }
                    i++;
                }
            }
            break;
        
        default:
            printf("\nErro!\n");
            break;
    }
}

/*
    Essa função transforma a peça em sua forma promovida e adiciona um número único à nova peça.
*/
void promoverPeca(char peca[3], int *casa, char numeroPecaJogador[13][9], char numeroPecaOponente[13][9], pecasTabuleiro tabuleiro[9][9])
{
    /*
        //Matriz
        0 B
        1 G
        2 K
        3 L
        4 P
        5 R
        6 S
        //Promovidos
        7 T
        8 Z
        9 N
        10 I
        11 D
        12 H
    */
    int j = 0;
    bool achou;
    int colunaCasa, linhaCasa;

    //Converter "casa" para matriz tabuleiro
    colunaCasa = 9 - (*casa % 10);
    linhaCasa = (*casa / 10) - 1;

    switch (toupper(peca[0]))
    {
        case 'B':
            //Jogador
            if(peca[0] == 'B')
            {
                //Adicionar o número de peça da peça alterada
                while((j < 2) && (achou == false))
                {
                    if(numeroPecaJogador[0][j] == '0')
                    {
                        numeroPecaJogador[0][j] = peca[1];
                        achou = true;
                    }
                    j++;
                }

                //Alterar o nome da peça
                peca[0] = 'H';

                //Buscar um número de peça
                j = 0;
                achou = false;

                while((j < 2) && (achou == false))
                {
                    if(numeroPecaJogador[12][j] != '0')
                    {
                        peca[1] = numeroPecaJogador[12][j];
                        numeroPecaJogador[12][j] = '0';

                        achou = true;
                    }
                    j++;
                }
            }
            else //Oponente
            {
                //Adicionar o número de peça da peça alterada
                while((j < 2) && (achou == false))
                {
                    if(numeroPecaOponente[0][j] == '0')
                    {
                        numeroPecaOponente[0][j] = peca[1];
                        achou = true;
                    }
                    j++;
                }

                //Alterar o nome da peça
                peca[0] = 'h';

                //Buscar um número de peça
                j = 0;
                achou = false;

                while((j < 2) && (achou == false))
                {
                    if(numeroPecaOponente[12][j] != '0')
                    {
                        peca[1] = numeroPecaOponente[12][j];
                        numeroPecaOponente[12][j] = '0';

                        achou = true;
                    }
                    j++;
                }
            }
            break;

        case 'K':
            //Jogador
            if(peca[0] == 'K')
            {
                //Adicionar o número de peça da peça alterada
                while((j < 4) && (achou == false))
                {
                    if(numeroPecaJogador[2][j] == '0')
                    {
                        numeroPecaJogador[2][j] = peca[1];
                        achou = true;
                    }
                    j++;
                }

                //Alterar o nome da peça
                peca[0] = 'I';

                //Buscar um número de peça
                j = 0;
                achou = false;

                while((j < 4) && (achou == false))
                {
                    if(numeroPecaJogador[10][j] != '0')
                    {
                        peca[1] = numeroPecaJogador[10][j];
                        numeroPecaJogador[10][j] = '0';

                        achou = true;
                    }
                    j++;
                }
            }
            else //Oponente
            {
                //Adicionar o número de peça da peça alterada
                while((j < 4) && (achou == false))
                {
                    if(numeroPecaOponente[2][j] == '0')
                    {
                        numeroPecaOponente[2][j] = peca[1];
                        achou = true;
                    }
                    j++;
                }

                //Alterar o nome da peça
                peca[0] = 'i';

                //Buscar um número de peça
                j = 0;
                achou = false;

                while((j < 4) && (achou == false))
                {
                    if(numeroPecaOponente[10][j] != '0')
                    {
                        peca[1] = numeroPecaOponente[10][j];
                        numeroPecaOponente[10][j] = '0';

                        achou = true;
                    }
                    j++;
                }
            }
            break;

        case 'L':
            //Jogador
            if(peca[0] == 'L')
            {
                //Adicionar o número de peça da peça alterada
                while((j < 4) && (achou == false))
                {
                    if(numeroPecaJogador[3][j] == '0')
                    {
                        numeroPecaJogador[3][j] = peca[1];
                        achou = true;
                    }
                    j++;
                }

                //Alterar o nome da peça
                peca[0] = 'N';

                //Buscar um número de peça
                j = 0;
                achou = false;

                while((j < 4) && (achou == false))
                {
                    if(numeroPecaJogador[9][j] != '0')
                    {
                        peca[1] = numeroPecaJogador[9][j];
                        numeroPecaJogador[9][j] = '0';

                        achou = true;
                    }
                    j++;
                }
            }
            else //Oponente
            {
                //Adicionar o número de peça da peça alterada
                while((j < 4) && (achou == false))
                {
                    if(numeroPecaOponente[3][j] == '0')
                    {
                        numeroPecaOponente[3][j] = peca[1];
                        achou = true;
                    }
                    j++;
                }

                //Alterar o nome da peça
                peca[0] = 'n';

                //Buscar um número de peça
                j = 0;
                achou = false;

                while((j < 4) && (achou == false))
                {
                    if(numeroPecaOponente[9][j] != '0')
                    {
                        peca[1] = numeroPecaOponente[9][j];
                        numeroPecaOponente[9][j] = '0';

                        achou = true;
                    }
                    j++;
                }
            }
            break;

        case 'P':
            //Jogador
            if(peca[0] == 'P')
            {
                //Adicionar o número de peça da peça alterada
                while((j < 9) && (achou == false))
                {
                    if(numeroPecaJogador[4][j] == '0')
                    {
                        numeroPecaJogador[4][j] = peca[1];
                        achou = true;
                    }
                    j++;
                }

                //Alterar o nome da peça
                peca[0] = 'T';

                //Buscar um número de peça
                j = 0;
                achou = false;

                while((j < 9) && (achou == false))
                {
                    if(numeroPecaJogador[7][j] != '0')
                    {
                        peca[1] = numeroPecaJogador[7][j];
                        numeroPecaJogador[7][j] = '0';

                        achou = true;
                    }
                    j++;
                }
            }
            else //Oponente
            {
                //Adicionar o número de peça da peça alterada
                while((j < 9) && (achou == false))
                {
                    if(numeroPecaOponente[4][j] == '0')
                    {
                        numeroPecaOponente[4][j] = peca[1];
                        achou = true;
                    }
                    j++;
                }

                //Alterar o nome da peça
                peca[0] = 't';

                //Buscar um número de peça
                j = 0;
                achou = false;

                while((j < 9) && (achou == false))
                {
                    if(numeroPecaOponente[7][j] != '0')
                    {
                        peca[1] = numeroPecaOponente[7][j];
                        numeroPecaOponente[7][j] = '0';

                        achou = true;
                    }
                    j++;
                }
            }
            break;

        case 'R':
            //Jogador
            if(peca[0] == 'R')
            {
                //Adicionar o número de peça da peça alterada
                while((j < 2) && (achou == false))
                {
                    if(numeroPecaJogador[5][j] == '0')
                    {
                        numeroPecaJogador[5][j] = peca[1];
                        achou = true;
                    }
                    j++;
                }

                //Alterar o nome da peça
                peca[0] = 'D';

                //Buscar um número de peça
                j = 0;
                achou = false;

                while((j < 2) && (achou == false))
                {
                    if(numeroPecaJogador[11][j] != '0')
                    {
                        peca[1] = numeroPecaJogador[11][j];
                        numeroPecaJogador[11][j] = '0';

                        achou = true;
                    }
                    j++;
                }
            }
            else //Oponente
            {
                //Adicionar o número de peça da peça alterada
                while((j < 2) && (achou == false))
                {
                    if(numeroPecaOponente[5][j] == '0')
                    {
                        numeroPecaOponente[5][j] = peca[1];
                        achou = true;
                    }
                    j++;
                }

                //Alterar o nome da peça
                peca[0] = 'd';

                //Buscar um número de peça
                j = 0;
                achou = false;

                while((j < 2) && (achou == false))
                {
                    if(numeroPecaOponente[11][j] != '0')
                    {
                        peca[1] = numeroPecaOponente[11][j];
                        numeroPecaOponente[11][j] = '0';

                        achou = true;
                    }
                    j++;
                }
            }
            break;

        case 'S':
            //Jogador
            if(peca[0] == 'S')
            {
                //Adicionar o número de peça da peça alterada
                while((j < 4) && (achou == false))
                {
                    if(numeroPecaJogador[6][j] == '0')
                    {
                        numeroPecaJogador[6][j] = peca[1];
                        achou = true;
                    }
                    j++;
                }

                //Alterar o nome da peça
                peca[0] = 'Z';

                //Buscar um número de peça
                j = 0;
                achou = false;

                while((j < 4) && (achou == false))
                {
                    if(numeroPecaJogador[8][j] != '0')
                    {
                        peca[1] = numeroPecaJogador[8][j];
                        numeroPecaJogador[8][j] = '0';

                        achou = true;
                    }
                    j++;
                }
            }
            else //Oponente
            {
                //Adicionar o número de peça da peça alterada
                while((j < 4) && (achou == false))
                {
                    if(numeroPecaOponente[6][j] == '0')
                    {
                        numeroPecaOponente[6][j] = peca[1];
                        achou = true;
                    }
                    j++;
                }

                //Alterar o nome da peça
                peca[0] = 'z';

                //Buscar um número de peça
                j = 0;
                achou = false;

                while((j < 4) && (achou == false))
                {
                    if(numeroPecaOponente[8][j] != '0')
                    {
                        peca[1] = numeroPecaOponente[8][j];
                        numeroPecaOponente[8][j] = '0';

                        achou = true;
                    }
                    j++;
                }
            }
            break;
        
        default:
            printf("\nErro Promoção!\n");
            break;
    }

    //Colocar a peça promovida
    tabuleiro[linhaCasa][colunaCasa][0] = peca[0];
    tabuleiro[linhaCasa][colunaCasa][1] = peca[1];
}

/*
    Essa função movimenta a peça, verifica se houve uma captura (retornando a peça capturada na variável "capturado[3]") 
    e se deve ocorrer uma promoção (retornando verdadeiro no ponteiro "promover").
*/
bool movimentarPeca(char peca[3], int *casa, int *posicaoMatriz, int *posicaoInterface, pecasTabuleiro tabuleiro[9][9], char capturado[3], bool *promover)
{
    int linhaPos, colunaPos, linhaCasa, colunaCasa;
    bool move = false;
    bool achou = false;
    bool movimentou = false;
    int i, j;
    int dezena = 10; //Para calculos na vertical
    int unidade = 1; //Para calculos na horizontal
    char pecaCapturada;
    char opcao;
    bool podePromover = false; //Identifica se determinada peça pode ser promovida
    char qualPeca = toupper(peca[0]); //Possibilitar que T, Z, N e I tenham movimento de G sem ter de repetir código
    char qualRei[3];
    //Variáveis que possibilitarão usar a função acharPeca()
    int auxPosicaoMatriz;
    int auxPosicaoInterface;
    //Caso a jogada resulte em check, salvar a peça antes de realizar as trocas
    char salvarPeca[3];

    //Descobrir qual é o rei, oponente ou jogador
    //Jogador
    if((peca[0] > 'A') && (peca[0] <= 'Z'))
    {
        qualRei[0] = 'K';
        qualRei[1] = 'K';
    }
    else //Oponente
    {
        qualRei[0] = 'k';
        qualRei[1] = 'k';
    }

    //Caso a peça seja T, Z, N ou I, utilizar a movimentação de G
    if((qualPeca == 'T') || (qualPeca == 'Z') || (qualPeca == 'N') || (qualPeca == 'I'))
    {
        qualPeca = 'G';
    }
    
    //Pegar a posição atual da peça (linha x coluna)
    colunaPos = *posicaoMatriz % 10;
    linhaPos = *posicaoMatriz  / 10;
    
    //Variáveis que vão indicar a linha e coluna em que a peça deve ser deslocada
    colunaCasa = colunaPos;
    linhaCasa = linhaPos;
    
    //Verifica os locais possíveis para deslocamento e se a jogada equivale a um deles
    switch (qualPeca)
    {
        //Lance   [Jogador != Oponente]
        case 'L':
            //Jogador
            if(peca[0] == 'L')
            {
                i = linhaPos - 1;

                //Cima
                while((i >= 0) && (achou == false)) 
                {
                    if(*casa == (*posicaoInterface - dezena))
                    {
                        move = true;
                        linhaCasa -= (dezena / 10);
                    }
                    //Confere se a peça "passou por cima de outra"
                    else if((tabuleiro[i][colunaPos][0] != ' ') && (move == false))
                    {
                        achou = true;
                    }
                    dezena += 10;
                    i--;
                }
            }
            else //Oponente
            {
                i = linhaPos + 1;

                //Baixo
                while((i < 9) && (achou == false)) 
                {
                    if(*casa == (*posicaoInterface + dezena))
                    {
                        move = true;
                        linhaCasa += (dezena / 10);
                    }
                    //Confere se a peça "passou por cima de outra"
                    else if((tabuleiro[i][colunaPos][0] != ' ') && (move == false))
                    {
                        achou = true;
                    }
                    dezena += 10;
                    i++;
                }
            }
            podePromover = true;
            break;
        
        //Knight && King
        case 'K':
            //King   [Jogador == Oponente]
            if(toupper(peca[1]) == 'K')
            {
                //Cima
                if(*casa == (*posicaoInterface - 10))
                {
                    move = true;
                    linhaCasa -= 1;
                }
                //Cima direita
                else if(*casa == (*posicaoInterface - 10 - 1))
                {
                    move = true;
                    linhaCasa -= 1;
                    colunaCasa += 1;
                }
                //Cima esquerda
                else if(*casa == (*posicaoInterface - 10 + 1))
                {
                    move = true;
                    linhaCasa -= 1;
                    colunaCasa -= 1;
                }
                //Esquerda
                else if(*casa == (*posicaoInterface + 1))
                {
                    move = true;
                    colunaCasa -= 1;
                }
                //Direita
                else if(*casa == (*posicaoInterface - 1))
                {
                    move = true;
                    colunaCasa += 1;
                }
                //Baixo
                else if(*casa == (*posicaoInterface + 10))
                {
                    move = true;
                    linhaCasa += 1;
                }
                //Baixo direita
                else if(*casa == (*posicaoInterface + 10 - 1))
                {
                    move = true;
                    linhaCasa += 1;
                    colunaCasa += 1;
                }
                //Baixo esquerda
                else if(*casa == (*posicaoInterface + 10 + 1))
                {
                    move = true;
                    linhaCasa += 1;
                    colunaCasa -= 1;
                }
            }
            //Knight   [Jogador != Oponente]
            else
            {
                //Jogador
                if(peca[0] == 'K')
                {
                    //Cima esquerda
                    if(*casa == (*posicaoInterface - 20 + 1))
                    {
                        move = true;
                        linhaCasa -= 2;
                        colunaCasa -= 1;
                    }
                    //Cima direita
                    else if(*casa == (*posicaoInterface - 20 - 1))
                    {
                        move = true;
                        linhaCasa -= 2;
                        colunaCasa += 1;
                    }
                }
                else //Oponente
                {
                    //Baixo esquerda
                    if(*casa == (*posicaoInterface + 20 + 1))
                    {
                        move = true;
                        linhaCasa += 2;
                        colunaCasa -= 1;
                    }
                    //Baixo direita
                    else if(*casa == (*posicaoInterface + 20 - 1))
                    {
                        move = true;
                        linhaCasa += 2;
                        colunaCasa += 1;
                    }
                }
                podePromover = true;
            }
            break;

        //Silver   [Jogador != Oponente]
        case 'S':
            //Jogador
            if(peca[0] == 'S')
            {
                //Cima
                if(*casa == (*posicaoInterface - 10))
                {
                    move = true;
                    linhaCasa -= 1;
                }
                //Cima direita
                else if(*casa == (*posicaoInterface - 10 - 1))
                {
                    move = true;
                    linhaCasa -= 1;
                    colunaCasa += 1;
                }
                //Cima esquerda
                else if(*casa == (*posicaoInterface - 10 + 1))
                {
                    move = true;
                    linhaCasa -= 1;
                    colunaCasa -= 1;
                }
                //Baixo direita
                else if(*casa == (*posicaoInterface + 10 - 1))
                {
                    move = true;
                    linhaCasa += 1;
                    colunaCasa += 1;
                }
                //Baixo esquerda
                else if(*casa == (*posicaoInterface + 10 + 1))
                {
                    move = true;
                    linhaCasa += 1;
                    colunaCasa -= 1;
                }
            }
            else //Oponente
            {
                //Baixo
                if(*casa == (*posicaoInterface + 10))
                {
                    move = true;
                    linhaCasa += 1;
                }
                //Baixo direita
                else if(*casa == (*posicaoInterface + 10 - 1))
                {
                    move = true;
                    linhaCasa += 1;
                    colunaCasa += 1;
                }
                //Baixo esquerda
                else if(*casa == (*posicaoInterface + 10 + 1))
                {
                    move = true;
                    linhaCasa += 1;
                    colunaCasa -= 1;
                }
                //Cima direita
                else if(*casa == (*posicaoInterface - 10 - 1))
                {
                    move = true;
                    linhaCasa -= 1;
                    colunaCasa += 1;
                }
                //Cima esquerda
                else if(*casa == (*posicaoInterface - 10 + 1))
                {
                    move = true;
                    linhaCasa -= 1;
                    colunaCasa -= 1;
                }
            }
            podePromover = true;
            break;

        //Gold   [Jogador != Oponente]
        case 'G':
            //Jogador
            if(peca[0] == 'G')
            {
                //Cima
                if(*casa == (*posicaoInterface - 10))
                {
                    move = true;
                    linhaCasa -= 1;
                }
                //Cima direita
                else if(*casa == (*posicaoInterface - 10 - 1))
                {
                    move = true;
                    linhaCasa -= 1;
                    colunaCasa += 1;
                }
                //Cima esquerda
                else if(*casa == (*posicaoInterface - 10 + 1))
                {
                    move = true;
                    linhaCasa -= 1;
                    colunaCasa -= 1;
                }
                //Esquerda
                else if(*casa == (*posicaoInterface + 1))
                {
                    move = true;
                    colunaCasa -= 1;
                }
                //Direita
                else if(*casa == (*posicaoInterface - 1))
                {
                    move = true;
                    colunaCasa += 1;
                }
                //Baixo
                else if(*casa == (*posicaoInterface + 10))
                {
                    move = true;
                    linhaCasa += 1;
                }
            }
            else //Oponente
            {
                //Cima
                if(*casa == (*posicaoInterface - 10))
                {
                    move = true;
                    linhaCasa -= 1;
                }
                //Esquerda
                else if(*casa == (*posicaoInterface + 1))
                {
                    move = true;
                    colunaCasa -= 1;
                }
                //Direita
                else if(*casa == (*posicaoInterface - 1))
                {
                    move = true;
                    colunaCasa += 1;
                }
                //Baixo
                else if(*casa == (*posicaoInterface + 10))
                {
                    move = true;
                    linhaCasa += 1;
                }
                //Baixo direita
                else if(*casa == (*posicaoInterface + 10 - 1))
                {
                    move = true;
                    linhaCasa += 1;
                    colunaCasa += 1;
                }
                //Baixo esquerda
                else if(*casa == (*posicaoInterface + 10 + 1))
                {
                    move = true;
                    linhaCasa += 1;
                    colunaCasa -= 1;
                }
            }
            break;

        //Rook   [Jogador == Oponente]
        case 'R':
            //Cima
            if((*casa / 10) < (*posicaoMatriz / 10) + 1)
            {
                i = linhaPos - 1;

                while((i >= 0) && (achou == false) && (move == false))
                {
                    if(*casa == (*posicaoInterface - dezena))
                    {
                        move = true;
                        linhaCasa -= (dezena / 10);
                    }
                    //Confere se a peça "passou por cima de outra"
                    else if(tabuleiro[i][colunaPos][0] != ' ')
                    {
                        achou = true;
                    }
                    dezena += 10;
                    i--;
                }
            }
            //Baixo
            else if((*casa / 10) > (*posicaoMatriz / 10) + 1)
            {
                i = linhaPos + 1;

                while((i < 9) && (achou == false) && (move == false))
                {
                    if(*casa == (*posicaoInterface + dezena))
                    {
                        move = true;
                        linhaCasa += (dezena / 10);
                    }
                    //Confere se a peça "passou por cima de outra"
                    else if(tabuleiro[i][colunaPos][0] != ' ')
                    {
                        achou = true;
                    }
                    dezena += 10;
                    i++;
                }
            }
            //Esquerda
            else if((9 - *casa % 10) < (*posicaoMatriz % 10))
            {
                j = colunaPos - 1;

                while((j >= 0) && (achou == false) && (move == false))
                {
                    if(*casa == (*posicaoInterface + unidade))
                    {
                        move = true;
                        colunaCasa -= unidade;
                    }
                    //Confere se a peça "passou por cima de outra"
                    else if(tabuleiro[linhaPos][j][0] != ' ')
                    {
                        achou = true;
                    }
                    unidade++;
                    j--;
                }
            }
            //Direita
            else
            {
                j = colunaPos + 1;

                while((j < 9) && (achou == false) && (move == false))
                {
                    if(*casa == (*posicaoInterface - unidade))
                    {
                        move = true;
                        colunaCasa += unidade;
                    }
                    //Confere se a peça "passou por cima de outra"
                    else if(tabuleiro[linhaPos][j][0] != ' ')
                    {
                        achou = true;
                    }
                    unidade++;
                    j++;
                }
            }
            podePromover = true;
            break;

        //Bishop   [Jogador == Oponente]
        case 'B':
            //Cima
            if((*casa / 10) < (*posicaoMatriz / 10) + 1)
            {
                //Esquerda
                if((9 - *casa % 10) < (*posicaoMatriz % 10))
                {
                    j = colunaPos - 1;
                    i = linhaPos - 1;

                    while((j >= 0) && (achou == false) && (move == false))
                    {
                        if(*casa == (*posicaoInterface - dezena + unidade))
                        {
                            move = true;
                            linhaCasa -= (dezena / 10);
                            colunaCasa -= unidade;
                        }
                        //Confere se a peça "passou por cima de outra"
                        else if(tabuleiro[i][j][0] != ' ')
                        {
                            achou = true;
                        }
                        dezena += 10;
                        unidade++;

                        j--;
                        i--;
                    }
                }
                //Direita
                else
                {
                    j = colunaPos + 1;
                    i = linhaPos - 1;

                    while((j < 9) && (achou == false) && (move == false))
                    {
                        if(*casa == (*posicaoInterface - dezena - unidade))
                        {
                            move = true;
                            linhaCasa -= (dezena / 10);
                            colunaCasa += unidade;
                        }
                        //Confere se a peça "passou por cima de outra"
                        else if(tabuleiro[i][j][0] != ' ')
                        {
                            achou = true;
                        }
                        dezena += 10;
                        unidade++;

                        j++;
                        i--;
                    }
                }
            }
            //Baixo
            else
            {
                //Esquerda
                if((9 - *casa % 10) < (*posicaoMatriz % 10))
                {
                    j = colunaPos - 1;
                    i = linhaPos + 1;

                    while((j >= 0) && (achou == false) && (move == false))
                    {
                        if(*casa == (*posicaoInterface + dezena + unidade))
                        {
                            move = true;
                            linhaCasa += (dezena / 10);
                            colunaCasa -= unidade;
                        }
                        //Confere se a peça "passou por cima de outra"
                        else if(tabuleiro[i][j][0] != ' ')
                        {
                            achou = true;
                        }
                        dezena += 10;
                        unidade++;

                        j--;
                        i++;
                    }
                }
                //Direita
                else
                {
                    j = colunaPos + 1;
                    i = linhaPos + 1;

                    while((j < 9) && (achou == false) && (move == false))
                    {
                        if(*casa == (*posicaoInterface + dezena - unidade))
                        {
                            move = true;
                            linhaCasa += (dezena / 10);
                            colunaCasa += unidade;
                        }
                        //Confere se a peça "passou por cima de outra"
                        else if(tabuleiro[i][j][0] != ' ')
                        {
                            achou = true;
                        }
                        dezena += 10;
                        unidade++;

                        j++;
                        i++;
                    }
                }
            }
            podePromover = true;
            break;

        //Pawn   [Jogador != Oponente]
        case 'P':
            //Jogador
            if(peca[0] == 'P')
            {
                //Cima
                if(*casa == (*posicaoInterface - 10))
                {
                    move = true;
                    linhaCasa -= 1;
                }
            }
            else //Oponente
            {
                //Baixo
                if(*casa == (*posicaoInterface + 10))
                {
                    move = true;
                    linhaCasa += 1;
                }
            }
            podePromover = true;
            break;

        //Dragon   [Jogador == Oponente]
        case 'D':
            //Cima direita
            if(*casa == (*posicaoInterface - 10 - 1))
            {
                move = true;
                linhaCasa -= 1;
                colunaCasa += 1;
            }
            //Cima esquerda
            else if(*casa == (*posicaoInterface - 10 + 1))
            {
                move = true;
                linhaCasa -= 1;
                colunaCasa -= 1;
            }
            //Baixo direita
            else if(*casa == (*posicaoInterface + 10 - 1))
            {
                move = true;
                linhaCasa += 1;
                colunaCasa += 1;
            }
            //Baixo esquerda
            else if(*casa == (*posicaoInterface + 10 + 1))
            {
                move = true;
                linhaCasa += 1;
                colunaCasa -= 1;
            }
            //Cima
            if((*casa / 10) < (*posicaoMatriz / 10) + 1)
            {
                i = linhaPos - 1;

                while((i >= 0) && (achou == false) && (move == false))
                {
                    if(*casa == (*posicaoInterface - dezena))
                    {
                        move = true;
                        linhaCasa -= (dezena / 10);
                    }
                    //Confere se a peça "passou por cima de outra"
                    else if(tabuleiro[i][colunaPos][0] != ' ')
                    {
                        achou = true;
                    }
                    dezena += 10;
                    i--;
                }
            }
            //Baixo
            else if((*casa / 10) > (*posicaoMatriz / 10) + 1)
            {
                i = linhaPos + 1;

                while((i < 9) && (achou == false) && (move == false))
                {
                    if(*casa == (*posicaoInterface + dezena))
                    {
                        move = true;
                        linhaCasa += (dezena / 10);
                    }
                    //Confere se a peça "passou por cima de outra"
                    else if(tabuleiro[i][colunaPos][0] != ' ')
                    {
                        achou = true;
                    }
                    dezena += 10;
                    i++;
                }
            }
            //Esquerda
            else if((9 - *casa % 10) < (*posicaoMatriz % 10))
            {
                j = colunaPos - 1;

                while((j >= 0) && (achou == false) && (move == false))
                {
                    if(*casa == (*posicaoInterface + unidade))
                    {
                        move = true;
                        colunaCasa -= unidade;
                    }
                    //Confere se a peça "passou por cima de outra"
                    else if(tabuleiro[linhaPos][j][0] != ' ')
                    {
                        achou = true;
                    }
                    unidade++;
                    j--;
                }
            }
            //Direita
            else
            {
                j = colunaPos + 1;

                while((j < 9) && (achou == false) && (move == false))
                {
                    if(*casa == (*posicaoInterface - unidade))
                    {
                        move = true;
                        colunaCasa += unidade;
                    }
                    //Confere se a peça "passou por cima de outra"
                    else if(tabuleiro[linhaPos][j][0] != ' ')
                    {
                        achou = true;
                    }
                    unidade++;
                    j++;
                }
            }
            break;

        //Horse   [Jogador == Oponente]
        case 'H':
            //Cima
            if(*casa == (*posicaoInterface - 10))
            {
                move = true;
                linhaCasa -= 1;
            }
            //Esquerda
            else if(*casa == (*posicaoInterface + 1))
            {
                move = true;
                colunaCasa -= 1;
            }
            //Direita
            else if(*casa == (*posicaoInterface - 1))
            {
                move = true;
                colunaCasa += 1;
            }
            //Baixo
            else if(*casa == (*posicaoInterface + 10))
            {
                move = true;
                linhaCasa += 1;
            }
            //Cima (DIAGONAIS)
            if((*casa / 10) < (*posicaoMatriz / 10) + 1)
            {
                //Esquerda
                if((9 - *casa % 10) < (*posicaoMatriz % 10))
                {
                    j = colunaPos - 1;
                    i = linhaPos - 1;

                    while((j >= 0) && (achou == false) && (move == false))
                    {
                        if(*casa == (*posicaoInterface - dezena + unidade))
                        {
                            move = true;
                            linhaCasa -= (dezena / 10);
                            colunaCasa -= unidade;
                        }
                        //Confere se a peça "passou por cima de outra"
                        else if(tabuleiro[i][j][0] != ' ')
                        {
                            achou = true;
                        }
                        dezena += 10;
                        unidade++;

                        j--;
                        i--;
                    }
                }
                //Direita
                else
                {
                    j = colunaPos + 1;
                    i = linhaPos - 1;

                    while((j < 9) && (achou == false) && (move == false))
                    {
                        if(*casa == (*posicaoInterface - dezena - unidade))
                        {
                            move = true;
                            linhaCasa -= (dezena / 10);
                            colunaCasa += unidade;
                        }
                        //Confere se a peça "passou por cima de outra"
                        else if(tabuleiro[i][j][0] != ' ')
                        {
                            achou = true;
                        }
                        dezena += 10;
                        unidade++;

                        j++;
                        i--;
                    }
                }
            }
            //Baixo (DIAGONAIS)
            else
            {
                //Esquerda
                if((9 - *casa % 10) < (*posicaoMatriz % 10))
                {
                    j = colunaPos - 1;
                    i = linhaPos + 1;

                    while((j >= 0) && (achou == false) && (move == false))
                    {
                        if(*casa == (*posicaoInterface + dezena + unidade))
                        {
                            move = true;
                            linhaCasa += (dezena / 10);
                            colunaCasa -= unidade;
                        }
                        //Confere se a peça "passou por cima de outra"
                        else if(tabuleiro[i][j][0] != ' ')
                        {
                            achou = true;
                        }
                        dezena += 10;
                        unidade++;

                        j--;
                        i++;
                    }
                }
                //Direita
                else
                {
                    j = colunaPos + 1;
                    i = linhaPos + 1;

                    while((j < 9) && (achou == false) && (move == false))
                    {
                        if(*casa == (*posicaoInterface + dezena - unidade))
                        {
                            move = true;
                            linhaCasa += (dezena / 10);
                            colunaCasa += unidade;
                        }
                        //Confere se a peça "passou por cima de outra"
                        else if(tabuleiro[i][j][0] != ' ')
                        {
                            achou = true;
                        }
                        dezena += 10;
                        unidade++;

                        j++;
                        i++;
                    }
                }
            }
            break;

        //Erro!
        default:
            printf("\nerro!\n");
            break;
    }
    
    //T, Z, N e I, após utilizar o movimento de G, devem informar que não podem ser promovidos
    if((qualPeca == 'T') || (qualPeca == 'Z') || (qualPeca == 'N') || (qualPeca == 'I'))
    {
        podePromover = false;
    }

    pecaCapturada = tabuleiro[linhaCasa][colunaCasa][0];

    //Confere se a peça está tentando capturar uma peça aliada
    //Jogador
    if((peca[0] > 'A') && (peca[0] <= 'Z'))
    {
        if((pecaCapturada > 'A') && (pecaCapturada <= 'Z'))
        {
            move = false;
        }
        else if(pecaCapturada != ' ')
        {
            //"capturado" recebe a peça capturada
            capturado[0] = tabuleiro[linhaCasa][colunaCasa][0];
            capturado[1] = tabuleiro[linhaCasa][colunaCasa][1];
        }
    }
    else //Oponente
    {
        if((pecaCapturada > 'a') && (pecaCapturada <= 'z'))
        {
            move = false;
        }
        else if(pecaCapturada != ' ')
        {
            //"capturado" recebe a peça capturada
            capturado[0] = tabuleiro[linhaCasa][colunaCasa][0];
            capturado[1] = tabuleiro[linhaCasa][colunaCasa][1];
        }
    }

    //Se o movimento for válido, mexe a peça 
    if(move == true)
    {   
        //Conferir se a jogada não resulta em um check
        //Antes, salvar a peça
        salvarPeca[0] = tabuleiro[linhaCasa][colunaCasa][0];
        salvarPeca[1] = tabuleiro[linhaCasa][colunaCasa][1];

        //Move a peça para o local escolhido
        tabuleiro[linhaCasa][colunaCasa][0] = peca[0];
        tabuleiro[linhaCasa][colunaCasa][1] = peca[1];

        //Deixa vazio o local em que a peça estava
        tabuleiro[linhaPos][colunaPos][0] = ' ';
        tabuleiro[linhaPos][colunaPos][1] = ' ';

        //Procurar a posição do Rei
        acharPeca(qualRei, tabuleiro, &auxPosicaoMatriz, &auxPosicaoInterface);

        //Se a jogada resultar em check, desfazer o movimento e avisar o usuário
        if(check(qualRei[0], &auxPosicaoMatriz, tabuleiro) == true)
        {
            printf("\nA jogada resulta em check!!\n");

            //Retorna a peça capturada ou vazio
            tabuleiro[linhaCasa][colunaCasa][0] = salvarPeca[0];
            tabuleiro[linhaCasa][colunaCasa][1] = salvarPeca[1];

            //Retorna a peça ao local original
            tabuleiro[linhaPos][colunaPos][0] = peca[0];
            tabuleiro[linhaPos][colunaPos][1] = peca[1];

            //Se houve captura, desfazer.
            capturado[0] = ' ';
            capturado[1] = ' ';
        }
        else //Mantém a jogada
        {
            movimentou = true;

            //Se for possível, pergunta se o usuário quer promover a peça
            //Jogador
            if((peca[0] > 'A') && (peca[0] < 'Z'))
            {
                //Promover entrando nas linhas de promoção OU saindo delas
                if((podePromover == true) && ((linhaCasa < 3) || (linhaPos < 3)))
                {
                    //Pergunta se o jogador quer promover a peça SE não deve promover automático
                    if(((peca[0] == 'P') || (peca[0] == 'L')) && (linhaCasa == 0))
                    {
                        //Do nothing
                    }
                    else if(((peca[0] == 'K') && (peca[1] != 'K')) && (linhaCasa < 2))
                    {
                        //Do nothing
                    }
                    else
                    {
                        printf("\nDeseja promover a peca ? [S/N]\n");
                        scanf("%s", &opcao);

                        if(toupper(opcao) == 'S')
                        {
                            *promover = true;
                        }
                    }
                }
            }
            else //Oponente
            {
                //Promover entrando nas linhas de promoção OU saindo delas
                if((podePromover == true) && ((linhaCasa > 5) || (linhaPos > 5)))
                {
                    //Pergunta se o jogador quer promover a peça SE não deve promover automático
                    if(((peca[0] == 'p') || (peca[0] == 'l')) && (linhaCasa == 8))
                    {
                        //Do nothing
                    }
                    else if(((peca[0] == 'k') && (peca[1] != 'k')) && (linhaCasa > 6))
                    {
                        //Do nothing
                    }
                    else
                    {
                        printf("\nDeseja promover a peca ? [S/N]\n");
                        scanf("%s", &opcao);

                        if(toupper(opcao) == 'S')
                        {
                            *promover = true;
                        }
                    }
                }
            }
        }
    }
    else
    {
        printf("\nMovimento invalido!\n");
    }

    return movimentou;
}

/*
    Verifica quais são os movimentos possíveis do rei e armazena no vetor "casasPossiveis[7]".
*/
bool movimentosPossiveisRei(char rei, int *posicaoRei, pecasTabuleiro tabuleiro[9][9], int casasPossiveis[7])
{
    //Verificar as posições:
    /*
        * * * 
        * K * 
        * * * 
    */
    int i;
    bool podeMover = false;
    int linhaPos, colunaPos;
    char peca;
    int checarPosicao;
    bool aux;

    //Pegar a posição atual da peça (linha x coluna)
    colunaPos = *posicaoRei % 10;
    linhaPos = *posicaoRei  / 10;

    //Cima
    //Garantir que não tenha números fora do índice da matriz
	if((linhaPos - 1) >= 0)
	{
        //Conferir se a posição está vazia ou pode capturar
        peca = tabuleiro[linhaPos - 1][colunaPos][0];

        //Jogador
        if(rei == 'K')
        {
            if((peca == ' ') || ((peca > 'a') && (peca <= 'z')))
            {
                checarPosicao = *posicaoRei - 10;
                if(check(rei, &checarPosicao, tabuleiro) == false)
                {
                    podeMover = true;
                    casasPossiveis[0] = (*posicaoRei - 10);
                }
            }
        }
        else //Oponente
        {
            if((peca == ' ') || ((peca > 'A') && (peca <= 'Z')))
            {
                checarPosicao = *posicaoRei - 10;
                if(check(rei, &checarPosicao, tabuleiro) == false)
                {
                    podeMover = true;
                    casasPossiveis[0] = (*posicaoRei - 10);
                }
            }
        }
    }

    //Cima esquerda
    //Garantir que não tenha números fora do índice da matriz
	if(((linhaPos - 1) >= 0) && ((colunaPos - 1) >= 0))
	{
        //Conferir se a posição está vazia ou pode capturar
        peca = tabuleiro[linhaPos - 1][colunaPos - 1][0];

        //Jogador
        if(rei == 'K')
        {
            if((peca == ' ') || ((peca > 'a') && (peca <= 'z')))
            {
                checarPosicao = *posicaoRei - 11;
                if(check(rei, &checarPosicao, tabuleiro) == false)
                {
                    podeMover = true;
                    casasPossiveis[1] = (*posicaoRei - 11);
                }
            }
        }
        else //Oponente
        {
            if((peca == ' ') || ((peca > 'A') && (peca <= 'Z')))
            {
                checarPosicao = *posicaoRei - 11;
                if(check(rei, &checarPosicao, tabuleiro) == false)
                {
                    podeMover = true;
                    casasPossiveis[1] = (*posicaoRei - 11);
                }
            }
        }
    }

    //Cima direita
    //Garantir que não tenha números fora do índice da matriz
	if(((linhaPos - 1) >= 0) && ((colunaPos + 1) < 9))
	{
        //Conferir se a posição está vazia ou pode capturar
        peca = tabuleiro[linhaPos - 1][colunaPos + 1][0];

        //Jogador
        if(rei == 'K')
        {
            if((peca == ' ') || ((peca > 'a') && (peca <= 'z')))
            {
                checarPosicao = *posicaoRei - 9;
                if(check(rei, &checarPosicao, tabuleiro) == false)
                {
                    podeMover = true;
                    casasPossiveis[2] = (*posicaoRei - 9);
                }
            }
        }
        else //Oponente
        {
            if((peca == ' ') || ((peca > 'A') && (peca <= 'Z')))
            {
                checarPosicao = *posicaoRei - 9;
                if(check(rei, &checarPosicao, tabuleiro) == false)
                {
                    podeMover = true;
                    casasPossiveis[2] = (*posicaoRei - 9);
                }
            }
        }
    }

    //Esquerda
    //Garantir que não tenha números fora do índice da matriz
	if((colunaPos - 1) >= 0)
	{
        //Conferir se a posição está vazia ou pode capturar
        peca = tabuleiro[linhaPos][colunaPos - 1][0];

        //Jogador
        if(rei == 'K')
        {
            if((peca == ' ') || ((peca > 'a') && (peca <= 'z')))
            {
                checarPosicao = *posicaoRei - 1;
                if(check(rei, &checarPosicao, tabuleiro) == false)
                {
                    podeMover = true;
                    casasPossiveis[3] = (*posicaoRei - 1);
                }
            }
        }
        else //Oponente
        {
            if((peca == ' ') || ((peca > 'A') && (peca <= 'Z')))
            {
                checarPosicao = *posicaoRei - 1;
                if(check(rei, &checarPosicao, tabuleiro) == false)
                {
                    podeMover = true;
                    casasPossiveis[3] = (*posicaoRei - 1);
                }
            }
        }
    }

    //Direita
    //Garantir que não tenha números fora do índice da matriz
	if((colunaPos + 1) < 9)
	{
        //Conferir se a posição está vazia ou pode capturar
        peca = tabuleiro[linhaPos][colunaPos + 1][0];

        //Jogador
        if(rei == 'K')
        {
            if((peca == ' ') || ((peca > 'a') && (peca <= 'z')))
            {
                checarPosicao = *posicaoRei + 1;
                if(check(rei, &checarPosicao, tabuleiro) == false)
                {
                    podeMover = true;
                    casasPossiveis[4] = (*posicaoRei + 1);
                }
            }
        }
        else //Oponente
        {
            if((peca == ' ') || ((peca > 'A') && (peca <= 'Z')))
            {
                checarPosicao = *posicaoRei + 1;
                if(check(rei, &checarPosicao, tabuleiro) == false)
                {
                    podeMover = true;
                    casasPossiveis[4] = (*posicaoRei + 1);
                }
            }
        }
    }

    //Baixo
    //Garantir que não tenha números fora do índice da matriz
	if((linhaPos + 1) < 9)
	{
        //Conferir se a posição está vazia ou pode capturar
        peca = tabuleiro[linhaPos + 1][colunaPos][0];

        //Jogador
        if(rei == 'K')
        {
            if((peca == ' ') || ((peca > 'a') && (peca <= 'z')))
            {
                checarPosicao = *posicaoRei + 10;
                if(check(rei, &checarPosicao, tabuleiro) == false)
                {
                    podeMover = true;
                    casasPossiveis[5] = (*posicaoRei + 10);
                }
            }
        }
        else //Oponente
        {
            if((peca == ' ') || ((peca > 'A') && (peca <= 'Z')))
            {
                checarPosicao = *posicaoRei + 10;
                if(check(rei, &checarPosicao, tabuleiro) == false)
                {
                    podeMover = true;
                    casasPossiveis[5] = (*posicaoRei + 10);
                }
            }
        }
    }

    //Baixo esquerda
    //Garantir que não tenha números fora do índice da matriz
	if(((linhaPos + 1) < 9) && ((colunaPos - 1) >= 0))
    {
        //Conferir se a posição está vazia ou pode capturar
        peca = tabuleiro[linhaPos + 1][colunaPos - 1][0];

        //Jogador
        if(rei == 'K')
        {
            if((peca == ' ') || ((peca > 'a') && (peca <= 'z')))
            {
                checarPosicao = *posicaoRei + 9;
                if(check(rei, &checarPosicao, tabuleiro) == false)
                {
                    podeMover = true;
                    casasPossiveis[6] = (*posicaoRei + 9);
                }
            }
        }
        else //Oponente
        {
            if((peca == ' ') || ((peca > 'A') && (peca <= 'Z')))
            {
                checarPosicao = *posicaoRei + 9;
                if(check(rei, &checarPosicao, tabuleiro) == false)
                {
                    podeMover = true;
                    casasPossiveis[6] = (*posicaoRei + 9);
                }
            }
        }
    }

    //Baixo direita
    //Garantir que não tenha números fora do índice da matriz
	if(((linhaPos + 1) < 9) && ((colunaPos + 1) < 9))
	{
        //Conferir se a posição está vazia ou pode capturar
        peca = tabuleiro[linhaPos + 1][colunaPos + 1][0];

        //Jogador
        if(rei == 'K')
        {
            if((peca == ' ') || ((peca > 'a') && (peca <= 'z')))
            {
                checarPosicao = *posicaoRei + 11;
                if(check(rei, &checarPosicao, tabuleiro) == false)
                {
                    podeMover = true;
                    casasPossiveis[7] = (*posicaoRei + 11);
                }
            }
        }
        else //Oponente
        {
            if((peca == ' ') || ((peca > 'A') && (peca <= 'Z')))
            {
                checarPosicao = *posicaoRei + 11;
                if(check(rei, &checarPosicao, tabuleiro) == false)
                {
                    podeMover = true;
                    casasPossiveis[7] = (*posicaoRei + 11);
                }
            }
        }
    }
	
    return podeMover;
}

/*
    Função usada na função podeProtegerAtacando. Essa função confere se determinada peça está pinada.
    Uma peça está pinada quando, entre ela, existe um rei e um R/D/B/H.
*/
bool pinado(char peca, int *posicaoPeca, pecasTabuleiro tabuleiro[9][9])
{
    bool estaPinado = false;
    bool achou = false;
    bool temPeca = false; //Variável que verificará se existe alguma peça no caminho
    int linhaPos, colunaPos, i, j;
    char rei;
    char pecaPin; //Variável que verificará se existe R/D/B/H
    char auxRei, auxPecaPin;

    //Pegar a posição atual da peça (linha x coluna)
    colunaPos = *posicaoPeca % 10;
    linhaPos = *posicaoPeca / 10;

    //Jogador
    if((peca > 'A') && (peca <= 'Z'))
    {
        //Vertical superior
        i = 1;
            
        //Tenta localizar rook, dragon ou rei
        while(((linhaPos - i) >= 0) && (temPeca == false) && (achou == false))
        {
            auxPecaPin = tabuleiro[linhaPos - i][colunaPos][0];
            auxRei = tabuleiro[linhaPos - i][colunaPos][1];

            //Verificar se tem rook, dragon
            if((auxPecaPin == 'r') || (auxPecaPin == 'd'))
            {
                achou = true;
                pecaPin = auxPecaPin;
            }
            //Verificar se tem rei
            else if(auxRei == 'K')
            {
                achou = true;
                rei = auxRei;
            }
            //Verificar se tem uma peça no caminho
            else if(auxPecaPin != ' ')
            {
                temPeca = true;
            }
            i++;
        }
        
        //Vertical inferior
        i = 1;
        achou = false;

        //Tenta localizar rook, dragon ou rei
        while(((linhaPos + i) < 9) && (temPeca == false) && (achou == false))
        {
            auxPecaPin = tabuleiro[linhaPos + i][colunaPos][0];
            auxRei = tabuleiro[linhaPos + i][colunaPos][1];

            //Verificar se tem rook, dragon
            if((auxPecaPin == 'r') || (auxPecaPin == 'd'))
            {
                achou = true;
                pecaPin = auxPecaPin;
            }
            //Verificar se tem rei
            else if(auxRei == 'K')
            {
                achou = true;
                rei = auxRei;
            }
            //Verificar se tem uma peça no caminho
            else if(auxPecaPin != ' ')
            {
                temPeca = true;
            }
            i++;
        }

        //Conferir se a peça está pinada
        if((rei == 'K') && ((pecaPin == 'r') || (pecaPin == 'd')))
        {
            estaPinado = true;
        }
        else //Continua procurando
        {
            //Zerar "rei" e "pecaPin" por garantia
            rei = '0';
            pecaPin = '0';

            //Horizontal esquerda
            j = 1;
            achou = false;
            temPeca = false;

            //Tenta localizar rook, dragon ou rei
            while(((colunaPos - j) >= 0) && (temPeca == false) && (achou == false))
            {
                auxPecaPin = tabuleiro[linhaPos][colunaPos - j][0];
                auxRei = tabuleiro[linhaPos][colunaPos - j][1];

                //Verificar se tem rook, dragon
                if((auxPecaPin == 'r') || (auxPecaPin == 'd'))
                {
                    achou = true;
                    pecaPin = auxPecaPin;
                }
                //Verificar se tem rei
                else if(auxRei == 'K')
                {
                    achou = true;
                    rei = auxRei;
                }
                //Verificar se tem uma peça no caminho
                else if(auxPecaPin != ' ')
                {
                    temPeca = true;
                }
                j++;
            }
            
            //Horizontal direita
            j = 1;
            achou = false;

            //Tenta localizar rook, dragon ou rei
            while(((colunaPos + j) < 9) && (temPeca == false) && (achou == false))
            {
                auxPecaPin = tabuleiro[linhaPos][colunaPos + j][0];
                auxRei = tabuleiro[linhaPos][colunaPos + j][1];

                //Verificar se tem rook, dragon
                if((auxPecaPin == 'r') || (auxPecaPin == 'd'))
                {
                    achou = true;
                    pecaPin = auxPecaPin;
                }
                //Verificar se tem rei
                else if(auxRei == 'K')
                {
                    achou = true;
                    rei = auxRei;
                }
                //Verificar se tem uma peça no caminho
                else if(auxPecaPin != ' ')
                {
                    temPeca = true;
                }
                j++;
            }

            //Conferir se a peça está pinada
            if((rei == 'K') && ((pecaPin == 'r') || (pecaPin == 'd')))
            {
                estaPinado = true;
            }
            else //Continua procurando
            {
                //Zerar "rei" e "pecaPin" por garantia
                rei = '0';
                pecaPin = '0';

                //Diagonal esquerda superior
                i = 1;
                j = 1;

                achou = false;
                temPeca = false;

                //Tenta localizar bishop, horse ou rei
                while(((linhaPos - i) >= 0) && (temPeca == false) && (achou == false))
                {
                    auxPecaPin = tabuleiro[linhaPos - i][colunaPos - j][0];
                    auxRei = tabuleiro[linhaPos - i][colunaPos - j][1];

                    //Verificar se tem bishop ou horse
                    if((auxPecaPin == 'b') || (auxPecaPin == 'h'))
                    {
                        achou = true;
                        pecaPin = auxPecaPin;
                    }
                    //Verificar se tem rei
                    else if(auxRei == 'K')
                    {
                        achou = true;
                        rei = auxRei;
                    }
                    //Verificar se tem uma peça no caminho
                    else if(auxPecaPin != ' ')
                    {
                        temPeca = true;
                    }
                    i++;
                    j++;
                }
                
                //Diagonal direita inferior
                i = 1;
                j = 1;
                achou = false;

                //Tenta localizar bishop, horse ou rei
                while(((linhaPos + i) < 9) && (temPeca == false) && (achou == false))
                {
                    auxPecaPin = tabuleiro[linhaPos + i][colunaPos + j][0];
                    auxRei = tabuleiro[linhaPos + i][colunaPos + j][1];

                    //Verificar se tem bishop ou horse
                    if((auxPecaPin == 'b') || (auxPecaPin == 'h'))
                    {
                        achou = true;
                        pecaPin = auxPecaPin;
                    }
                    //Verificar se tem rei
                    else if(auxRei == 'K')
                    {
                        achou = true;
                        rei = auxRei;
                    }
                    //Verificar se tem uma peça no caminho
                    else if(auxPecaPin != ' ')
                    {
                        temPeca = true;
                    }
                    i++;
                    j++;
                }

                //Conferir se a peça está pinada
                if((rei == 'K') && ((pecaPin == 'b') || (pecaPin == 'h')))
                {
                    estaPinado = true;
                }
                else //Continua procurando
                {
                    //Zerar "rei" e "pecaPin" por garantia
                    rei = '0';
                    pecaPin = '0';
                    
                    //Diagonal direita superior
                    i = 1;
                    j = 1;

                    achou = false;
                    temPeca = false;

                    //Tenta localizar bishop, horse ou rei
                    while(((linhaPos - i) >= 0) && (temPeca == false) && (achou == false))
                    {
                        auxPecaPin = tabuleiro[linhaPos - i][colunaPos + j][0];
                        auxRei = tabuleiro[linhaPos - i][colunaPos + j][1];

                        //Verificar se tem bishop ou horse
                        if((auxPecaPin == 'b') || (auxPecaPin == 'h'))
                        {
                            achou = true;
                            pecaPin = auxPecaPin;
                        }
                        //Verificar se tem rei
                        else if(auxRei == 'K')
                        {
                            achou = true;
                            rei = auxRei;
                        }
                        //Verificar se tem uma peça no caminho
                        else if(auxPecaPin != ' ')
                        {
                            temPeca = true;
                        }
                        i++;
                        j++;
                    }
                    
                    //Diagonal esquerda inferior
                    i = 1;
                    j = 1;
                    achou = false;

                    //Tenta localizar bishop, horse ou rei
                    while(((linhaPos + i) < 9) && (temPeca == false) && (achou == false))
                    {
                        auxPecaPin = tabuleiro[linhaPos + i][colunaPos - j][0];
                        auxRei = tabuleiro[linhaPos + i][colunaPos - j][1];

                        //Verificar se tem bishop ou horse
                        if((auxPecaPin == 'b') || (auxPecaPin == 'h'))
                        {
                            achou = true;
                            pecaPin = auxPecaPin;
                        }
                        //Verificar se tem rei
                        else if(auxRei == 'K')
                        {
                            achou = true;
                            rei = auxRei;
                        }
                        //Verificar se tem uma peça no caminho
                        else if(auxPecaPin != ' ')
                        {
                            temPeca = true;
                        }
                        i++;
                        j++;
                    }

                    //Conferir se a peça está pinada
                    if((rei == 'K') && ((pecaPin == 'b') || (pecaPin == 'h')))
                    {
                        estaPinado = true;
                    }
                }
            }
        }
    }
    else //Oponente
    {
        //Vertical superior
        i = 1;
            
        //Tenta localizar rook, dragon ou rei
        while(((linhaPos - i) >= 0) && (temPeca == false) && (achou == false))
        {
            auxPecaPin = tabuleiro[linhaPos - i][colunaPos][0];
            auxRei = tabuleiro[linhaPos - i][colunaPos][1];

            //Verificar se tem rook, dragon
            if((auxPecaPin == 'R') || (auxPecaPin == 'D'))
            {
                achou = true;
                pecaPin = auxPecaPin;
            }
            //Verificar se tem rei
            else if(auxRei == 'k')
            {
                achou = true;
                rei = auxRei;
            }
            //Verificar se tem uma peça no caminho
            else if(auxPecaPin != ' ')
            {
                temPeca = true;
            }
            i++;
        }
        
        //Vertical inferior
        i = 1;
        achou = false;

        //Tenta localizar rook, dragon ou rei
        while(((linhaPos + i) < 9) && (temPeca == false) && (achou == false))
        {
            auxPecaPin = tabuleiro[linhaPos + i][colunaPos][0];
            auxRei = tabuleiro[linhaPos + i][colunaPos][1];

            //Verificar se tem rook, dragon
            if((auxPecaPin == 'R') || (auxPecaPin == 'D'))
            {
                achou = true;
                pecaPin = auxPecaPin;
            }
            //Verificar se tem rei
            else if(auxRei == 'k')
            {
                achou = true;
                rei = auxRei;
            }
            //Verificar se tem uma peça no caminho
            else if(auxPecaPin != ' ')
            {
                temPeca = true;
            }
            i++;
        }

        //Conferir se a peça está pinada
        if((rei == 'k') && ((pecaPin == 'R') || (pecaPin == 'D')))
        {
            estaPinado = true;
        }
        else //Continua procurando
        {
            //Zerar "rei" e "pecaPin" por garantia
            rei = '0';
            pecaPin = '0';

            //Horizontal esquerda
            j = 1;
            achou = false;
            temPeca = false;

            //Tenta localizar rook, dragon ou rei
            while(((colunaPos - j) >= 0) && (temPeca == false) && (achou == false))
            {
                auxPecaPin = tabuleiro[linhaPos][colunaPos - j][0];
                auxRei = tabuleiro[linhaPos][colunaPos - j][1];

                //Verificar se tem rook, dragon
                if((auxPecaPin == 'R') || (auxPecaPin == 'D'))
                {
                    achou = true;
                    pecaPin = auxPecaPin;
                }
                //Verificar se tem rei
                else if(auxRei == 'k')
                {
                    achou = true;
                    rei = auxRei;
                }
                //Verificar se tem uma peça no caminho
                else if(auxPecaPin != ' ')
                {
                    temPeca = true;
                }
                j++;
            }
            
            //Horizontal direita
            j = 1;
            achou = false;

            //Tenta localizar rook, dragon ou rei
            while(((colunaPos + j) < 9) && (temPeca == false) && (achou == false))
            {
                auxPecaPin = tabuleiro[linhaPos][colunaPos + j][0];
                auxRei = tabuleiro[linhaPos][colunaPos + j][1];

                //Verificar se tem rook, dragon
                if((auxPecaPin == 'R') || (auxPecaPin == 'D'))
                {
                    achou = true;
                    pecaPin = auxPecaPin;
                }
                //Verificar se tem rei
                else if(auxRei == 'k')
                {
                    achou = true;
                    rei = auxRei;
                }
                //Verificar se tem uma peça no caminho
                else if(auxPecaPin != ' ')
                {
                    temPeca = true;
                }
                j++;
            }

            //Conferir se a peça está pinada
            if((rei == 'k') && ((pecaPin == 'R') || (pecaPin == 'D')))
            {
                estaPinado = true;
            }
            else //Continua procurando
            {
                //Zerar "rei" e "pecaPin" por garantia
                rei = '0';
                pecaPin = '0';

                //Diagonal esquerda superior
                i = 1;
                j = 1;

                achou = false;
                temPeca = false;

                //Tenta localizar bishop, horse ou rei
                while(((linhaPos - i) >= 0) && (temPeca == false) && (achou == false))
                {
                    auxPecaPin = tabuleiro[linhaPos - i][colunaPos - j][0];
                    auxRei = tabuleiro[linhaPos - i][colunaPos - j][1];

                    //Verificar se tem bishop ou horse
                    if((auxPecaPin == 'B') || (auxPecaPin == 'H'))
                    {
                        achou = true;
                        pecaPin = auxPecaPin;
                    }
                    //Verificar se tem rei
                    else if(auxRei == 'k')
                    {
                        achou = true;
                        rei = auxRei;
                    }
                    //Verificar se tem uma peça no caminho
                    else if(auxPecaPin != ' ')
                    {
                        temPeca = true;
                    }
                    i++;
                    j++;
                }
                
                //Diagonal direita inferior
                i = 1;
                j = 1;
                achou = false;

                //Tenta localizar bishop, horse ou rei
                while(((linhaPos + i) < 9) && (temPeca == false) && (achou == false))
                {
                    auxPecaPin = tabuleiro[linhaPos + i][colunaPos + j][0];
                    auxRei = tabuleiro[linhaPos + i][colunaPos + j][1];

                    //Verificar se tem bishop ou horse
                    if((auxPecaPin == 'B') || (auxPecaPin == 'H'))
                    {
                        achou = true;
                        pecaPin = auxPecaPin;
                    }
                    //Verificar se tem rei
                    else if(auxRei == 'k')
                    {
                        achou = true;
                        rei = auxRei;
                    }
                    //Verificar se tem uma peça no caminho
                    else if(auxPecaPin != ' ')
                    {
                        temPeca = true;
                    }
                    i++;
                    j++;
                }

                //Conferir se a peça está pinada
                if((rei == 'k') && ((pecaPin == 'B') || (pecaPin == 'H')))
                {
                    estaPinado = true;
                }
                else //Continua procurando
                {
                    //Zerar "rei" e "pecaPin" por garantia
                    rei = '0';
                    pecaPin = '0';
                    
                    //Diagonal direita superior
                    i = 1;
                    j = 1;

                    achou = false;
                    temPeca = false;

                    //Tenta localizar bishop, horse ou rei
                    while(((linhaPos - i) >= 0) && (temPeca == false) && (achou == false))
                    {
                        auxPecaPin = tabuleiro[linhaPos - i][colunaPos + j][0];
                        auxRei = tabuleiro[linhaPos - i][colunaPos + j][1];

                        //Verificar se tem bishop ou horse
                        if((auxPecaPin == 'B') || (auxPecaPin == 'H'))
                        {
                            achou = true;
                            pecaPin = auxPecaPin;
                        }
                        //Verificar se tem rei
                        else if(auxRei == 'k')
                        {
                            achou = true;
                            rei = auxRei;
                        }
                        //Verificar se tem uma peça no caminho
                        else if(auxPecaPin != ' ')
                        {
                            temPeca = true;
                        }
                        i++;
                        j++;
                    }
                    
                    //Diagonal esquerda inferior
                    i = 1;
                    j = 1;
                    achou = false;

                    //Tenta localizar bishop, horse ou rei
                    while(((linhaPos + i) < 9) && (temPeca == false) && (achou == false))
                    {
                        auxPecaPin = tabuleiro[linhaPos + i][colunaPos - j][0];
                        auxRei = tabuleiro[linhaPos + i][colunaPos - j][1];

                        //Verificar se tem bishop ou horse
                        if((auxPecaPin == 'B') || (auxPecaPin == 'H'))
                        {
                            achou = true;
                            pecaPin = auxPecaPin;
                        }
                        //Verificar se tem rei
                        else if(auxRei == 'k')
                        {
                            achou = true;
                            rei = auxRei;
                        }
                        //Verificar se tem uma peça no caminho
                        else if(auxPecaPin != ' ')
                        {
                            temPeca = true;
                        }
                        i++;
                        j++;
                    }

                    //Conferir se a peça está pinada
                    if((rei == 'k') && ((pecaPin == 'B') || (pecaPin == 'H')))
                    {
                        estaPinado = true;
                    }
                }
            }
        }
    }

    return estaPinado;
}

/*
    Função parecida com a check() que verifica se a peça que efetuou o check pode ser capturada. Essa função é necessária apenas para 
    descobrir se houve um checkmate. ELA NÃO CONSIDERA O REI COMO POSSIBILIDADE DE DEFESA.
*/
bool podeProtegerAtacando(char atacante, int *posicaoAtacante, pecasTabuleiro tabuleiro[9][9])
{
    int i, j;
    bool podeProteger = false;
    char pecaAtacando;
    bool temPeca = false; //Para verificar horizontais, verticais e diagonais
    int linhaPos, colunaPos;
    char rei;
    int posicao;

    //Pegar a posição atual da peça (linha x coluna) após conversão da jogada para matriz tabuleiro
    colunaPos = 9 - (*posicaoAtacante % 10);
    linhaPos = (*posicaoAtacante / 10) - 1;

    /*
        //Peças que podem proteger o rei
        L K S G R B P T Z N I D H
    */
    /*
        //Oponente toma check
        * * * * *
          * * *
        * * P * *
          * * *
        *   *   *
    */
    /*
        //Jogador toma check
        *   *   *
          * * *
        * * p * *
          * * *
        * * * * *
    */
    //Conferir se é o jogador ou oponente
    //Jogador toma check
    if((atacante > 'a') && (atacante <= 'z'))
    {
        //u == upperCase
        atacante = 'u';
    }
    else //Oponente toma check
    {
        //l == lowerCase
        atacante = 'l';
    }

	switch(atacante)
	{
        //Oponente toma check
	    case 'l':
            //Cima
	        //Garantir que não tenha números fora do índice da matriz
		    if((linhaPos - 1) >= 0)
		    {
		        pecaAtacando = tabuleiro[linhaPos - 1][colunaPos][0];
		
                //Verificar se a peça pode capturar o atacante
                if((pecaAtacando == 'l') || (pecaAtacando == 's') || (pecaAtacando == 'g') || (pecaAtacando == 'r') || (pecaAtacando == 'p') || 
                    (pecaAtacando == 't') || (pecaAtacando == 'z') || (pecaAtacando == 'n') || (pecaAtacando == 'i') || (pecaAtacando == 'd') || (pecaAtacando == 'h'))
                {
                    //Pegar a posição
                    posicao = (linhaPos - 1) * 10;
                    posicao += colunaPos;

                    //Conferir se a peça não está pinada
                    if(pinado(pecaAtacando, &posicao, tabuleiro) == false)
                    {
                        podeProteger = true;
                        
                        //Utilizar break para sair do switch case evitando conferir outras direções
                        break;
                    }
                }
		    }
		    //Cima esquerda
		    //Garantir que não tenha números fora do índice da matriz
		    if(((linhaPos - 1) >= 0) && ((colunaPos - 1) >= 0))
		    {
		        pecaAtacando = tabuleiro[linhaPos - 1][colunaPos - 1][0];
		
                //Verificar se a peça pode capturar o atacante
                if((pecaAtacando == 's') || (pecaAtacando == 'g') || (pecaAtacando == 'b') || (pecaAtacando == 't') ||  
                    (pecaAtacando == 'z') || (pecaAtacando == 'n') || (pecaAtacando == 'i') || (pecaAtacando == 'd') || (pecaAtacando == 'h'))
                {
                    //Pegar a posição
                    posicao = (linhaPos - 1) * 10;
                    posicao += colunaPos - 1;

                    //Conferir se a peça não está pinada
                    if(pinado(pecaAtacando, &posicao, tabuleiro) == false)
                    {
                        podeProteger = true;
                        break;
                    }
                }
		    }
		    //Cima direita
		    //Garantir que não tenha números fora do índice da matriz
		    if(((linhaPos - 1) >= 0) && ((colunaPos + 1) < 9))
		    {
		        pecaAtacando = tabuleiro[linhaPos - 1][colunaPos + 1][0];
		
                //Verificar se a peça pode capturar o atacante
                if((pecaAtacando == 's') || (pecaAtacando == 'g') || (pecaAtacando == 'b') || (pecaAtacando == 't') || 
                    (pecaAtacando == 'z') || (pecaAtacando == 'n') || (pecaAtacando == 'i') || (pecaAtacando == 'd') || (pecaAtacando == 'h'))
                {
                    //Pegar a posição
                    posicao = (linhaPos - 1) * 10;
                    posicao += colunaPos + 1;

                    //Conferir se a peça não está pinada
                    if(pinado(pecaAtacando, &posicao, tabuleiro) == false)
                    {
                        podeProteger = true;
                        break;
                    }
                }
		    }
		    //Esquerda
		    //Garantir que não tenha números fora do índice da matriz
		    if((colunaPos - 1) >= 0)
		    {
		        pecaAtacando = tabuleiro[linhaPos][colunaPos - 1][0];
		
                //Verificar se a peça pode capturar o atacante
                if((pecaAtacando == 'g') || (pecaAtacando == 'r') || (pecaAtacando == 't') || (pecaAtacando == 'z') || 
                    (pecaAtacando == 'n') || (pecaAtacando == 'i') || (pecaAtacando == 'd') || (pecaAtacando == 'h'))
                {
                    //Pegar a posição
                    posicao = linhaPos * 10;
                    posicao += colunaPos - 1;

                    //Conferir se a peça não está pinada
                    if(pinado(pecaAtacando, &posicao, tabuleiro) == false)
                    {
                        podeProteger = true;
                        break;
                    }
                }
		    }
		    //Direita
		    //Garantir que não tenha números fora do índice da matriz
		    if((colunaPos + 1) < 9)
		    {
		        pecaAtacando = tabuleiro[linhaPos][colunaPos + 1][0];
		
                //Verificar se a peça pode capturar o atacante
                if((pecaAtacando == 'g') || (pecaAtacando == 'r') || (pecaAtacando == 't') || (pecaAtacando == 'z') || 
                    (pecaAtacando == 'n') || (pecaAtacando == 'i') || (pecaAtacando == 'd') || (pecaAtacando == 'h'))
                {
                    //Pegar a posição
                    posicao = linhaPos * 10;
                    posicao += colunaPos + 1;

                    //Conferir se a peça não está pinada
                    if(pinado(pecaAtacando, &posicao, tabuleiro) == false)
                    {
                        podeProteger = true;
                        break;
                    }
                }
		    }
		    //Baixo
		    //Garantir que não tenha números fora do índice da matriz
		    if((linhaPos + 1) < 9)
		    {
		        pecaAtacando = tabuleiro[linhaPos + 1][colunaPos][0];
		
                //Verificar se a peça pode capturar o atacante
                if((pecaAtacando == 'g') || (pecaAtacando == 'r') || (pecaAtacando == 't') || (pecaAtacando == 'z') || 
                    (pecaAtacando == 'n') || (pecaAtacando == 'i') || (pecaAtacando == 'd') || (pecaAtacando == 'h'))
                {
                    //Pegar a posição
                    posicao = (linhaPos + 1) * 10;
                    posicao += colunaPos;

                    //Conferir se a peça não está pinada
                    if(pinado(pecaAtacando, &posicao, tabuleiro) == false)
                    {
                        podeProteger = true;
                        break;
                    }
                }
		    }
		    //Baixo esquerda
		    //Garantir que não tenha números fora do índice da matriz
		    if(((linhaPos + 1) < 9) && ((colunaPos - 1) >= 0))
		    {
		        pecaAtacando = tabuleiro[linhaPos + 1][colunaPos - 1][0];
		
                //Verificar se a peça pode capturar o atacante
                if((pecaAtacando == 's') || (pecaAtacando == 'b') || (pecaAtacando == 'd') || (pecaAtacando == 'h'))
                {
                    //Pegar a posição
                    posicao = (linhaPos + 1) * 10;
                    posicao += colunaPos - 1;

                    //Conferir se a peça não está pinada
                    if(pinado(pecaAtacando, &posicao, tabuleiro) == false)
                    {
                        podeProteger = true;
                        break;
                    }
                }
		    }
		    //Baixo direita
		    //Garantir que não tenha números fora do índice da matriz
		    if(((linhaPos + 1) < 9) && ((colunaPos + 1) < 9))
		    {
		        pecaAtacando = tabuleiro[linhaPos + 1][colunaPos + 1][0];
		
                //Verificar se a peça pode capturar o atacante
                if((pecaAtacando == 's') || (pecaAtacando == 'b') || (pecaAtacando == 'd') || (pecaAtacando == 'h'))
                {
                    //Pegar a posição
                    posicao = (linhaPos + 1) * 10;
                    posicao += colunaPos + 1;

                    //Conferir se a peça não está pinada
                    if(pinado(pecaAtacando, &posicao, tabuleiro) == false)
                    {
                        podeProteger = true;
                        break;
                    }
                }
		    }
		    //Vertical superior
		    i = 1;
		        
		    //Tenta localizar rook, dragon ou lance
		    while(((linhaPos - i) >= 0) && (temPeca == false) && (podeProteger == false))
		    {
		        pecaAtacando = tabuleiro[linhaPos - i][colunaPos][0];
		
		        //Verificar se tem rook, dragon ou lance
		        if((pecaAtacando == 'r') || (pecaAtacando == 'd') || (pecaAtacando == 'l'))
		        {
		            //Pegar a posição
                    posicao = (linhaPos - i) * 10;
                    posicao += colunaPos;

                    //Conferir se a peça não está pinada
                    if(pinado(pecaAtacando, &posicao, tabuleiro) == false)
                    {
                        podeProteger = true;
                        break;
                    }
		        }
                //Verificar se tem uma peça no caminho
		        else if(pecaAtacando != ' ')
		        {
		            temPeca = true;
		        }
		        i++;
		    }
		    
		    //Vertical inferior
		    i = 1;
            temPeca = false;
		
		    //Tenta localizar rook ou dragon
		    while(((linhaPos + i) < 9) && (temPeca == false) && (podeProteger == false))
		    {
		        pecaAtacando = tabuleiro[linhaPos + i][colunaPos][0];
		
		        if((pecaAtacando == 'r') || (pecaAtacando == 'd'))
		        {
		            //Pegar a posição
                    posicao = (linhaPos + i) * 10;
                    posicao += colunaPos;

                    //Conferir se a peça não está pinada
                    if(pinado(pecaAtacando, &posicao, tabuleiro) == false)
                    {
                        podeProteger = true;
                        break;
                    }
		        }
                //Verificar se tem uma peça no caminho
		        else if(pecaAtacando != ' ')
		        {
		            temPeca = true;
		        }
		        i++;
		    }
		    
		    //Horizontal esquerda
		    j = 1;
            temPeca = false;
		
		    //Tenta localizar rook ou dragon
		    while(((colunaPos - j) >= 0) && (temPeca == false) && (podeProteger == false))
		    {
		        pecaAtacando = tabuleiro[linhaPos][colunaPos - j][0];
		
		        if((pecaAtacando == 'r') || (pecaAtacando == 'd'))
		        {
		            //Pegar a posição
                    posicao = linhaPos * 10;
                    posicao += colunaPos - j;

                    //Conferir se a peça não está pinada
                    if(pinado(pecaAtacando, &posicao, tabuleiro) == false)
                    {
                        podeProteger = true;
                        break;
                    }
		        }
                //Verificar se tem uma peça no caminho
		        else if(pecaAtacando != ' ')
		        {
		            temPeca = true;
		        }
		        j++;
		    }
		    
		    //Horizontal direita
		    j = 1;
            temPeca = false;
		
		    //Tenta localizar rook ou dragon
		    while(((colunaPos + j) < 9) && (temPeca == false) && (podeProteger == false))
		    {
		        pecaAtacando = tabuleiro[linhaPos][colunaPos + j][0];

		        if((pecaAtacando == 'r') || (pecaAtacando == 'd'))
		        {
		            //Pegar a posição
                    posicao = linhaPos * 10;
                    posicao += colunaPos + j;

                    //Conferir se a peça não está pinada
                    if(pinado(pecaAtacando, &posicao, tabuleiro) == false)
                    {
                        podeProteger = true;
                        break;
                    }
		        }
                //Verificar se tem uma peça no caminho
		        else if(pecaAtacando != ' ')
		        {
		            temPeca = true;
		        }
		        j++;
		    }
		    
		    //Diagonal esquerda superior
		    i = 1;
		    j = 1;
            temPeca = false;
		
		    //Tenta localizar bishop ou horse
		    while(((linhaPos - i) >= 0) && (temPeca == false) && (podeProteger == false))
		    {
		        pecaAtacando = tabuleiro[linhaPos - i][colunaPos - j][0];
		
		        //Verificar se tem bishop ou horse
		        if((pecaAtacando == 'b') || (pecaAtacando == 'h'))
		        {
		            //Pegar a posição
                    posicao = (linhaPos - i) * 10;
                    posicao += colunaPos - j;

                    //Conferir se a peça não está pinada
                    if(pinado(pecaAtacando, &posicao, tabuleiro) == false)
                    {
                        podeProteger = true;
                        break;
                    }
		        }
                //Verificar se tem uma peça no caminho
		        else if(pecaAtacando != ' ')
		        {
		            temPeca = true;
		        }
		        i++;
		        j++;
		    }
		    
		    //Diagonal direita inferior
		    i = 1;
		    j = 1;
            temPeca = false;
		
		    //Tenta localizar bishop ou horse
		    while(((linhaPos + i) < 9) && (temPeca == false) && (podeProteger == false))
		    {
		        pecaAtacando = tabuleiro[linhaPos + i][colunaPos + j][0];

		        //Verificar se tem bishop ou horse
		        if((pecaAtacando == 'b') || (pecaAtacando == 'h'))
		        {
		            //Pegar a posição
                    posicao = (linhaPos + i) * 10;
                    posicao += colunaPos + j;

                    //Conferir se a peça não está pinada
                    if(pinado(pecaAtacando, &posicao, tabuleiro) == false)
                    {
                        podeProteger = true;
                        break;
                    }
		        }
                //Verificar se tem uma peça no caminho
		        else if(pecaAtacando != ' ')
		        {
		            temPeca = true;
		        }
		        i++;
		        j++;
		    }
		    
		    //Diagonal direita superior
		    i = 1;
		    j = 1;
            temPeca = false;
		
		    //Tenta localizar bishop ou horse
		    while(((linhaPos - i) >= 0) && (temPeca == false) && (podeProteger == false))
		    {
		        pecaAtacando = tabuleiro[linhaPos - i][colunaPos + j][0];

		        //Verificar se tem bishop ou horse
		        if((pecaAtacando == 'b') || (pecaAtacando == 'h'))
		        {
		            //Pegar a posição
                    posicao = (linhaPos - i) * 10;
                    posicao += colunaPos + j;

                    //Conferir se a peça não está pinada
                    if(pinado(pecaAtacando, &posicao, tabuleiro) == false)
                    {
                        podeProteger = true;
                        break;
                    }
		        }
                //Verificar se tem uma peça no caminho
		        else if(pecaAtacando != ' ')
		        {
		            temPeca = true;
		        }
		        i++;
		        j++;
		    }
		    
		    //Diagonal esquerda inferior
		    i = 1;
		    j = 1;
            temPeca = false;
		
		    //Tenta localizar bishop ou horse
		    while(((linhaPos + i) < 9) && (temPeca == false) && (podeProteger == false))
		    {
		        pecaAtacando = tabuleiro[linhaPos + i][colunaPos - j][0];

		        //Verificar se tem bishop ou horse
		        if((pecaAtacando == 'b') || (pecaAtacando == 'h'))
		        {
		            //Pegar a posição
                    posicao = (linhaPos + i) * 10;
                    posicao += colunaPos - j;

                    //Conferir se a peça não está pinada
                    if(pinado(pecaAtacando, &posicao, tabuleiro) == false)
                    {
                        podeProteger = true;
                        break;
                    }
		        }
                //Verificar se tem uma peça no caminho
		        else if(pecaAtacando != ' ')
		        {
		            temPeca = true;
		        }
		        i++;
		        j++;
		    }
		    //Knight na esquerda
		    if(((linhaPos - 2) < 9) && ((colunaPos - 1) >= 0))
		    {
		        //Verificar se não é rei inimigo (começa com a mesma palavra)
                rei = tabuleiro[linhaPos - 2][colunaPos - 1][1];
		
		        if(rei != 'k')
		        {
		            pecaAtacando = tabuleiro[linhaPos - 2][colunaPos - 1][0];
		
		            //Verificar se tem knight
		            if(pecaAtacando == 'k')
		            {
		                //Pegar a posição
                        posicao = (linhaPos - 2) * 10;
                        posicao += colunaPos - 1;

                        //Conferir se a peça não está pinada
                        if(pinado(pecaAtacando, &posicao, tabuleiro) == false)
                        {
                            podeProteger = true;
                            break;
                        }
		            }
		        }
		    }
		    //Knight na direita
		    if(((linhaPos - 2) < 9) && ((colunaPos + 1) < 9))
		    {
		        //Verificar se não é rei inimigo (começa com a mesma palavra)
		        rei = tabuleiro[linhaPos - 2][colunaPos + 1][1];
		
		        if(rei != 'k')
		        {
		            pecaAtacando = tabuleiro[linhaPos - 2][colunaPos + 1][0];
		
		            //Verificar se tem knight
		            if(pecaAtacando == 'k')
		            {
		                //Pegar a posição
                        posicao = (linhaPos - 2) * 10;
                        posicao += colunaPos + 1;

                        //Conferir se a peça não está pinada
                        if(pinado(pecaAtacando, &posicao, tabuleiro) == false)
                        {
                            podeProteger = true;
                            break;
                        }
		            }
		        }
		    }
            break;

        //Jogador toma check
	    case 'u':
            //Cima
	        //Garantir que não tenha números fora do índice da matriz
		    if((linhaPos - 1) >= 0)
		    {
		        pecaAtacando = tabuleiro[linhaPos - 1][colunaPos][0];

		        //Verificar se a peça pode capturar o atacante
                if((pecaAtacando == 'G') || (pecaAtacando == 'R') || (pecaAtacando == 'N') || (pecaAtacando == 'I') || 
                   (pecaAtacando == 'T') || (pecaAtacando == 'Z') || (pecaAtacando == 'D') || (pecaAtacando == 'H'))
                {
                    //Pegar a posição
                    posicao = (linhaPos - 1) * 10;
                    posicao += colunaPos;

                    //Conferir se a peça não está pinada
                    if(pinado(pecaAtacando, &posicao, tabuleiro) == false)
                    {
                        podeProteger = true;
                        
                        //Utilizar break para sair do switch case evitando conferir outras direções
                        break;
                    }
                }
		    }
		    //Cima esquerda
		    //Garantir que não tenha números fora do índice da matriz
		    if(((linhaPos - 1) >= 0) && ((colunaPos - 1) >= 0))
		    {
		        pecaAtacando = tabuleiro[linhaPos - 1][colunaPos - 1][0];
		
                //Verificar se a peça pode capturar o atacante
                if((pecaAtacando == 'S') || (pecaAtacando == 'B') || (pecaAtacando == 'D') || (pecaAtacando == 'H'))
                {
                    //Pegar a posição
                    posicao = (linhaPos - 1) * 10;
                    posicao += colunaPos - 1;

                    //Conferir se a peça não está pinada
                    if(pinado(pecaAtacando, &posicao, tabuleiro) == false)
                    {
                        podeProteger = true;
                        break;
                    }
                }
		    }
		    //Cima direita
		    //Garantir que não tenha números fora do índice da matriz
		    if(((linhaPos - 1) >= 0) && ((colunaPos + 1) < 9))
		    {
		        pecaAtacando = tabuleiro[linhaPos - 1][colunaPos + 1][0];
		
                //Verificar se a peça pode capturar o atacante
                if((pecaAtacando == 'S') || (pecaAtacando == 'B') || (pecaAtacando == 'D') || (pecaAtacando == 'H'))
                {
                    //Pegar a posição
                    posicao = (linhaPos - 1) * 10;
                    posicao += colunaPos + 1;

                    //Conferir se a peça não está pinada
                    if(pinado(pecaAtacando, &posicao, tabuleiro) == false)
                    {
                        podeProteger = true;
                        break;
                    }
                }
		    }
		    //Esquerda
		    //Garantir que não tenha números fora do índice da matriz
		    if((colunaPos - 1) >= 0)
		    {
		        pecaAtacando = tabuleiro[linhaPos][colunaPos - 1][0];
		
                //Verificar se a peça pode capturar o atacante
                if((pecaAtacando == 'G') || (pecaAtacando == 'R') || (pecaAtacando == 'T') || (pecaAtacando == 'Z') || 
                   (pecaAtacando == 'N') || (pecaAtacando == 'I') || (pecaAtacando == 'D') || (pecaAtacando == 'H'))
                {
                    //Pegar a posição
                    posicao = linhaPos * 10;
                    posicao += colunaPos - 1;

                    //Conferir se a peça não está pinada
                    if(pinado(pecaAtacando, &posicao, tabuleiro) == false)
                    {
                        podeProteger = true;
                        break;
                    }
                }
		    }
		    //Direita
		    //Garantir que não tenha números fora do índice da matriz
		    if((colunaPos + 1) < 9)
		    {
		        pecaAtacando = tabuleiro[linhaPos][colunaPos + 1][0];
		
                //Verificar se a peça pode capturar o atacante
                if((pecaAtacando == 'G') || (pecaAtacando == 'R') || (pecaAtacando == 'T') || (pecaAtacando == 'Z') ||  
                   (pecaAtacando == 'N') || (pecaAtacando == 'I') || (pecaAtacando == 'D') || (pecaAtacando == 'H'))
                {
                    //Pegar a posição
                    posicao = linhaPos * 10;
                    posicao += colunaPos + 1;

                    //Conferir se a peça não está pinada
                    if(pinado(pecaAtacando, &posicao, tabuleiro) == false)
                    {
                        podeProteger = true;
                        break;
                    }
                }
		    }
		    //Baixo
		    //Garantir que não tenha números fora do índice da matriz
		    if((linhaPos + 1) < 9)
		    {
		        pecaAtacando = tabuleiro[linhaPos + 1][colunaPos][0];
		
                //Verificar se a peça pode capturar o atacante
                if((pecaAtacando == 'L') || (pecaAtacando == 'S') || (pecaAtacando == 'G') || (pecaAtacando == 'R') || (pecaAtacando == 'P') || 
                   (pecaAtacando == 'T') || (pecaAtacando == 'Z') || (pecaAtacando == 'N') || (pecaAtacando == 'I') || (pecaAtacando == 'D') || (pecaAtacando == 'H'))
                {
                    //Pegar a posição
                    posicao = (linhaPos + 1) * 10;
                    posicao += colunaPos;

                    //Conferir se a peça não está pinada
                    if(pinado(pecaAtacando, &posicao, tabuleiro) == false)
                    {
                        podeProteger = true;
                        break;
                    }
                }
		    }
		    //Baixo esquerda
		    //Garantir que não tenha números fora do índice da matriz
		    if(((linhaPos + 1) < 9) && ((colunaPos - 1) >= 0))
		    {
		        pecaAtacando = tabuleiro[linhaPos + 1][colunaPos - 1][0];
		
                //Verificar se a peça pode capturar o atacante
                if((pecaAtacando == 'S') || (pecaAtacando == 'G') || (pecaAtacando == 'T') || (pecaAtacando == 'Z') ||  
                   (pecaAtacando == 'N') || (pecaAtacando == 'I') || (pecaAtacando == 'B') || (pecaAtacando == 'D') || (pecaAtacando == 'H'))
                {
                    //Pegar a posição
                    posicao = (linhaPos + 1) * 10;
                    posicao += colunaPos - 1;

                    //Conferir se a peça não está pinada
                    if(pinado(pecaAtacando, &posicao, tabuleiro) == false)
                    {
                        podeProteger = true;
                        break;
                    }
                }
		    }
		    //Baixo direita
		    //Garantir que não tenha números fora do índice da matriz
		    if(((linhaPos + 1) < 9) && ((colunaPos + 1) < 9))
		    {
		        pecaAtacando = tabuleiro[linhaPos + 1][colunaPos + 1][0];
		
                //Verificar se a peça pode capturar o atacante
                if((pecaAtacando == 'S') || (pecaAtacando == 'G') || (pecaAtacando == 'T') || (pecaAtacando == 'Z') ||  
                   (pecaAtacando == 'N') || (pecaAtacando == 'I') || (pecaAtacando == 'B') || (pecaAtacando == 'D') || (pecaAtacando == 'H'))
                {
                    //Pegar a posição
                    posicao = (linhaPos + 1) * 10;
                    posicao += colunaPos + 1;

                    //Conferir se a peça não está pinada
                    if(pinado(pecaAtacando, &posicao, tabuleiro) == false)
                    {
                        podeProteger = true;
                        break;
                    }
                }
		    }
		    //Vertical superior
		    i = 1;
		        
		    //Tenta localizar rook ou dragon
		    while(((linhaPos - i) >= 0) && (temPeca == false) && (podeProteger == false))
		    {
		        pecaAtacando = tabuleiro[linhaPos - i][colunaPos][0];
		
		        //Verificar se tem rook, dragon ou lance
		        if((pecaAtacando == 'R') || (pecaAtacando == 'D'))
		        {
		            //Pegar a posição
                    posicao = (linhaPos - i) * 10;
                    posicao += colunaPos;

                    //Conferir se a peça não está pinada
                    if(pinado(pecaAtacando, &posicao, tabuleiro) == false)
                    {
                        podeProteger = true;
                        break;
                    }
		        }
                //Verificar se tem uma peça no caminho
		        else if(pecaAtacando != ' ')
		        {
		            temPeca = true;
		        }
		        i++;
		    }
		    
		    //Vertical inferior
		    i = 1;
            temPeca = false;
		
		    //Tenta localizar rook ou dragon
		    while(((linhaPos + i) < 9) && (temPeca == false) && (podeProteger == false))
		    {
		        pecaAtacando = tabuleiro[linhaPos + i][colunaPos][0];
		
		        if((pecaAtacando == 'R') || (pecaAtacando == 'D') || (pecaAtacando == 'L'))
		        {
		            //Pegar a posição
                    posicao = (linhaPos + i) * 10;
                    posicao += colunaPos;

                    //Conferir se a peça não está pinada
                    if(pinado(pecaAtacando, &posicao, tabuleiro) == false)
                    {
                        podeProteger = true;
                        break;
                    }
		        }
                //Verificar se tem uma peça no caminho
		        else if(pecaAtacando != ' ')
		        {
		            temPeca = true;
		        }
		        i++;
		    }
		    
		    //Horizontal esquerda
		    j = 1;
            temPeca = false;
		
		    //Tenta localizar rook ou dragon
		    while(((colunaPos - j) >= 0) && (temPeca == false) && (podeProteger == false))
		    {
		        pecaAtacando = tabuleiro[linhaPos][colunaPos - j][0];
		
		        if((pecaAtacando == 'R') || (pecaAtacando == 'D'))
		        {
		            //Pegar a posição
                    posicao = linhaPos * 10;
                    posicao += (colunaPos - j);

                    //Conferir se a peça não está pinada
                    if(pinado(pecaAtacando, &posicao, tabuleiro) == false)
                    {
                        podeProteger = true;
                        break;
                    }
		        }
                //Verificar se tem uma peça no caminho
		        else if(pecaAtacando != ' ')
		        {
		            temPeca = true;
		        }
		        j++;
		    }
		    
		    //Horizontal direita
		    j = 1;
            temPeca = false;
		
		    //Tenta localizar rook ou dragon
		    while(((colunaPos + j) < 9) && (temPeca == false) && (podeProteger == false))
		    {
		        pecaAtacando = tabuleiro[linhaPos][colunaPos + j][0];

		        if((pecaAtacando == 'R') || (pecaAtacando == 'D'))
		        {
		            //Pegar a posição
                    posicao = linhaPos * 10;
                    posicao += (colunaPos + j);

                    //Conferir se a peça não está pinada
                    if(pinado(pecaAtacando, &posicao, tabuleiro) == false)
                    {
                        podeProteger = true;
                        break;
                    }
		        }
                //Verificar se tem uma peça no caminho
		        else if(pecaAtacando != ' ')
		        {
		            temPeca = true;
		        }
		        j++;
		    }
		    
		    //Diagonal esquerda superior
		    i = 1;
		    j = 1;
            temPeca = false;
		
		    //Tenta localizar bishop ou horse
		    while(((linhaPos - i) >= 0) && (temPeca == false) && (podeProteger == false))
		    {
		        pecaAtacando = tabuleiro[linhaPos - i][colunaPos - j][0];
		
		        //Verificar se tem bishop ou horse
		        if((pecaAtacando == 'B') || (pecaAtacando == 'H'))
		        {
		            //Pegar a posição
                    posicao = (linhaPos - i) * 10;
                    posicao += (colunaPos - j);

                    //Conferir se a peça não está pinada
                    if(pinado(pecaAtacando, &posicao, tabuleiro) == false)
                    {
                        podeProteger = true;
                        break;
                    }
		        }
                //Verificar se tem uma peça no caminho
		        else if(pecaAtacando != ' ')
		        {
		            temPeca = true;
		        }
		        i++;
		        j++;
		    }
		    
		    //Diagonal direita inferior
		    i = 1;
		    j = 1;
            temPeca = false;
		
		    //Tenta localizar bishop ou horse
		    while(((linhaPos + i) < 9) && (temPeca == false) && (podeProteger == false))
		    {
		        pecaAtacando = tabuleiro[linhaPos + i][colunaPos + j][0];

		        //Verificar se tem bishop ou horse
		        if((pecaAtacando == 'B') || (pecaAtacando == 'H'))
		        {
		            //Pegar a posição
                    posicao = (linhaPos + i) * 10;
                    posicao += (colunaPos + j);

                    //Conferir se a peça não está pinada
                    if(pinado(pecaAtacando, &posicao, tabuleiro) == false)
                    {
                        podeProteger = true;
                        break;
                    }
		        }
                //Verificar se tem uma peça no caminho
		        else if(pecaAtacando != ' ')
		        {
		            temPeca = true;
		        }
		        i++;
		        j++;
		    }
		    
		    //Diagonal direita superior
		    i = 1;
		    j = 1;
            temPeca = false;
		
		    //Tenta localizar bishop ou horse
		    while(((linhaPos - i) >= 0) && (temPeca == false) && (podeProteger == false))
		    {
		        pecaAtacando = tabuleiro[linhaPos - i][colunaPos + j][0];

		        //Verificar se tem bishop ou horse
		        if((pecaAtacando == 'B') || (pecaAtacando == 'H'))
		        {
		            //Pegar a posição
                    posicao = (linhaPos - i) * 10;
                    posicao += (colunaPos + j);

                    //Conferir se a peça não está pinada
                    if(pinado(pecaAtacando, &posicao, tabuleiro) == false)
                    {
                        podeProteger = true;
                        break;
                    }
		        }
                //Verificar se tem uma peça no caminho
		        else if(pecaAtacando != ' ')
		        {
		            temPeca = true;
		        }
		        i++;
		        j++;
		    }
		    
		    //Diagonal esquerda inferior
		    i = 1;
		    j = 1;
            temPeca = false;
		
		    //Tenta localizar bishop ou horse
		    while(((linhaPos + i) < 9) && (temPeca == false) && (podeProteger == false))
		    {
		        pecaAtacando = tabuleiro[linhaPos + i][colunaPos - j][0];

		        //Verificar se tem bishop ou horse
		        if((pecaAtacando == 'B') || (pecaAtacando == 'H'))
		        {
		            //Pegar a posição
                    posicao = (linhaPos + i) * 10;
                    posicao += (colunaPos - j);

                    //Conferir se a peça não está pinada
                    if(pinado(pecaAtacando, &posicao, tabuleiro) == false)
                    {
                        podeProteger = true;
                        break;
                    }
		        }
                //Verificar se tem uma peça no caminho
		        else if(pecaAtacando != ' ')
		        {
		            temPeca = true;
		        }
		        i++;
		        j++;
		    }
            //Knight na esquerda
		    if(((linhaPos + 2) >= 0) && ((colunaPos - 1) >= 0))
		    {
		        //Verificar se não é rei inimigo (começa com a mesma palavra)
                rei = tabuleiro[linhaPos + 2][colunaPos - 1][1];
		
		        if(rei != 'K')
		        {
		            pecaAtacando = tabuleiro[linhaPos + 2][colunaPos - 1][0];
		
		            //Verificar se tem knight
		            if(pecaAtacando == 'K')
		            {
		                //Pegar a posição
                        posicao = (linhaPos + 2) * 10;
                        posicao += (colunaPos - 1);

                        //Conferir se a peça não está pinada
                        if(pinado(pecaAtacando, &posicao, tabuleiro) == false)
                        {
                            podeProteger = true;
                            break;
                        }
		            }
		        }
		    }
		    //Knight na direita
		    if(((linhaPos + 2) >= 0) && ((colunaPos + 1) < 9))
		    {
		        //Verificar se não é rei inimigo (começa com a mesma palavra)
		        rei = tabuleiro[linhaPos + 2][colunaPos + 1][1];
		
		        if(rei != 'K')
		        {
		            pecaAtacando = tabuleiro[linhaPos + 2][colunaPos + 1][0];
		
		            //Verificar se tem knight
		            if(pecaAtacando == 'K')
		            {
		                //Pegar a posição
                        posicao = (linhaPos + 2) * 10;
                        posicao += (colunaPos + 1);

                        //Conferir se a peça não está pinada
                        if(pinado(pecaAtacando, &posicao, tabuleiro) == false)
                        {
                            podeProteger = true;
                            break;
                        }
		            }
		        }
		    }
            break;

		default:
			break;
	}

    return podeProteger;
}

/*
    Essa função verifica se dá para colocar uma peça em um dos lugares vazios caso a peça que deu check seja == R, B, D, H. 
    Essa função é necessária apenas para descobrir se houve um checkmate. "posicaoRei" DEVE receber o valor da matriz tabuleiro
*/
bool podeProtegerDefendendo(char atacante, int *posicaoAtacante, int *posicaoRei, int maoJogador[7], int maoOponente[7], pecasTabuleiro tabuleiro[9][9])
{
    /*
        VETOR
        0 1 2 3 4 5 6
        B G K L P R S
    */
    int i, j, z = 0;
    bool podeProteger = false;
    bool achou = false;
    char peca;
    char qualPeca;
    int linhaRei, colunaRei, linhaAtacante, colunaAtacante;
    int posicao;
    bool jogador = false; //Verificar se quem está chamando a função é o Jogador ou Oponente
    bool confereRegra = false; //Verificar se é preciso conferir a regra das peças: Pawn, Lance e Knight

    //Pegar a posição atual do rei (linha x coluna)
    colunaRei = *posicaoRei % 10;
    linhaRei = *posicaoRei / 10;

    //Pegar a posição atual da peça (linha x coluna) após conversão da jogada para matriz tabuleiro
    colunaAtacante = 9 - (*posicaoAtacante % 10);
    linhaAtacante = (*posicaoAtacante / 10) - 1;

    //Diminuir as linhas de código deixando jogador && oponente dentro do while
    qualPeca = toupper(atacante);

    //Verificar se o usuário possui alguma peça para se defender
    //Jogador
    if((atacante > 'a') && (atacante <= 'z'))
    {
        while((z < 7) && (achou == false))
        {
            if(maoJogador[z] > 0)
            {
                achou = true;
                jogador = true;
            }
            z++;
        }
    }
    else //Oponente
    {
        while((z < 7) && (achou == false))
        {
            if(maoOponente[z] > 0)
            {
                achou = true;
            }
            z++;
        }
    }

    //Verificar se o usuário possui alguma peça
    if(achou == true)
    {
        //Verificar se é R/D ou B/H
        if((qualPeca == 'R') || (qualPeca == 'D'))
        {
            //Vertical superior
            i = 1;
                
            //Tenta localizar algum campo vazio
            while(((linhaRei - i) > linhaAtacante) && (podeProteger == false))
            {
                peca = tabuleiro[linhaRei - i][colunaRei][0];

                //Verificar se é um campo vazio E se é o JOGADOR
                if((jogador == true) && (peca == ' '))
                {
                    //Verificar se o jogador é obrigado a usar alguma peça que possui regra
                    if((maoJogador[0] == 0) && (maoJogador[1] == 0) && (maoJogador[5] == 0) && (maoJogador[6] == 0))
                    {
                        //Pegar a posição atual para chamada da função regrasPeca e peaoColuna
                        posicao = (linhaRei - i) * 10;
                        posicao += colunaRei;

                        confereRegra = true;
                    }
                    else
                    {
                        podeProteger = true;
                    }
                }
                //Verificar se é um campo vazio E se é o OPONENTE
                else if((jogador == false) && (peca == ' '))
                {
                    //Verificar se o oponente é obrigado a usar alguma peça que possui regra
                    if((maoOponente[0] == 0) && (maoOponente[1] == 0) && (maoOponente[5] == 0) && (maoOponente[6] == 0))
                    {
                        //Pegar a posição atual para chamada da função regrasPeca e peaoColuna
                        posicao = (linhaRei - i) * 10;
                        posicao += colunaRei;

                        confereRegra = true;
                    }
                    else
                    {
                        podeProteger = true;
                    }
                }
                i++;
            }
            
            //Vertical inferior
            i = 1;

            //Tenta localizar algum campo vazio
            while(((linhaRei + i) < linhaAtacante) && (podeProteger == false))
            {
                peca = tabuleiro[linhaRei + i][colunaRei][0];
                
                //Verificar se é um campo vazio E se é o JOGADOR
                if((jogador == true) && (peca == ' '))
                {
                    //Verificar se o jogador é obrigado a usar alguma peça que possui regra
                    if((maoJogador[0] == 0) && (maoJogador[1] == 0) && (maoJogador[5] == 0) && (maoJogador[6] == 0))
                    {
                        //Pegar a posição atual para chamada da função regrasPeca e peaoColuna
                        posicao = (linhaRei + i) * 10;
                        posicao += colunaRei;

                        confereRegra = true;
                    }
                    else
                    {
                        podeProteger = true;
                    }
                }
                //Verificar se é um campo vazio E se é o OPONENTE
                else if((jogador == false) && (peca == ' '))
                {
                    //Verificar se o oponente é obrigado a usar alguma peça que possui regra
                    if((maoOponente[0] == 0) && (maoOponente[1] == 0) && (maoOponente[5] == 0) && (maoOponente[6] == 0))
                    {
                        //Pegar a posição atual para chamada da função regrasPeca e peaoColuna
                        posicao = (linhaRei + i) * 10;
                        posicao += colunaRei;

                        confereRegra = true;
                    }
                    else
                    {
                        podeProteger = true;
                    }
                }
                i++;
            }
            
            //Horizontal esquerda
            j = 1;

            //Tenta localizar algum campo vazio
            while(((colunaRei - j) > colunaAtacante) && (podeProteger == false))
            {
                peca = tabuleiro[linhaRei][colunaRei - j][0];
                
                //Verificar se é um campo vazio E se é o JOGADOR
                if((jogador == true) && (peca == ' '))
                {
                    //Verificar se o jogador é obrigado a usar alguma peça que possui regra
                    if((maoJogador[0] == 0) && (maoJogador[1] == 0) && (maoJogador[5] == 0) && (maoJogador[6] == 0))
                    {
                        //Pegar a posição atual para chamada da função regrasPeca e peaoColuna
                        posicao = linhaRei * 10;
                        posicao += colunaRei - j;

                        confereRegra = true;
                    }
                    else
                    {
                        podeProteger = true;
                    }
                }
                //Verificar se é um campo vazio E se é o OPONENTE
                else if((jogador == false) && (peca == ' '))
                {
                    //Verificar se o oponente é obrigado a usar alguma peça que possui regra
                    if((maoOponente[0] == 0) && (maoOponente[1] == 0) && (maoOponente[5] == 0) && (maoOponente[6] == 0))
                    {
                        //Pegar a posição atual para chamada da função regrasPeca e peaoColuna
                        posicao = linhaRei * 10;
                        posicao += colunaRei - j;

                        confereRegra = true;
                    }
                    else
                    {
                        podeProteger = true;
                    }
                }
                j++;
            }
            
            //Horizontal direita
            j = 1;

            //Tenta localizar algum campo vazio
            while(((colunaRei + j) < colunaAtacante) && (podeProteger == false))
            {
                peca = tabuleiro[linhaRei][colunaRei + j][0];
                
                //Verificar se é um campo vazio E se é o JOGADOR
                if((jogador == true) && (peca == ' '))
                {
                    //Verificar se o jogador é obrigado a usar alguma peça que possui regra
                    if((maoJogador[0] == 0) && (maoJogador[1] == 0) && (maoJogador[5] == 0) && (maoJogador[6] == 0))
                    {
                        //Pegar a posição atual para chamada da função regrasPeca e peaoColuna
                        posicao = linhaRei * 10;
                        posicao += colunaRei + j;

                        confereRegra = true;
                    }
                    else
                    {
                        podeProteger = true;
                    }
                }
                //Verificar se é um campo vazio E se é o OPONENTE
                else if((jogador == false) && (peca == ' '))
                {
                    //Verificar se o oponente é obrigado a usar alguma peça que possui regra
                    if((maoOponente[0] == 0) && (maoOponente[1] == 0) && (maoOponente[5] == 0) && (maoOponente[6] == 0))
                    {
                        //Pegar a posição atual para chamada da função regrasPeca e peaoColuna
                        posicao = linhaRei * 10;
                        posicao += colunaRei + j;

                        confereRegra = true;
                    }
                    else
                    {
                        podeProteger = true;
                    }
                }
                j++;
            }
        }
        else if((qualPeca == 'B') || (qualPeca == 'H'))
        {
            //Diagonal esquerda superior
            i = 1;
            j = 1;

            //Tenta localizar bishop ou horse
            while(((linhaRei - i) > linhaAtacante) && (podeProteger == false))
            {
                peca = tabuleiro[linhaRei - i][colunaRei - j][0];

                //Verificar se é um campo vazio E se é o JOGADOR
                if((jogador == true) && (peca == ' '))
                {
                    //Verificar se o jogador é obrigado a usar alguma peça que possui regra
                    if((maoJogador[0] == 0) && (maoJogador[1] == 0) && (maoJogador[5] == 0) && (maoJogador[6] == 0))
                    {
                        //Pegar a posição atual para chamada da função regrasPeca e peaoColuna
                        posicao = (linhaRei - i) * 10;
                        posicao += colunaRei - j;

                        confereRegra = true;
                    }
                    else
                    {
                        podeProteger = true;
                    }
                }
                //Verificar se é um campo vazio E se é o OPONENTE
                else if((jogador == false) && (peca == ' '))
                {
                    //Verificar se o oponente é obrigado a usar alguma peça que possui regra
                    if((maoOponente[0] == 0) && (maoOponente[1] == 0) && (maoOponente[5] == 0) && (maoOponente[6] == 0))
                    {
                        //Pegar a posição atual para chamada da função regrasPeca e peaoColuna
                        posicao = (linhaRei - i) * 10;
                        posicao += colunaRei - j;

                        confereRegra = true;
                    }
                    else
                    {
                        podeProteger = true;
                    }
                }
                i++;
                j++;
            }
            
            //Diagonal direita inferior
            i = 1;
            j = 1;

            //Tenta localizar bishop ou horse
            while(((linhaRei + i) < linhaAtacante) && (podeProteger == false))
            {
                peca = tabuleiro[linhaRei + i][colunaRei + j][0];
                
                //Verificar se é um campo vazio E se é o JOGADOR
                if((jogador == true) && (peca == ' '))
                {
                    //Verificar se o jogador é obrigado a usar alguma peça que possui regra
                    if((maoJogador[0] == 0) && (maoJogador[1] == 0) && (maoJogador[5] == 0) && (maoJogador[6] == 0))
                    {
                        //Pegar a posição atual para chamada da função regrasPeca e peaoColuna
                        posicao = (linhaRei + i) * 10;
                        posicao += colunaRei + j;

                        confereRegra = true;
                    }
                    else
                    {
                        podeProteger = true;
                    }
                }
                //Verificar se é um campo vazio E se é o OPONENTE
                else if((jogador == false) && (peca == ' '))
                {
                    //Verificar se o oponente é obrigado a usar alguma peça que possui regra
                    if((maoOponente[0] == 0) && (maoOponente[1] == 0) && (maoOponente[5] == 0) && (maoOponente[6] == 0))
                    {
                        //Pegar a posição atual para chamada da função regrasPeca e peaoColuna
                        posicao = (linhaRei + i) * 10;
                        posicao += colunaRei + j;

                        confereRegra = true;
                    }
                    else
                    {
                        podeProteger = true;
                    }
                }
                i++;
                j++;
            }
            
            //Diagonal direita superior
            i = 1;
            j = 1;

            //Tenta localizar bishop ou horse
            while(((linhaRei - i) > linhaAtacante) && (podeProteger == false))
            {
                peca = tabuleiro[linhaRei - i][colunaRei + j][0];
                
                //Verificar se é um campo vazio E se é o JOGADOR
                if((jogador == true) && (peca == ' '))
                {
                    //Verificar se o jogador é obrigado a usar alguma peça que possui regra
                    if((maoJogador[0] == 0) && (maoJogador[1] == 0) && (maoJogador[5] == 0) && (maoJogador[6] == 0))
                    {
                        //Pegar a posição atual para chamada da função regrasPeca e peaoColuna
                        posicao = (linhaRei - i) * 10;
                        posicao += colunaRei + j;

                        confereRegra = true;
                    }
                    else
                    {
                        podeProteger = true;
                    }
                }
                //Verificar se é um campo vazio E se é o OPONENTE
                else if((jogador == false) && (peca == ' '))
                {
                    //Verificar se o oponente é obrigado a usar alguma peça que possui regra
                    if((maoOponente[0] == 0) && (maoOponente[1] == 0) && (maoOponente[5] == 0) && (maoOponente[6] == 0))
                    {
                        //Pegar a posição atual para chamada da função regrasPeca e peaoColuna
                        posicao = (linhaRei - i) * 10;
                        posicao += colunaRei + j;

                        confereRegra = true;
                    }
                    else
                    {
                        podeProteger = true;
                    }
                }
                i++;
                j++;
            }
            
            //Diagonal esquerda inferior
            i = 1;
            j = 1;

            //Tenta localizar bishop ou horse
            while(((linhaRei + i) < linhaAtacante) && (podeProteger == false))
            {
                peca = tabuleiro[linhaRei + i][colunaRei - j][0];
                
                //Verificar se é um campo vazio E se é o JOGADOR
                if((jogador == true) && (peca == ' '))
                {
                    //Verificar se o jogador é obrigado a usar alguma peça que possui regra
                    if((maoJogador[0] == 0) && (maoJogador[1] == 0) && (maoJogador[5] == 0) && (maoJogador[6] == 0))
                    {
                        //Pegar a posição atual para chamada da função regrasPeca e peaoColuna
                        posicao = (linhaRei + i) * 10;
                        posicao += colunaRei - j;

                        confereRegra = true;
                    }
                    else
                    {
                        podeProteger = true;
                    }
                }
                //Verificar se é um campo vazio E se é o OPONENTE
                else if((jogador == false) && (peca == ' '))
                {
                    //Verificar se o oponente é obrigado a usar alguma peça que possui regra
                    if((maoOponente[0] == 0) && (maoOponente[1] == 0) && (maoOponente[5] == 0) && (maoOponente[6] == 0))
                    {
                        //Pegar a posição atual para chamada da função regrasPeca e peaoColuna
                        posicao = (linhaRei + i) * 10;
                        posicao += colunaRei - j;

                        confereRegra = true;
                    }
                    else
                    {
                        podeProteger = true;
                    }
                }
                i++;
                j++;
            }
        }
    }

    //Se precisar conferir a regra do Pawn, Lance e Knight...
    if(confereRegra == true)
    {   
        //Jogador
        if(jogador == true)
        {
            //Knight
            if(maoJogador[2] > 0)
            {
                qualPeca = 'K';
                if(regrasPeca(qualPeca, &posicao, tabuleiro) == true)
                {
                    podeProteger = true; 
                }
            }
            //Lance
            else if(maoJogador[3] > 0)
            {
                qualPeca = 'L';
                if(regrasPeca(qualPeca, &posicao, tabuleiro) == true)
                {
                    podeProteger = true; 
                }
            }
            //Pawn
            else if(maoJogador[4] > 0)
            {
                qualPeca = 'P';
                if((regrasPeca(qualPeca, &posicao, tabuleiro) == true) && (peaoColuna(qualPeca, &posicao, tabuleiro) == false))
                {
                    podeProteger = true; 
                }
            }
        }
        else //Oponente
        {
            //Knight
            if(maoOponente[2] > 0)
            {
                qualPeca = 'k';
                if(regrasPeca(qualPeca, &posicao, tabuleiro) == true)
                {
                    podeProteger = true; 
                }
            }
            //Lance
            else if(maoOponente[3] > 0)
            {
                qualPeca = 'l';
                if(regrasPeca(qualPeca, &posicao, tabuleiro) == true)
                {
                    podeProteger = true; 
                }
            }
            //Pawn
            else if(maoOponente[4] > 0)
            {
                qualPeca = 'p';
                if((regrasPeca(qualPeca, &posicao, tabuleiro) == true) && (peaoColuna(qualPeca, &posicao, tabuleiro) == false))
                {
                    podeProteger = true; 
                }
            }
        }
    }

    return podeProteger;
}

bool podeProtegerInterceptando(char atacante, int *posicaoAtacante, int *posicaoRei, pecasTabuleiro tabuleiro[9][9])
{
    bool podeInterceptar = false;
    int i, j;
    int colunaRei, linhaRei, colunaAtacante, linhaAtacante;
    char qualPeca = toupper(atacante);
    int posicao;

    //Pegar a posição atual do rei (linha x coluna)
    colunaRei = *posicaoRei % 10;
    linhaRei = *posicaoRei / 10;

    //Pegar a posição atual da peça (linha x coluna) após conversão da jogada para matriz tabuleiro
    colunaAtacante = 9 - (*posicaoAtacante % 10);
    linhaAtacante = (*posicaoAtacante / 10) - 1;

    //Verificar se é R/D
    if((qualPeca == 'R') || (qualPeca == 'D'))
    {
        //Vertical superior
        i = 1;
            
        //Tenta localizar algum campo vazio
        while(((linhaRei - i) > linhaAtacante) && (podeInterceptar == false))
        {
            //Pegar a posicao atual e converter para matriz interface para usar a função podeProtegerAtacando()
            posicao = (linhaRei - i + 1) * 10;
            posicao += abs(colunaRei - 9) ;

            //Verificar se "existe alguma peça que pode capturar o campo vazio", ou seja, proteger o rei do check
            if(podeProtegerAtacando(atacante, &posicao, tabuleiro) == true)
            {
                podeInterceptar = true;
            }
            i++;
        }
        
        //Vertical inferior
        i = 1;

        //Tenta localizar algum campo vazio
        while(((linhaRei + i) < linhaAtacante) && (podeInterceptar == false))
        {
            //Pegar a posicao atual e converter para matriz interface para usar a função podeProtegerAtacando()
            posicao = (linhaRei + i + 1) * 10;
            posicao += abs(colunaRei - 9) ;

            //Verificar se "existe alguma peça que pode capturar o campo vazio", ou seja, proteger o rei do check
            if(podeProtegerAtacando(atacante, &posicao, tabuleiro) == true)
            {
                podeInterceptar = true;
            }
            i++;
        }
        
        //Horizontal esquerda
        j = 1;

        //Tenta localizar algum campo vazio
        while(((colunaRei - j) > colunaAtacante) && (podeInterceptar == false))
        {
            //Pegar a posicao atual e converter para matriz interface para usar a função podeProtegerAtacando()
            posicao = (linhaRei + 1) * 10;
            posicao += abs(colunaRei - j - 9) ;

            //Verificar se "existe alguma peça que pode capturar o campo vazio", ou seja, proteger o rei do check
            if(podeProtegerAtacando(atacante, &posicao, tabuleiro) == true)
            {
                podeInterceptar = true;
            }
            j++;
        }
        
        //Horizontal direita
        j = 1;

        //Tenta localizar algum campo vazio
        while(((colunaRei + j) < colunaAtacante) && (podeInterceptar == false))
        {
            //Pegar a posicao atual e converter para matriz interface para usar a função podeProtegerAtacando()
            posicao = (linhaRei + 1) * 10;
            posicao += abs(colunaRei + j - 9) ;

            //Verificar se "existe alguma peça que pode capturar o campo vazio", ou seja, proteger o rei do check
            if(podeProtegerAtacando(atacante, &posicao, tabuleiro) == true)
            {
                podeInterceptar = true;
            }
            j++;
        }
    }
    //Verificar se é B/H
    else if((qualPeca == 'B') || (qualPeca == 'H'))
    {
        //Diagonal esquerda superior
        i = 1;
        j = 1;

        //Tenta localizar bishop ou horse
        while(((linhaRei - i) > linhaAtacante) && (podeInterceptar == false))
        {
            //Pegar a posicao atual e converter para matriz interface para usar a função podeProtegerAtacando()
            posicao = (linhaRei - i + 1) * 10;
            posicao += abs(colunaRei - j - 9) ;

            //Verificar se "existe alguma peça que pode capturar o campo vazio", ou seja, proteger o rei do check
            if(podeProtegerAtacando(atacante, &posicao, tabuleiro) == true)
            {
                podeInterceptar = true;
            }
            i++;
            j++;
        }
        
        //Diagonal direita inferior
        i = 1;
        j = 1;

        //Tenta localizar bishop ou horse
        while(((linhaRei + i) < linhaAtacante) && (podeInterceptar == false))
        {
            //Pegar a posicao atual e converter para matriz interface para usar a função podeProtegerAtacando()
            posicao = (linhaRei + i + 1) * 10;
            posicao += abs(colunaRei + j - 9) ;

            //Verificar se "existe alguma peça que pode capturar o campo vazio", ou seja, proteger o rei do check
            if(podeProtegerAtacando(atacante, &posicao, tabuleiro) == true)
            {
                podeInterceptar = true;
            }
            i++;
            j++;
        }
        
        //Diagonal direita superior
        i = 1;
        j = 1;

        //Tenta localizar bishop ou horse
        while(((linhaRei - i) > linhaAtacante) && (podeInterceptar == false))
        {
            //Pegar a posicao atual e converter para matriz interface para usar a função podeProtegerAtacando()
            posicao = (linhaRei - i + 1) * 10;
            posicao += abs(colunaRei + j - 9) ;

            //Verificar se "existe alguma peça que pode capturar o campo vazio", ou seja, proteger o rei do check
            if(podeProtegerAtacando(atacante, &posicao, tabuleiro) == true)
            {
                podeInterceptar = true;
            }
            i++;
            j++;
        }
        
        //Diagonal esquerda inferior
        i = 1;
        j = 1;

        //Tenta localizar bishop ou horse
        while(((linhaRei + i) < linhaAtacante) && (podeInterceptar == false))
        {
            //Pegar a posicao atual e converter para matriz interface para usar a função podeProtegerAtacando()
            posicao = (linhaRei + i + 1) * 10;
            posicao += abs(colunaRei - j - 9) ;

            //Verificar se "existe alguma peça que pode capturar o campo vazio", ou seja, proteger o rei do check
            if(podeProtegerAtacando(atacante, &posicao, tabuleiro) == true)
            {
                podeInterceptar = true;
            }
            i++;
            j++;
        }
    }
    //Verificar se é L/l
    else if(qualPeca == 'L')
    {   
        //Jogador
        if(atacante == 'L')
        {
            //Vertical superior
            i = 1;
                
            //Tenta localizar algum campo vazio
            while(((linhaRei - i) > linhaAtacante) && (podeInterceptar == false))
            {
                //Pegar a posicao atual e converter para matriz interface para usar a função podeProtegerAtacando()
                posicao = (linhaRei - i + 1) * 10;
                posicao += abs(colunaRei - 9) ;

                //Verificar se "existe alguma peça que pode capturar o campo vazio", ou seja, proteger o rei do check
                if(podeProtegerAtacando(atacante, &posicao, tabuleiro) == true)
                {
                    podeInterceptar = true;
                }
                i++;
            }
        }
        
        //Oponente
        if(atacante == 'l')
        {
            //Vertical inferior
            i = 1;

            //Tenta localizar algum campo vazio
            while(((linhaRei + i) < linhaAtacante) && (podeInterceptar == false))
            {
                //Pegar a posicao atual e converter para matriz interface para usar a função podeProtegerAtacando()
                posicao = (linhaRei + i + 1) * 10;
                posicao += abs(colunaRei - 9) ;

                //Verificar se "existe alguma peça que pode capturar o campo vazio", ou seja, proteger o rei do check
                if(podeProtegerAtacando(atacante, &posicao, tabuleiro) == true)
                {
                    podeInterceptar = true;
                }
                i++;
            }
        }
    }

    return podeInterceptar;
}